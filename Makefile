#-------------------------------------------------------#
#                   Diaphane library                      #
#-------------------------------------------------------#

#CC   =  gcc
#CXX =  g++

CC  = gcc
CXX = g++
FC  = gfortran

TARGET = libdpn

# Options
# Turn on off fld/starrad/common
OPT += COMMON
OPT += FLD
OPT += STARRAD

# Debugging
# Do make DEBUG=1 for debug build
ifeq ($(DEBUG), 1)
CFLAGS = -g -O0 
else
CFLAGS = -O2
endif

# Standard flags
CFLAGS   += -fPIC -lm -Wall
CPPFLAGS  = $(CFLAGS)
SHARED    = -shared

# FLD Module
ifeq (FLD,$(findstring FLD,$(OPT)))

ifneq (COMMON,$(findstring COMMON,$(OPT)))
$(error FLD Module depends on COMMON module, please enable this option.)
endif

FLD_PATH = ./fld/fld
SRCS     += $(wildcard $(FLD_PATH)/*.c) $(wildcard $(FLD_PATH)/*.cpp)
INCL     += -I$(FLD_PATH)
CFLAGS   += -DDIAPHANE_FLD
CPPFLAGS += -DDIAPHANE_FLD
LDFLAGS  += -DDIAPHANE_FLD
endif

# Starrad Module
ifeq (STARRAD,$(findstring STARRAD,$(OPT)))

ifneq (COMMON,$(findstring COMMON,$(OPT)))
$(error STARRAD Module depends on COMMON module, please enable this option.)
endif
 
# MDL 
##PKD_PATH        = ../gasoline
PKD_PATH        = ../
MPI_MDL         = $(PKD_PATH)/mdl/mpi
MPI_CFLAGS      =  -I$(MPI_MDL) 
MPI_CPPFLAGS    =  -I$(MPI_MDL)  -DMPICH_SKIP_MPICXX -Wall $(CFLAGS)
MPI_LIBMDL      = $(MPI_MDL)/mdl.o -lm
MPI_MDL_CFLAGS  = -fPIC $(CFLAGS) -Wall


# Healpix, and pixel neighbours library
HPX_PATH 	= $(SR_PATH)/healpix
LIBPXN    = $(HPX_PATH)/libpxn.a
MDL       = $(MPI_MDL)/mdl.o

# Build dependencies mdl and healpix
DEPS = $(MDL) $(LIBPXN)

SR_PATH     = ./starrad/starrad
SRCS       += $(wildcard $(SR_PATH)/*.c) $(wildcard $(SR_PATH)/*.cpp)
INCL       += -I$(SR_PATH) -I$(SR_PATH)/healpix 
CFLAGS     += -DDIAPHANE_STARRAD $(MPI_CFLAGS) 
CPPFLAGS   += -DDIAPHANE_STARRAD $(MPI_CPPFLAGS)
LDFLAGS    += -DDIAPHANE_STARRAD $(MPI_LIBMDL)  -L$(SR_PATH)/healpix -lpxn

endif

# Common Module
ifeq (COMMON,$(findstring COMMON,$(OPT)))
CMN_PATH    = ./common
LIBCXXSPRT_PATH = $(CMN_PATH)/libcxxsupport
LIBCXXSPRT_SRCS = $(wildcard $(LIBCXXSPRT_PATH)/*.cc) 
SRCS       += $(wildcard $(CMN_PATH)/*.c) $(wildcard $(CMN_PATH)/*.cpp) $(LIBCXXSPRT_SRCS)
INCL       += -I$(CMN_PATH) -I$(LIBCXXSPRT_PATH)
CFLAGS     += -DCOMMON
CPPFLAGS   += -DCOMMON
LDFLAGS    += -DCOMMON
endif

CFLAGS += $(INCL)
CPPFLAGS += $(INCL)
OBJS =  $(patsubst %, %.o, $(SRCS)) 



# Rules
all: shared

shared: $(DEPS) $(OBJS) 
	$(CXX) $(CPPFLAGS) $(SHARED)  -o $(TARGET).so $(OBJS) $(LDFLAGS)

static: $(OBJS) 
	ar rcs $(TARGET).a  $(OBJS)

%.c.o: %.c 
	$(CC) -c $(CFLAGS) -o "$@" "$<"

%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) -o "$@" "$<"

%.cc.o: %.cc
	$(CXX) -c $(CPPFLAGS) -o "$@" "$<"

clean: 
	@printf "Cleaning: \n"
	@find . -type f -name '*.o' -print0 | xargs -0 -I % sh -c 'printf "% "; rm -f %'
	rm -f $(TARGET).so $(TARGET).a
	rm -f $(HPX_PATH)/libpxn.so

cleanall: clean
	cd $(MPI_MDL) && make clean
	cd $(HPX_PATH) && make clean

$(MDL):
	cd $(MPI_MDL) && make CC=$(CC) "CC_FLAGS=$(MPI_MDL_CFLAGS)";


$(LIBPXN): 
	cd $(HPX_PATH) && make CC=$(CC) CXX=$(CXX) FC=$(FC) DEBUG=$(DEBUG)


# Header dependencies 


#--------------------------------------------------------------------------------------------
# OLD MAKEFILE CONTENT

# ## Makefile for Flux Limited Diffusion library 

# CC = gcc
# ##CC=g++
# #CC = cc
# #CC = cpp
# CXX = g++
# #CFLAGS = -g -I.
# #CFLAGS = -I.


# ##OBJS = setunits.o setfldparams.o fldgen.o
# ###SOURCES = setunits.cpp setfldparams.c fldgen.cpp
# ##OBJECTS = $(SOURCES:.c=.o)


# COMMON_PATH = ../../common

# OBJECTS = $(COMMON_PATH)/setunits.o setfldparams.o fldgen.o

# LDFLAGS = -shared

# FLD_INCLUDE =   fldgen.h setunits.h setfldparams.h fld_types.h

# CFLAGS = -fPIC -lm -Wall -I$(COMMON_PATH) -I.

# TARGET = libfld.so

# fld: $(OBJECTS)
# 	$(CXX) $(CFLAGS) -o $(TARGET) $(OBJECTS)	$(LDFLAGS)

# ##$(OBJECTS): $(INCLUDE)

# .cpp.o:
# 	$(CXX) $(CFLAGS) -c $< -o $@

# .c.o:
# 	$(CC) $(CFLAGS) -c $< -o $@

# ####  static library
# ##libfld.a: $(OBJS)
# #	rm -f libfld.a
# #	ar qv libfld.a $(OBJS)
# #	mv libfld.a ..
# #	ranlib ../libfld.a

# #clean:
# #	rm -rf $(OBJS) *.a

# clean:
# 	rm -rf $(OBJECTS) *.so

# # DO NOT DELETE

# fldgen.o: $(COMMON_PATH)/Interpolate.h $(COMMON_PATH)/CodeUnits.h
# setunits.o: $(COMMON_PATH)/CodeUnits.h $(COMMON_PATH)/setunits.h 
# setfldparams.o: setfldparams.c setfldparams.h


# #----------------------------------------#
# #	Starrad Makefile 	 #
# #----------------------------------------#

# # Compiler
# CXX = g++

# # Target
# TARGET = libsrd.so

# # General Compiler flags
# OPTS = -Wall  -fPIC -Wl,-z,defs -g
# #-O2

# OPTS += -I.

# SHARED = -shared 

# # Compiler flags for c++ files
# CPPFLAGS = $(OPTS) 

# # Additional libraries
# LIBS = 

# # Objects to be built
# BUILD_DIR = build/
# OBJS = $(patsubst %.cpp,$(BUILD_DIR)%.o,$(wildcard *.cpp))

# #$(@D) = directory current target resides in
# # Rules
# all: $(TARGET)

# $(TARGET): $(OBJS) 
# 	$(CXX) $(CPPFLAGS) $(SHARED) $(LIBS) -o $(TARGET) $(OBJS)

# $(BUILD_DIR)%.o: %.cpp
# 	@mkdir -p $(@D)
# 	$(CXX) -c $(CPPFLAGS) -o "$@" "$<"

# clean: 
# 	rm -f build/*.o *.so *.dylib
# 	rm -f $(TARGET)
