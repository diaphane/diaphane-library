#ifndef DIAPHANE_H
#define DIAPHANE_H

/* 
	Diaphane main header
	Contains common diaphane functions
*/

#include "diaphane_types.h"

#ifdef __cplusplus
extern "C" {
#endif

DiaphaneContext* dpnInit();
void dpnInitUnits(DiaphaneContext *dpn_context);
void dpnInitModules(DiaphaneContext* dpn_context, unsigned module_choice);
DiaphaneContext* dpnCreateContext();
void dpnFinish(DiaphaneContext *dpn_context);
void dpnUpdateInternalEnergy(double* u, double dt, double uDot);
void dpnEvolveVariableSingle(float* y, float dt, float yDot);
void dpnEvolveVariableDouble(double* y, double dt, double yDot);
void dpnFinishModules(DiaphaneContext* dpn_context, unsigned module_choice);


#ifdef __cplusplus
 }
#endif

#endif
