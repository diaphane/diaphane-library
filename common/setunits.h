#ifndef SETUNITS_H
#define SETUNITS_H


/* setunits.h */

/*  functions associated with setting/converting units 
*/


#include "diaphane_types.h"

#ifdef __cplusplus
extern "C" {
#endif

// Common
double get_timeunit_in_seconds_givenG(double lengthunit_cm, double massunit_grams, double G_simunits);
double get_timeunit_in_seconds_givenvelunitcgs(double lengthunit_cm, double velunit_cgs);
double get_proper_velocityunit_gadget(double velunit_cgs_z0, double scale_factor);
double get_proper_velocityunit_gasoline(double velunit_cgs_z0, double scale_factor);
double get_proper_lengthunit(double length_cgs_z0, double scale_factor);
double get_kpc_cgs();
double get_solarmass_cgs();
double get_grav_const_cgs();
double cgsenergy_to_temperature_neutral_gas(double energy, double hydrogen_massfrac, double protonmass, double kboltzmann);

void compute_diaphane_units_from_base(DiaphaneUnits *dpnunits);



// // FLD
//#ifdef FLD
void rescale_comoving_to_physical_units_with_redshift_gadget(DiaphaneContext *dpncontext);
void rescale_comoving_to_physical_units_with_redshift_gasoline(DiaphaneContext *dpncontext);
//#endif

// // STARRAD
// #ifdef STARRAD
// void get_starrad_units(sr_units* sr_u);
// #endif

AFLOAT gadgetentropy_to_gadgetenergy(AFLOAT entropy, AFLOAT density, DiaphaneContext *dpncontext);
// AFLOAT gadgetentropy_to_gadgetenergy_gadget_style_version(AFLOAT entropy, AFLOAT density); // embedded gadget style
AFLOAT gadgetentropy_to_temperature(AFLOAT entropy, AFLOAT density, DiaphaneContext *dpncontext);
AFLOAT cgsenergy_to_gadgetentropy(AFLOAT energy, AFLOAT density, DiaphaneContext *dpncontext);
AFLOAT cgsenergydot_to_gadgetentropydot(AFLOAT energydot, AFLOAT density, DiaphaneContext *dpncontext);

AFLOAT entropy_to_energy(AFLOAT entropy, AFLOAT density, AFLOAT gamma_minus1); // pseudo entropy as used in e.g. gadget 
AFLOAT energy_to_entropy(AFLOAT energy, AFLOAT density, AFLOAT gamma_minus1);




#ifdef __cplusplus
}
#endif

#endif
