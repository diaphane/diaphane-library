#include <fstream>
#include <iterator>
#include <assert.h>
#include <cmath>

#include "opacity.h"
#include "Interpolate.h"

// TODO: change float, double to AFLOAT?

void setup_opacity_table(OpacityContext* oc)
{
	double MinTemperature, MaxTemperature, MinPressure, MaxPressure;
	/* read opacity table file -- actual c++ here */
	std::ifstream infile(oc->opacityTableFilename);
	if(infile.fail())
	{
		fprintf(stderr, "Couldn't open the opacity table file \"%s\"!\n", oc->opacityTableFilename);
		assert(0);
	}

	// Opacity table contains a line of boundaries/grid size info, then one entry per line.                                           
	// Note: table is log10 temperature vs log10 pressure (CGS)
	infile >> MinTemperature >> MaxTemperature;
	infile >> MinPressure >> MaxPressure;
	int tempGrid, pressGrid;
	infile >> tempGrid >> pressGrid;
	if(infile.fail())
	{
		fprintf(stderr, "Opacity table header malformed!\n");
		assert(0);
	}

	std::vector<double> kappas;
	kappas.reserve(tempGrid * pressGrid);
	std::copy(std::istream_iterator<double>(infile), std::istream_iterator<double>(), back_inserter(kappas));
	if((int) kappas.size() != tempGrid * pressGrid)
	{
		fprintf(stderr, "Opacity table didn't contain the right number of entries!\n");
		assert(0);
	}

	/* from Interpolate.h
	* BilinearInterpolator 
	*  Requires equal spaced input data (and there is no check)
	*/
	BilinearInterpolator<double>* interpolator = new BilinearInterpolator<double>(MinTemperature, MinPressure, MaxTemperature, MaxPressure, tempGrid, pressGrid, kappas.begin(), kappas.end());
	oc->opacity_interpolator = interpolator;  

	/* store table limits */
	oc->opacityTableMinTemperature = pow(10.,MinTemperature); 
	oc->opacityTableMaxTemperature = pow(10.,MaxTemperature);
	oc->opacityTableMinPressure = pow(10.,MinPressure);
	oc->opacityTableMaxPressure = pow(10.,MaxPressure);
}

/*  input: T, P  output: Opacity 
      Using a pre-built table, does a bi-linear interpolation of log(T) log(P) log(opacity) */
float interpolate_opacity(OpacityContext* oc, float temp, float pressure)
{
   /* should this be a static_cast, not reinterpret_cast (for portability)?? */ 
   return (float)pow(10.0, (*reinterpret_cast<BilinearInterpolator<double> *> (oc->opacity_interpolator))(log10((float)temp), log10((float)pressure)));
}

// TODO: should this be double precision?
float calc_opacity(OpacityContext* oc, float density, float temperature)
{
  /*
   * inputs and outputs in cgs units
   */
  
  float pressure, opacity, energy;

  if(!oc->PowerKappa) /* get opacity from Table */
    {
      /***
          internal energy = 3/2 kT/mu/mp (monatomic), 5/2 kT/mu/mp (diatomic)
                   energy = kT/mu/mp /(gamma-1)
	  pressure = nkT = density * kT/mu/mp
	  pressure = (kT/mu/mp) * density 
       -->pressure = density * energy  * (gamma-1)
	       monatomic:  gamma = 5/3
	       diatomic:   gamma = 7/5
      ***/

      energy = temperature * oc->temp_to_internalenergy_cgs;
      pressure = (oc->ConstGamma - 1.0) * density * energy; 

      /* respect the Temp and Press limits of the opacity interpolation table */
      /* should have something friendlier than an assert */

   
////  debugging ////////////////////////////////////////////  should user be notified of this? 
      if ((oc->opacityTableMinPressure  > pressure) | (pressure > oc->opacityTableMaxPressure)) // bad news!
        {
          printf("Diaphane calc_opacity(): bad news  Pmin P Pmax:  %g   %g   %g\n",oc->opacityTableMinPressure,pressure,oc->opacityTableMaxPressure);
          printf("Diaphane calc_opacity(): bad news  density energy temperature: %g   %g   %g\n",density, energy, temperature);
          printf("Diaphane calc_opacity(): bad news  T to u    gamma: %g %g  \n",oc->temp_to_internalenergy_cgs, oc->ConstGamma);
          fflush(stdout);
        } ////////////////////////////////////// debugging 

      // TIM: temp solution to very low density regions, clamp...
      pressure = (pressure < oc->opacityTableMinPressure) ? oc->opacityTableMinPressure : pressure;
      assert(pressure <= oc->opacityTableMaxPressure);       
      
      // Clamp temperature (check_temperature_in_range)
      temperature = (temperature < oc->opacityTableMinTemperature) ? oc->opacityTableMinTemperature : temperature;
      temperature = (temperature > oc->opacityTableMaxTemperature) ? oc->opacityTableMaxTemperature : temperature;

      opacity = interpolate_opacity(oc, temperature, pressure);
    }
  else /* use power law opacity = Const*Temp**Index */
    {
      opacity = oc->KappaConst*pow(temperature, oc->KappaIndex);
    }

  return opacity; /* cgs units expected in input/output in this library */
}

void free_opacity_table(OpacityContext* oc)
{
	delete (reinterpret_cast<BilinearInterpolator<double> *> (oc->opacity_interpolator));
}
