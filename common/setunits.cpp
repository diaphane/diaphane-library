/* units.cpp */

/*  functions associated with setting/converting units 
   Some of the functions have gasoline or gadget in the name, referring only to those code units conventions,
   Nothing here has any data structure specific to any particular simulation code.   

*/

#include <cassert>
#include <cstdio>
#include <fstream>
#include <vector>
#include <iterator>
#include <malloc.h>
#include <stdlib.h>

#include "CodeUnits.h"   /* c++ functions */


#include "setunits.h"
#include "diaphane_types.h"

CodeUnits units;

/* to do:    everything here is double change to AFLOAT ? */

/*** set Units and constants needed for Flux Limited Diffusion 
 call the c++ CodeUnits.h pass the needed values back to c wrapper.
there is probably a more elegant way to do this.
***/
/* these functions are technically code independent, but assume G=1 in code units  */



/* input:  mass unit, length unit, and either G or vel unit 
   output: all other physical units. 
*/
/* does NOT assume G==1 */
  /* note -- G in simunits is determined by timeunit_sec (can have G!=1) */
  //units = CodeUnits(lengthunit_cgs,massunit_cgs, timeunit_cgs); 


// input is length unit, mass unit, Gravitational const 
double get_timeunit_in_seconds_givenG(double lengthunit_cm, double massunit_grams, double G_simunits)
{
  double timeUnitInSeconds;
  double GravitationalConstantCGS;

  GravitationalConstantCGS = get_grav_const_cgs();
  timeUnitInSeconds = (sqrt(lengthunit_cm * lengthunit_cm * lengthunit_cm / massunit_grams / GravitationalConstantCGS / G_simunits));       
  return timeUnitInSeconds;
}

// input is length unit and velocitiy unit
double get_timeunit_in_seconds_givenvelunitcgs(double lengthunit_cm, double velunit_cgs)
{ 
  double timeUnitInSeconds;
  timeUnitInSeconds = lengthunit_cm / velunit_cgs;      
  return timeUnitInSeconds;
}

/* input base units: 1 simulation unit ==   lengthunit_cgs cm, massunit_cgs grams, timeunit_cgs seconds 
**   output the rest of the (derived) units        */

/* Codeunits.h require 3 variables -- 
*  1-  lengthunit in centimeters,
*  2-  massunit in grams, 
*  3 - and EITHER  G in sim units (useGunit = 1) (e.g. gasoline)
*          OR      sim velocity unit in cm/s (useVelunit = 1) (e.g. gadget)
*
*   This function is mainly a wrapper to the c++ CodeUnits.h
*
*  input:  massunit_cgs and lengthunit_cgs_z0 (in fldcontext), 
             and either G or vel unit 
             and scale_factor (=1/(1+z) == 1 for non-cosmological runs
   output: units needed for FLD calc., stored in fldcontext
*/


/* After length, time, and massunit are calculated,
     get all remaining units that might be used in various modules   
 */
void compute_diaphane_units_from_base(DiaphaneUnits *dpnunits)
{

  /* we store units in context so we do not need to call the c++ units class later (from c or fortran) */

  units = CodeUnits(dpnunits->lengthunit_cgs,dpnunits->massunit_cgs, dpnunits->timeunit_cgs);
  //  gasconstant_cgs = units.GasConstantCGS; /* cgs kB/mp*/
  
  dpnunits->StefanBoltzmannConstantCGS = units.StefanBoltzmannConstantCGS;
  dpnunits->pressureunit_cgs = units.pressureUnitCGS;
  dpnunits->opacityunit_cgs = units.opacityUnitCGS; 
  dpnunits->densityunit_cgs = units.densityUnitCGS;
  dpnunits->gradenergyunit_cgs = units.GradientInternalEnergyUnitCGS;
  //  dpnunits->gasconstantunit_cgs = units.GasConstantUnitCGS;  // ==kB/mp  in cgs (not gas constant in sim units)   // gas constant not needed in fld or sr
  /* convenient to have a direct Kelvin to u conversion to sim units and to cgs */

  /* Pressure = density * energy * (gamma-1) */
  /* Pressure = density * kB/mu/mp * Temperature */
  /* T  = u (gamma-1)  mu  mp  / kB */
  /* u = T * kB/mp  / mu  / (gamma-1) */
  /* gamma = 5/3 (monatomic gas) atomic, ionized */
  /* gamma = 1.4 (diatomic gas) molecular  */

  dpnunits->temperature_to_internalenergy_simunit = units.GasConstantUnitCGS/(dpnunits->ConstGamma - 1) / dpnunits->MeanMolWeight;
  dpnunits->temperature_to_internalenergy_cgs = units.GasConstantCGS/(dpnunits->ConstGamma - 1) / dpnunits->MeanMolWeight;
  dpnunits->heatingunit_cgs = units.HeatingUnitCGS; /* internal energy (heating or cooling) rate */
} 


// /***********    deprecated function ****************/
// /*
//     Unit needs for starrad are pretty much the same as for fld
// */
// void get_starrad_units(sr_units* sr_u)
// {

//   /* Also store units in starrad unit struct for easy use in c/fortran */

//   units = CodeUnits(sr_u->lengthunit_cgs,sr_u->massunit_cgs, sr_u->timeunit_cgs);

//   sr_u->pressureunit_cgs = units.pressureUnitCGS;
//   sr_u->opacityunit_cgs = units.opacityUnitCGS; 
//   sr_u->densityunit_cgs = units.densityUnitCGS;
//   sr_u->volumeunit_cgs = units.volumeUnitCGS;
//   sr_u->heatunit_cgs = units.HeatingUnitCGS;  internal energy (heating or cooling) rate 

//   /* 
//     Direct Kelvin to u conversion
//     Pressure = density * energy * (gamma-1) 
//     Pressure = density * kB/mu/mp * Temperature
//     T  = u (gamma-1)  mu  mp  / kB
//     u = T * kB/mp  / mu  / (gamma-1) 
//     gamma = 5/3 (monatomic gas) atomic, ionized 
//     gamma = 1.4 (diatomic gas) molecular  
//   */

//   sr_u->temp_to_internalenergy_sim = units.GasConstantUnitCGS/(sr_u->const_gamma - 1) / sr_u->mean_mol_weight;
//   sr_u->temp_to_internalenergy_cgs = units.GasConstantCGS/(sr_u->const_gamma - 1) / sr_u->mean_mol_weight;

// } 



/* Gadget velocity units: v_internal = a**2 v_comoving  (= a**2 v_peculiar/a) */
/*  "momentum" p = a**2 dx/dt, where x is comoving (phys dist r = a x) and
         where peculiar velocity (proper unis) == a * dx/dt.
  --> v_peculiar = v_internal / a */
/* Note: Gadget2 IC file velocity are different from Gadget2 internal velocity 
       units     v_file = v_pec / a**(1/2)  =  v_internal/a**(3/2)   
        Gadget2 outfile snapshot velocities should be same units as IC file */
/* see Gadget2 manual and Volker Springel comments here: 
     http://www.mpa-garching.mpg.de/gadget/gadget-list/0113.html */
double get_proper_velocityunit_gadget(double velunit_cgs_z0, double scale_factor)
{
  return (velunit_cgs_z0 / scale_factor);
}

/* in gasoline.  v_proper_peculiar = a * v_sim_peculiar */
double get_proper_velocityunit_gasoline(double velocityunit_cgs_z0, double scale_factor)
{
  return (velocityunit_cgs_z0 * scale_factor);
}

double get_proper_lengthunit(double length_unit_cgs_z0, double scale_factor)
{
  return (length_unit_cgs_z0 * scale_factor);
}


double get_kpc_cgs()
{
  units = CodeUnits(1.0,1.0,1.0); /* dummy call to get cgs length defintions */
  return units.KiloParsec;
}

double get_solarmass_cgs()
{
  units = CodeUnits(1.0,1.0,1.0); /* dummy call to get cgs mass definitions */
  return units.SolarMass;
}

double get_grav_const_cgs()
{
  units = CodeUnits(1.0,1.0,1.0); /* dummy call to get cgs mass definitions */
  return units.GravitationalConstantCGS;
}


/* adjust the units in the sim context variables to new cosmological redshift */
//   could separate out into 1 function for base units, and 1 for derived units
void rescale_comoving_to_physical_units_with_redshift_gadget(DiaphaneContext *dpncontext)
{
  DiaphaneSimContext *sim = dpncontext->sim;
  DiaphaneUnits *units = dpncontext->units;
  sim->periodx = sim->periodx_z0 * sim->scale_factor; // initialize z>0
  sim->periody = sim->periody_z0 * sim->scale_factor;
  sim->periodz = sim->periodz_z0 * sim->scale_factor;
  units->lengthunit_cgs = get_proper_lengthunit(sim->lengthunit_cgs_z0, sim->scale_factor);
  units->velocityunit_cgs = get_proper_velocityunit_gadget(sim->velocityunit_cgs_z0, sim->scale_factor);
  units->timeunit_cgs = get_timeunit_in_seconds_givenvelunitcgs(units->lengthunit_cgs, units->velocityunit_cgs); 

  /* could call Unit.h again, but only need heating unit */
//  units->heatingunit_cgs =  pow(units->lengthunit_cgs, 2.0)/pow(units->timeunit_cgs, 3.0); /* internal energy (heating or cooling) rate */
}


/* adjust the units in the FLD context variables to new cosmological redshift */
//   could separate out into 1 function for base units, and 1 for derived units
void rescale_comoving_to_physical_units_with_redshift_gasoline(DiaphaneContext *dpncontext)
{
  DiaphaneSimContext *sim = dpncontext->sim;
  DiaphaneUnits *units = dpncontext->units;
  sim->periodx = sim->periodx_z0 * sim->scale_factor; // initialize z>0
  sim->periody = sim->periody_z0 * sim->scale_factor;
  sim->periodz = sim->periodz_z0 * sim->scale_factor;
  units->lengthunit_cgs = get_proper_lengthunit(sim->lengthunit_cgs_z0, sim->scale_factor);
  units->velocityunit_cgs = get_proper_velocityunit_gasoline(sim->velocityunit_cgs_z0, sim->scale_factor);
  units->timeunit_cgs = get_timeunit_in_seconds_givenvelunitcgs(units->lengthunit_cgs, units->velocityunit_cgs); // constant in gasoline

  /* could call Unit.h again, but only need heating unit */
//  units->heatingunit_cgs =  pow(units->lengthunit_cgs, 2.0)/pow(units->timeunit_cgs, 3.0); /* internal energy (heating or cooling) rate */
}



// /******THIS FUNCTION NOT USED ---- JUST USE fldcontext->tempmperature_to_internalenergy  ********/
// double cgsenergy_to_temperature_neutral_gas(double energy, double hydrogen_massfrac, double protonmass, double kboltzmann)
// {
//   //  float mproton;
//   double temperature;
//   double molecular_weight;
//   molecular_weight = 4 / (1 + 3 * hydrogen_massfrac); /* neutral gas */
//   temperature = energy * molecular_weight * protonmass *2. /3. /kboltzmann;  
//   return temperature;
    
// }




/* internal energy. return code precision? velocity**2 code units  */
/* Gadget (not true) entropy is defined as entropy == A == P / rho**gamma
pressure = entropy * rho**gamma,  entropy = pressure / rho**gamma
pressure = (gamma - 1) * rho * u  --> u = pressure / rho / (gamma-1) */
/* input: entropy and density in gadget sim units, 
  output: sim units, identical to Internal energy of gadget snapshot (units of velocity**2, e.g. (km/s)**2 */
AFLOAT gadgetentropy_to_gadgetenergy(AFLOAT entropy, AFLOAT density, DiaphaneContext *dpncontext)
{
  AFLOAT energy;

  DiaphaneSimContext *sim = dpncontext->sim;
  
  if(sim->comoving) /* proper density != code density */
    {
      density /= pow((sim->scale_factor),3);
    }

  energy = (AFLOAT)entropy_to_energy((AFLOAT) entropy, (AFLOAT) density, (sim->ConstGamma-1));
  return energy; /* units of velocity**2 */
}


// TODO:   check the gadget entropy conversions -- use velocityunit_cgs_z0 or velocityunit_cgs?

/*  return library (not necessarily code) precision.  Kelvin */
/* Gadget (not true) entropy is defined as entropy == A == P / rho**gamma
pressure = entropy * rho**gamma,  entropy = pressure / rho**gamma
pressure = (gamma - 1) * rho * u  --> u = pressure / rho / (gamma-1) */
/*  E = u/m = 3/2kT    --> T = (2/3) * u*m/k      (m= mu*mp) */
AFLOAT gadgetentropy_to_temperature(AFLOAT entropy, AFLOAT density, DiaphaneContext *dpncontext)
{
  AFLOAT energy, temperature;
  //  AFLOAT proper_density, a3inv;

  DiaphaneSimContext *sim = dpncontext->sim;
  DiaphaneUnits *units = dpncontext->units;
  
  if(sim->comoving) /* proper density != code density */
    {
      density /= pow((sim->scale_factor),3);
    }

  /* ignoring this: in gadget GAMMA is hard-coded in allvars.h */  
/* internal energy per unit mass km/s **2  --> cm/s **2  --> Kelvin */
  energy = (AFLOAT)entropy_to_energy((AFLOAT) entropy, (AFLOAT) density, (sim->ConstGamma-1) );
  //  energy = energy * sim->velocityunit_cgs_z0 * sim->velocityunit_cgs_z0;
  energy = energy * units->velocityunit_cgs * units->velocityunit_cgs;  // proper vel units or comoving?
  temperature = energy / units->temperature_to_internalenergy_cgs;
  return temperature; /* Kelvin */
}


/* entropy here is "simulation entropy" 
   as in gadgdet: entropy == A == pressure / rho**gamma 
   input density must be in sim units
   input energy in cgs  
   output: entropy in gadget internal units   */
AFLOAT cgsenergy_to_gadgetentropy(AFLOAT energy, AFLOAT density, DiaphaneContext *dpncontext)
{
  //  AFLOAT entropy;
  AFLOAT gadgetentropy;

  DiaphaneSimContext *sim = dpncontext->sim;
  DiaphaneUnits *dpnunits = dpncontext->units;
  
  /***********  dangerously complicated unit conversion -- should verify that it is correct  *************/

  //    density in gadget units, but want proper not comoving
  if(sim->comoving) /* proper density != code density */
    {
      density /= pow((sim->scale_factor),3);
    }

  /* assuming density should be in proper units for the energy to entropy conversion.  */

  //  energy = energy / dpnunits->velocityunit_cgs_z0 / dpnunits->velocityunit_cgs_z0; // now gadget energy units (i.e. (km/s(**2)
  energy = energy / dpnunits->velocityunit_cgs / dpnunits->velocityunit_cgs; // now gadget energy units (i.e. (km/s(**2)

  gadgetentropy = energy_to_entropy(energy, density, (sim->ConstGamma-1) );
  /* entropy (cm/s)**2 to gadget (velocity**2 -- usually (km/s)**2 )  */

  return gadgetentropy; /* sim units */
}


/* entropy here is "simulation entropy" 
   as in gadgdet: entropy == A == pressure / rho**gamma */
/* input: du/dt  cgs internal energy rate 
          input density in gadget internal units
   output: dEntropy/dt  gadget Entropy rate sim units 
*/
AFLOAT cgsenergydot_to_gadgetentropydot(AFLOAT energydot, AFLOAT density, DiaphaneContext *dpncontext)
{
  // AFLOAT entropy;
  AFLOAT gadgetentropydot;
  DiaphaneUnits *dpnunits = dpncontext->units;
    
  /* 1st the numerator  Delta Internal Energy */
  gadgetentropydot = cgsenergy_to_gadgetentropy(energydot, density, dpncontext);

  /* then the denominator dt */
  gadgetentropydot *= dpnunits->timeunit_cgs;
  return gadgetentropydot; /* sim units */
}


/* input a proper (non-comoving) density, entropy, gas const. gamma, gamma-1 */
/* entropy here is "simulation entropy" 
        as in gadgdet: entropy == A == pressure / rho**gamma 
pressure = (gamma - 1) * rho * u 
--> entropy = (gamma-1) rho u / rho**gamma = (gamma-1) u /rho**(gamma-1)
energy = entropy / (gamma-1) * rho**(gamma-1)    */
AFLOAT entropy_to_energy(AFLOAT entropy, AFLOAT density, AFLOAT gamma_minus1)
{
  AFLOAT energy;
  energy = entropy / gamma_minus1 * pow(density, gamma_minus1);
  return energy;
}


/* input a proper (non-comoving) density, entropy, gas const. gamma, gamma-1 */
/* entropy here is "simulation entropy" 
        as in gadgdet: entropy == A == pressure / rho**gamma 
pressure = (gamma - 1) * rho * u
  --> entropy = (gamma-1) rho u / rho**gamma = (gamma-1) u / rho**(gamma-1)  */
AFLOAT energy_to_entropy(AFLOAT energy, AFLOAT density, AFLOAT gamma_minus1)
{
  AFLOAT entropy;
  entropy = energy * gamma_minus1 / pow(density, gamma_minus1);
  return entropy;
}

