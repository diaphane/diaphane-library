#ifndef DIAPHANE_COMMON_OPACITY_H
#define DIAPHANE_COMMON_OPACITY_H

/* 
	Opacity header
	Functions related to the setup and calcualtion of opacity
*/


#ifdef __cplusplus
extern "C" {
#endif

#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */ 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable.
#endif

	// TODO: should have the options of floats or doubles (use AFLOAT) here
/* parameters for calculating opacity */
typedef struct OpacityContext
{
 	// option to use power law opacity instead of table - Kappa = dKappaCont**dKappaIndex 
	int PowerKappa;  
	double KappaConst;
	double KappaIndex;
	// Limits/units
  	char opacityTableFilename[MAXPATHLEN];
  	double opacityTableMinTemperature; /* log_10(Kelvin) */
  	double opacityTableMaxTemperature; /* log_10(Kelvin) */
  	double opacityTableMinPressure; /* log_10(cgs pressure) */
  	double opacityTableMaxPressure; /* log_10(cgs pressure) */
	double temp_to_internalenergy_cgs;
	// Opacity table
  	double ConstGamma;
  	void* opacity_interpolator;  /* this is the bilinear interpolar c++ function */
} OpacityContext;



/********
typedef struct opacity_context{
	// Power law
	float power_kappa;
	float kappa_idx;
	float kappa_const;
	// Limits/units
	float max_temp;
	float min_temp;
	float max_pressure;
	float min_pressure;
	float temp_to_internalenergy_cgs;
	// Opacity table
	float const_gamma;
	void* interpolator;
}  opacity_context;
*********/

void setup_opacity_table(OpacityContext* oc);
float interpolate_opacity(OpacityContext* oc, float temp, float pressure);
float calc_opacity(OpacityContext* oc, float density, float temperature);
void free_opacity_table(OpacityContext* oc);

#ifdef __cplusplus
 }
#endif

#endif
