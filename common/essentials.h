
/* essentials.h */

#ifndef ESSENTIALS_H   /* inclusion guard needed because gasoline headers are nested */
#define ESSENTIALS_H



#ifdef __cplusplus
extern "C" {
#endif



  AFLOAT bspline_kernel_nonorm(AFLOAT nusquared) ;

  AFLOAT bspline_grad_kernel_over_nu_nonorm(AFLOAT nusquared) ;


  AFLOAT get_separation_1d(AFLOAT x1, AFLOAT x0, int periodic, AFLOAT period);

  AFLOAT calc_grad_kernel_over_r(grad_kernel_overnu_func_type grad_kernel_overnu_func, int periodic, AFLOAT periodx, AFLOAT periody, AFLOAT periodz, AFLOAT* pos, AFLOAT* pos_neighbor, AFLOAT rkernel);

  AFLOAT energysimunit_to_temperature(AFLOAT temperature_to_u_simunit, AFLOAT energy);

  AFLOAT check_temperature_in_range(AFLOAT temperature, AFLOAT mintemperature, AFLOAT maxtemperature);

  void initDiaphaneParticledata(AFLOAT *uDot, AFLOAT *dtmax);

  void CombineThisParticleWithVirtualUdot(AFLOAT* uDot1, AFLOAT* dt1, AFLOAT uDot2, AFLOAT dt2);

  typedef AFLOAT (* grad_kernel_overnu_func_type)(AFLOAT);

  INTIDTYPE* AllocateNeighborIDList(int nmaxneighbors);



#ifdef __cplusplus
}
#endif


#endif // ESSENTIALS_H



