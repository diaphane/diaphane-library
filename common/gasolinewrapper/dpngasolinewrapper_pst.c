/*
 Diaphane common gasoline wrapper 
 Processor set tree layer c file
 Contains pst layer function definitions  
*/
/* pkd pst layer header*/
#include "pst.h"


#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */ 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable 
#endif

 
#include "dpngasolinewrapper_pst.h"
#include "dpngasolinewrapper.h"


void pstInitDiaphane(PST pst, void* vin, int nIn, void* vout, int* pnOut) 
{
  //    as a reminder:
  //  inInitFluxLimitedDiffusionStruct *in = vin;                      // c version 
  //  inInitDiffusion* in = reinterpret_cast<inInitDiffusion *>(vin);  // c++ version 
  struct inInitDiaphaneStruct *in = vin;
  //  printf("sizesssssssssss %d %lu\n",nIn,sizeof(struct inInitDiaphaneStruct));
  mdlassert(pst->mdl,nIn==sizeof(struct inInitDiaphaneStruct));
//  mdlassert(pst->mdl,nIn==sizeof(*in));
  if(pst->nLeaves > 1) 
    {
      mdlReqService(pst->mdl, pst->idUpper, PST_INITDIAPHANE, vin, nIn);
      pstInitDiaphane(pst->pstLower, in, nIn, NULL, NULL);
      mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
    } 
  else
    {
      pkdInitDiaphane(pst->mdl, pst->plcl->pkd, in); 
    }
  if(pnOut)
    {
      *pnOut = 0;
    }
}


void pstInitDiaphaneStep(PST pst, void* vin, int nIn, void* vout, int* pnOut)
{
  //    as a reminder:
  //  inInitFluxLimitedDiffusionStruct *in = vin;                      // c version 
  //  inInitDiffusion* in = reinterpret_cast<inInitDiffusion *>(vin);  // c++ version 
  struct inInitDiaphaneStepStruct *in = vin;

  mdlassert(pst->mdl,nIn==sizeof(struct inInitDiaphaneStepStruct));
  if(pst->nLeaves > 1)
    {
      mdlReqService(pst->mdl, pst->idUpper, PST_INITDIAPHANESTEP, vin, nIn);
      pstInitDiaphaneStep(pst->pstLower, in, nIn, NULL, NULL);
      mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
    }
  else
    {
      pkdInitDiaphaneStep(pst->mdl, pst->plcl->pkd, in);
    }
  if(pnOut)
    {
      *pnOut = 0;
    }
}


// The arguments in these pst functions must match mdlAddService(MDL,int,void *,void (*)(void *,void *,int,void *,int *)
//void pstDpnUpdateu(PST pst, void* vin, size_t nIn)
void pstDpnUpdateu(PST pst, void* vin, int nIn, void* vout, int* pnOut)
{

	LCL *plcl = pst->plcl;
	struct inDpnUpdateu *in = vin;	
	mdlassert(pst->mdl,nIn == sizeof(struct inDpnUpdateu));
	
	if (pst->nLeaves > 1) {
		mdlReqService(pst->mdl,pst->idUpper,PST_DPNUPDATEU,vin,nIn);
		pstDpnUpdateu(pst->pstLower,in,nIn, NULL, NULL);
		mdlGetReply(pst->mdl,pst->idUpper,NULL,NULL);
	}
	else {
		dpnUpdateu(plcl->pkd, in->dt_length, in->radiationModel);
	}
}
