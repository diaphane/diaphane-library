
#ifndef COMMONGASOLINEWRAPPER_TYPES_H 
#define COMMONGASOLINEWRAPPER_TYPES_H


#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */ 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable 
#endif


// input struct for InitDiaphane
//typedef struct inDiaphaneStruct 
struct inInitDiaphaneStruct
{
  double dConstGamma;
  double dMeanMolWeight;
  int bPeriodic;
  int bComove;
  double dKpcUnit;
  double dMassUnit;
  double dPeriod;
  double dxPeriod;
  double dyPeriod;
  double dzPeriod;
  int simunittype;
  char param_file_name[MAXPATHLEN];
  int bGasFluxLimitedDiffusion;
  int bGasStarrad;
  int bFixedTempLimits;
  double dMinTemp;
  double dMaxTemp;
};////// inDiaphaneStruct;

// input structu for InitDiaphaneStep
struct inInitDiaphaneStepStruct
{
   double scale_factor;
}; /////////// inInitDiaphaneStepStruct;


// Input struct for DpnUpdateu
struct inDpnUpdateu 
{
        float dt_length;
        int radiationModel;
};

#endif
