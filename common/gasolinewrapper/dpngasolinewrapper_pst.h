#ifndef DPN_GASOLINEWRAPPER_PST_H
#define DPN_GASOLINEWRAPPER_PST_H

/*
 Diaphane common gasoline wrapper 
 Processor set tree layer c header
 Contains pst layer functions to be called from the diaphane master layer wrapper
*/

/* pkd pst layer header*/
#include "pst.h"

/*
	This allows gasoline to simply include dpngasolinewrapper_pst.h
	And cleanly get the includes for each radiation module
*/
#include "diaphane_types.h"
#include "dpngasolinewrapper_types.h"
#ifdef DIAPHANE_FLD
#include "fldgasolinewrapper_types.h"
#include "fldgasolinewrapper_pst.h"
#endif
#ifdef DIAPHANE_STARRAD
#include "srgasolinewrapper_pst.h"
#endif


void pstInitDiaphane(PST pst, void* vin, int nIn, void* vout, int* pnOut);
void pstInitDiaphaneStep(PST pst, void* vin, int nIn, void* vout, int* pnOut);
//void pstDpnUpdateu(PST pst, void* vin, size_t nIn);
void pstDpnUpdateu(PST pst, void* vin, int nIn, void* vout, int* pnOut);

#endif
