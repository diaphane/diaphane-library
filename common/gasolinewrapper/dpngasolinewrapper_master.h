#ifndef  DPN_GASOLINEWRAPPER_MASTER_H
#define DPN_GASOLINEWRAPPER_MASTER_H

/*
 Diaphane common gasoline wrapper 
 Master layer header
 Contains msr functions to be called from pkd master layer
*/

/* pkd master layer header */
#include "master.h"

/*
	This allows gasoline to simply include dpngasolinewrapper_master.h
	And cleanly get the includes for each radiation module
*/
#include "diaphane_types.h"
#ifdef DIAPHANE_FLD
#include "fldgasolinewrapper_types.h"
#include "fldgasolinewrapper_pst.h"
#endif
#ifdef DIAPHANE_STARRAD
#include "srgasolinewrapper_pst.h"
#endif

void msrInitDiaphane(MSR msr,double dTime, double scale_factor);
void msrInitDiaphaneStep(MSR msr, double scale_factor);  
void msrDoDiaphaneStep(MSR msr, double dTime, double dt_length, double scale_factor);
void msrDpnUpdateu(MSR msr, double dTime, int radiationModel);

#endif
