/*
 Diaphane common gasoline wrapper 
 Master layer c file
 Contains msr layer function definitions  
*/
#include <malloc.h>
#include <string.h>

#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */ 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable
#endif


#include "parameters.h"
#include "dpngasolinewrapper_types.h"
#include "dpngasolinewrapper_master.h"
#include "dpngasolinewrapper_pst.h"
#ifdef DIAPHANE_FLD
#include "fldgasolinewrapper_master.h"
#endif
#ifdef DIAPHANE_STARRAD
#include "srgasolinewrapper_master.h"
#endif

/* 
  Diaphane update internal energy 
*/

/* */

/* Using the gasoline code structure (master node calls slaves via pst) in the wrapper  --
     Is that the best way to do things? 
     TIM: I think its the only way of doing things for gasoline to ensure this stuff happens
     on every rank? In any case it seems to make sense to follow this stucture just for the gasoline wrapper
*/

//TODO:  should make sure library is updating u and uDot where appropriate, and not mixing them up.



/* Init Diaphane Library - once at start of run.
                           set units, parameters, persistent memory, etc.
            Driver function at master level.                             */
void msrInitDiaphane(MSR msr,double dTime, double scale_factor)
{
  struct inInitDiaphaneStruct dpnIn;

  int something_to_do;

// some debugging
//  printf("Init Diaphane mass unit %g struct debugging msr %p msr->param %p madd %p size %lu \n", msr->param.dMsolUnit, msr, (void *)&msr->param,  (void *)&msr->param.dMsolUnit, sizeof(msr->param)); ///////////////////////////
//printf("will read param file %s\n",msr->param.achDpnParams);
//printf("centmass value ad size %g %p %lu coolfac %g %p %lu \n",msr->param.dCentMass, (void *)&msr->param.dCentMass, sizeof(msr->param.dCentMass), msr->param.dCoolFac, (void *)&msr->param.dCoolFac, sizeof(msr->param.dCoolFac));
//printf("char14 %s %p %lu or len %lu %s %p %lu or len %lu \n",msr->param.achDigitMask, (void *)&msr->param.achDigitMask, sizeof(msr->param.achDigitMask),strlen(msr->param.achDigitMask),msr->param.achDataSubPath, (void *)&msr->param.achDataSubPath, sizeof(msr->param.achDataSubPath),strlen(msr->param.achDataSubPath));

  something_to_do = 0;

  //  printf("xxxxx  do starrad  %d  do fld %d  kpcunit %g munit %g xperiod %g \n",msr->param.bGasStarrad, msr->param.bGasFluxLimitedDiffusion, msr->param.dKpcUnit, msr->param.dMsolUnit, msr->param.dxPeriod); ///////////////////////////

#ifdef DIAPHANE_STARRAD
  if(msr->param.bGasStarrad)  something_to_do = 1;
#endif
#ifdef DIAPHANE_FLD
  if(msr->param.bGasFluxLimitedDiffusion) something_to_do = 1;
#endif

  if (something_to_do == 0) 
    {
      printf("warning -- No Diaphane modules specified to run -- skipping Diaphane Init\n");
      return;
    }

//printf("debugging %g %g %g %g %d %g %g %g %g %d %d %s\n",msr->param.dConstGamma,msr->param.dMeanMolWeight,msr->param.dKpcUnit, msr->param.dMsolUnit,msr->param.bPeriodic,msr->param.dPeriod,msr->param.dxPeriod,msr->param.dyPeriod,msr->param.dzPeriod,msr->param.bGasFluxLimitedDiffusion,msr->param.bGasStarrad,msr->param.achDpnParams);


  /* this struct should contain all context variables 
        that get passed to the library from the main code, practical */
//  dpnIn = (inDiaphaneStruct *)malloc(sizeof(inDiaphaneStruct));
  dpnIn.dConstGamma = msr->param.dConstGamma;
  dpnIn.dMeanMolWeight = msr->param.dMeanMolWeight;
  dpnIn.dKpcUnit = msr->param.dKpcUnit; 
  dpnIn.dMassUnit = msr->param.dMsolUnit;
  dpnIn.bPeriodic = msr->param.bPeriodic;
  dpnIn.dPeriod = msr->param.dPeriod;  /* sim units */
  dpnIn.dxPeriod = msr->param.dxPeriod; 
  dpnIn.dyPeriod = msr->param.dyPeriod; 
  dpnIn.dzPeriod =  msr->param.dzPeriod;
  dpnIn.simunittype = 1;  // 1-gasoline.  2-gadget.  3-sphynx.
  dpnIn.bGasFluxLimitedDiffusion = msr->param.bGasFluxLimitedDiffusion;
  dpnIn.bGasStarrad = msr->param.bGasStarrad;
  // Get parameter file name
  // be careful that MAXPATHLEN in dpn library for param_file_name is same in sim code and lib
  strcpy(dpnIn.param_file_name,msr->param.achDpnParams);

  if (msrComove(msr)) dpnIn.bComove = 1;
  pstInitDiaphane(msr->pst, &dpnIn, sizeof(dpnIn), NULL, NULL);
  printf("Done with Init Diaphane.  Now get initial rates.\n"); fflush(stdout);

//     We need to initialize by running a diaphane step.
//           reason: need timestep lengths from uDot (or 1st step may be too long), msrInitSph does this for hydro.
  msrDoDiaphaneStep(msr, dTime, 0., scale_factor); // doing a step of length 0, because we only want to the rates to get the timestep criteria


}


/* Call Diaphane Step */
/* Each physics module calls it own module_master.c  */
/* All diaphane physics lives on the same timestep */
/* inputs 
-- dTime: simulation time (sim units)
   dt_length: timestep length (sim units)
   scale_factor: (only used for cosmological comoving runs)
*/
void msrDoDiaphaneStep(MSR msr, double dTime, double dt_length, double scale_factor)
{
  printf("Init Diaphane Step \n"); 
  msrInitDiaphaneStep(msr, scale_factor); // some inits (and units rescaling for comoving runs)

  /* Here we call msrDo_module_Step (which includes any init and finish module routine) */
 /* Each module also has its own msrInitMODULEStep -- would it be better to call those module inits here  */
#ifdef DIAPHANE_STARRAD
  if(msr->param.bGasStarrad) 
    {
      printf("starrad step\n");  
      // Probably something about scaling here a la fld below
      msrDoStarradStep(msr, dTime);
   
      // Update particle temps
      msrDpnUpdateu(msr, dt_length, DPN_STARRAD);
 
      // update particle timesteps here?
      // msrDpnUpdateTimestep
   }
#endif
#ifdef DIAPHANE_FLD
  if(msr->param.bGasFluxLimitedDiffusion)
    {
      /* Flux Lmited Diffusion */
      printf("Flux Limited Diffusion step\n");
      //  includes init fld step and finish fld step
      // smooth needs dTime.  scale_factor needed for units.
      msrDoFluxLimitedDiffusionStep(msr, dTime, scale_factor);

      // update temperatures
      msrDpnUpdateu(msr, dt_length, DPN_FLD);

      // update particle timesteps here?
      // msrDpnUpdateTimestep
    }
#endif
}


/* some inits (and units rescaling for comoving runs)   */
// maybe we should re-organize so that this calls the init for each module 
void msrInitDiaphaneStep(MSR msr, double scale_factor)
{
    struct inInitDiaphaneStepStruct in;
//    in = (inInitDiaphaneStepStruct *)malloc(sizeof(inInitDiaphaneStepStruct));
    in.scale_factor = scale_factor;
    pstInitDiaphaneStep(msr->pst, &in, sizeof(in), NULL, NULL);  // go to pst layer to get to pkd layer
//////    free(in);
}



void msrDpnUpdateu(MSR msr, double dt_length, int radiationModel)
{
  // Fill input structure
  struct inDpnUpdateu in;
  in.dt_length = dt_length;
  in.radiationModel = radiationModel;

  // Pass on to pst layer
  pstDpnUpdateu(msr->pst, &in, sizeof(in), NULL, NULL);
}
