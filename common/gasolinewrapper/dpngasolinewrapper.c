/*
 Diaphane gasoline wrapper c file
 Final layer between gasoline wrapper and diaphane itself
 each function called per processor:
 	msr -> pst -> via mdl for parallel calls -> here
*/

#include <string.h>

#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */ 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable
#endif

#include "dpngasolinewrapper.h"
#include "diaphane_types.h"
#include "diaphane.h"
#include "setunits.h"

/* init Diaphane Radiation & Neutrino transport library -- once at start of run. */
/* in gasoline, this is called by dpngasolinewrapper_master.c to fill the input struct with code params  */
/* in gadget, this is called by run.c (gadget code makes all variables global)  */
/* gasoline opaqueness! PKD is already a typedef struct *pointer (to struct)    
 * inInitFluxLimitedDiffusionStruct is not because for obviousness */
/* keep this init routine code wrapper layer as short as practical
        because it must be rewritten for every new hydro code */

  /* inCodeParams should contain all hydro code parameters needed by the library,
     passed directly from master in gasoline,
     copied from "All" in gadget
  */
void pkdInitDiaphane(MDL mdl, PKD pkd, struct inInitDiaphaneStruct *inCodeParams) 
{
/* Initialize library, getting context struct */
  DiaphaneContext *diaphane_context = dpnInit();
  
  /* Init Context supplied by code */
  SetSimContext(diaphane_context->sim, inCodeParams);
  // Rescale units to redshift for cosmological runs // gasoline vs gadget velocity scaling is different

  printf("pkd init diaphane modules. dpn param filename   %s %lu\n",inCodeParams->param_file_name, sizeof(inCodeParams->param_file_name) );

  strcpy(diaphane_context->param_file_name, inCodeParams->param_file_name);
//  printf("checking again ........pkd init diaphane will read file %s  checking size %lu  checking again %s %lu\n",diaphane_context->param_file_name, sizeof(diaphane_context->param_file_name), inCodeParams->param_file_name, sizeof(inCodeParams->param_file_name) );


  // Get remaining units and read dpn param file
  //  printf("init diaphane units\n");
  dpnInitUnits(diaphane_context);


  /* store Diaphane context struct as part of pkd */
  pkd->dpn_context = diaphane_context;  
  
 // Init each module to be used -- (create context struct)
  #ifdef DIAPHANE_STARRAD
  if(inCodeParams->bGasStarrad == 1)    // these module switches should be part of diaphane context?
  {
    printf("init module for starrad\n"); fflush(stdout);
    // printf("gasoline wrapper mdl debugging  info    mdl rank %d nranks %d\n",mdl->idSelf,mdl->nThreads);
    // Temporarily pass mdl in here for STARRAD
    diaphane_context->mdl = mdl;  // should do this earlier in case more than 1 module needs mdl

//    unsigned module_choices = STARRAD;
//    dpnInitModules(diaphane_context, module_choices);
    dpnInitModules(diaphane_context, DPN_STARRAD);
  }
#endif
#ifdef DIAPHANE_FLD
  if(inCodeParams->bGasFluxLimitedDiffusion == 1)
  {
    printf("init module for fld.  will read param file %s\n",diaphane_context->param_file_name); fflush(stdout);
    //printf("wrapper dpn_context: %s pointers %p %p %p %p %p %p %p %p\n",diaphane_context->param_file_name,(void *)&diaphane_context,(void *)&diaphane_context->sim,(void *)&diaphane_context->units,(void *)&diaphane_context->opacity_context,(void *)&diaphane_context->fld,(void *)&diaphane_context->starrad,(void *)&diaphane_context->mdl,(void *)&diaphane_context->param_file_name);///////////// debugging 
//    unsigned module_choices = FLD;
//    dpnInitModules(diaphane_context, module_choices);
    dpnInitModules(diaphane_context, DPN_FLD);
  }
#endif
}


/* length, mass, velocity, time.  Enough to define all other units.
        all assuming z=0 (scale_factor=1) */
  /* time unit must be defined.  2 example methods:
      1- from length unit and mass unit, and setting G --gasoline method
      2- length unit and velocity unit. G is not set explicitly --gadget method 
  */
/* Should we have multiple versions for each type of units and comment one out? or should we could make a function pointer to specify version?  */
/* Gasoline version */  
void SetSimContext(DiaphaneSimContext *dpnsim,  struct inInitDiaphaneStruct *sim) 
{
  // Fill the diaphane SimContext structure with code variables
  double G_sim = 1.; // gasoline convention
  
dpnsim->ConstGamma       = sim->dConstGamma; 
  dpnsim->MeanMolWeight    = sim->dMeanMolWeight; 
  dpnsim->periodic         = sim->bPeriodic;
  dpnsim->scale_factor     = 1.;
  dpnsim->comoving    = 0;
  if(sim->bComove)
    dpnsim->comoving = 1; 
  printf("setting diaphane context\n");  fflush(stdout);
  /* With gasoline, we assume that the user has set the mass and length unit in the main sim param file 
 -- otherwise the simulation has arbitrary "natural" units (which make no sense for hydro runs), o
 f Mbox = omega_matter, G=1, H=(8Pi/3)**(1/3) as is convention for gravity-only gasoline runs */

  // Do base units needed for diaphane to create a units context
  //  1 sim length unit == lengthunit_cgs_z0 (at z=0 only for cosmo runs)
  dpnsim->lengthunit_cgs_z0 = sim->dKpcUnit * get_kpc_cgs();
  // 1 sim mass unit == massunit_cgs
  dpnsim->massunit_cgs     = sim->dMassUnit * get_solarmass_cgs();
  // time unit from length unit and velocity unit -- gasoline convention 
  dpnsim->timeunit_cgs_z0  = get_timeunit_in_seconds_givenG(dpnsim->lengthunit_cgs_z0, dpnsim->massunit_cgs, G_sim); // gasoline method
  // Velocity unit could be done in diaphane itself if scale factor is uniform across codes
  // E.g. if we set for gadget scale_factor = 1/scale_factor, and do proper_velocity = velocity_unit * scale_factor;
  dpnsim->velocityunit_cgs_z0 = dpnsim->lengthunit_cgs_z0 / dpnsim->timeunit_cgs_z0;
//  dpn->velocityunit_cgs = get_proper_velocityunit_gasoline(dpn->velocityunit_cgs_z0, dpn->scale_factor);

  /* store periodic boundaries */
  if (dpnsim->periodic)
  {
    dpnsim->periodx_z0 = sim->dxPeriod * dpnsim->lengthunit_cgs_z0;  /* box period in cgs */
    dpnsim->periody_z0 = sim->dyPeriod * dpnsim->lengthunit_cgs_z0;
    dpnsim->periodz_z0 = sim->dzPeriod * dpnsim->lengthunit_cgs_z0;
  }
  else 
  {
    dpnsim->periodx_z0 = 0;  
    dpnsim->periody_z0 = 0;
    dpnsim->periodz_z0 = 0;
  }

  dpnsim->periodx = dpnsim->periodx_z0; // initialize z>0
  dpnsim->periody = dpnsim->periody_z0;
  dpnsim->periodz = dpnsim->periodz_z0;
}



/* some inits (and units rescaling for comoving runs)   */
// maybe we should re-organize so that this calls the init for each module?
void pkdInitDiaphaneStep(MDL mdl, PKD pkd, struct inInitDiaphaneStepStruct *in)
{
  DiaphaneContext *diaphane_context;

  diaphane_context = pkd->dpn_context; 
  diaphane_context->sim->scale_factor = in->scale_factor;
  
  if(diaphane_context->sim->comoving)  // need to go to pkd level of code to access diaphane_context
        {
          diaphane_context->sim->scale_factor = in->scale_factor;
          rescale_comoving_to_physical_units_with_redshift_gasoline(diaphane_context);  // rescales length, vel, time, period
          compute_diaphane_units_from_base(diaphane_context->units); // rescales all other units 
        }
  else    // non-comoving (non-cosmological runs)
        {
          diaphane_context->sim->scale_factor = 1.;
        }

}





void dpnUpdateu(PKD pkd, float dt_length, int radiationModel)  
{
  //printf("Diaphane: dpnUpdateu()\n");
  // Update internal energy of each particle
  PARTICLE* p = pkd->pStore;
  long i;
  for(i = 0; i < pkdLocal(pkd); i++)
  {
    if (i == 0) printf("part - updating u %g %g\n",p[i].sruDot,p[i].flduDot);  
    if(!is_gasoline_particle_active(p)) continue;  // inactive particle nothing to do
    switch(radiationModel)
    {
#ifdef DIAPHANE_STARRAD
      case DPN_STARRAD:
       dpnUpdateInternalEnergy(&p[i].u, dt_length, p[i].sruDot);
        // Update uPred with dTime also, is this correct? yes for kdk but for dkd would be duPredDelta = duDelta/2 (as in master.c)
        dpnUpdateInternalEnergy(&p[i].uPred, dt_length, p[i].sruDot);
        // if (i == 0) printf("part - updating starrad u dt udot %g %g %g\n",p[i].u, dt_length, p[i].sruDot);   ///////////// debugging
      break;
#endif
#ifdef DIAPHANE_FLD     
      case DPN_FLD:
        dpnUpdateInternalEnergy(&p[i].u, dt_length, p[i].flduDot);
        // Update uPred with dTime also, is this correct? yes for kdk but for dkd would be duPredDelta = duDelta/2 (as in master.c)
        dpnUpdateInternalEnergy(&p[i].uPred, dt_length, p[i].flduDot);
        // if (i == 0) printf("part - updating fld u dt udot %g %g %g\n",p[i].u,dt_length,p[i].flduDot);   ///////////// debugging
      break;
#endif
    }

  }


}
  

  
  /*   active on this time(sub)step   */
int is_gasoline_particle_active(PARTICLE *ppart)
{
  if(TYPEQueryACTIVE(ppart))  
    {
      return 1;
    }
  return 0;
}




// These dont appear to be used
// AFLOAT GetTimeUnitGasoline(inDiaphaneStruct *inCodeParams, DiaphaneContextStruct *diaphanecontext) 
// {
//   double G_sim; 

//   G_sim = 1.; /* G=1 convention for gasoline --could make part of fldcontext */

//   /* time unit from length unit and mass unit and G -- gasoline  */
//   fldcontext->timeunit_cgs_z0 = get_timeunit_in_seconds_givenG(fldcontext->lengthunit_cgs_z0, fldcontext->massunit_cgs, G_sim); 

// }

// AFLOAT GetTimeUnitGadget(inDiaphaneStruct *inCodeParams, DiaphaneContextStruct *diaphanecontext) 
// {
//    /* time unit from length unit and velocity unit -- gadget */
//   fldcontext->timeunit_cgs_z0 = get_timeunit_in_seconds_givenvelunitcgs(fldcontext->lengthunit_cgs_z0, fldcontext->velocityunit_cgs_z0); 
// }






