#ifndef DPN_GASOLINEWRAPPER_H
#define DPN_GASOLINEWRAPPER_H

/*
 Diaphane common gasoline wrapper
 final layer between gasoline wrapper and starrad itself
 each function called per processor:
 	msr -> pst -> via mdl for parallel calls -> here
*/
	

/* pkd headers */
#include "pkd.h"
#include "smoothfcn.h"

/* diaphane headers */
#include "diaphane.h"
#include "dpngasolinewrapper_types.h"
void pkdInitDiaphane(MDL mdl, PKD pkd, struct inInitDiaphaneStruct *);
void SetSimContext(DiaphaneSimContext *dpn, struct inInitDiaphaneStruct *);
//AFLOAT GetTimeUnitGasoline(inDiaphaneStruct *inCodeParams, DiaphaneContext *diaphanecontext);
//AFLOAT GetTimeUnitGadget(inDiaphaneStruct *inCodeParams, DiaphaneContext *diaphanecontext);
void pkdInitDiaphaneStep(MDL mdl, PKD pkd, struct inInitDiaphaneStepStruct *);

void dpnUpdateu(PKD pkd, float dTime, int radiationModel);
int is_gasoline_particle_active(PARTICLE *ppart);

#endif
