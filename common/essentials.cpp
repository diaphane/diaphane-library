/* fld.cpp */

// TODO: clean up this file -- much of it is already in opacity.cpp (where they belong).  other funcs were copied from fldgen.cpp and need to be removed from there.

/* This file contains "essential" auxiliary funtions needed for diaphane library modules   */


/* single versus double precision:
    in cgs units --- mass can easily exceed the ~10**38 floating point limit 
      and other variables distance**n, etc can also
      so make everything double?
*/

/* assumes all particles have same kernel -- Is that a problem for SPH with different smoothing lengths? */

/* 

Equations can be found in Whitehouse & Bate 2004, MNRAS, 353, 1078
or   Tuner & Stone 2001, ApJS, 135, 95
This method is described in Mayer et al. 2007, ApJ, 661, L77
FLd for sph method based on Cleay and Monaghan 1999, Journal Comp Phys, 148, 227

* Note that the assumption of Tgas = Tradiation for this FLD method implicitly assumes that the gas is optically thick.

*/

//This whole file only makes sense for simulations with gas

#include <cassert>
#include <cstdio>
#include <fstream>
#include <vector>
#include <iterator>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>

#include "Interpolate.h" /* c++ functions */
//#include "Vector3D.h"    /* c++ functions */


//#include "fld_types.h" // structs, AFLOAT (single or double)
//#include "fldgen.h"
#include "diaphane_types.h"
#include "essentials.h"


/* //Could be used to encapsulate C++ stuff from C side and obviate need for casts in this file
struct FLDContext {
	auto_ptr<CodeUnits> units;
	auto_ptr<BilinearInterpolator<double> > opacityInterpolator;
	STIFF sbs;
	auto_ptr<FLDDerivsData> derivsData;
};

and could put the context struct in c++
  //auto_ptr<DiffusionContext> d(new DiffusionContext);
  //  DiffusionContext* d = new DiffusionContext; 

*/



/******  the kernel *********/

/* this is the unnormailed kernel ***/
/* Monaghan & Latanzio 1995, B-spline with compact support, 
 *    eqn 21 (with the typo from that paper fixed to be nu**2??)! 
 *  (or reference Monaghan 1992, p. 554 (section 7)
 *  nu_squared =(gasoline ar2) = (r/h)**2 (nu=r/h=sqrt(ar2))
 *   e.g. W(r,h) = kernelnonorm / pi / h**3  (comoving)
 * x 1/2 for symmetric calc
 */
/** w(r,h) = 8/pi/h**3 * 1-6(r/h)**2+6*(r/h)**3  (r/h <= 1/2)  (h==2rsmooth)
 **   = 8/pi/h**3 * 2(1-r/h)**3          (1/2 < r/h <+ 1)  **/
/* or   w(nu) = 1/pi/h**3 * [1 - 3/2 nu**2 + 3/4 nu**3]  nu<1 (h==rsmooth) 
              = 1/pi/h**3 * 1/4(2-nu)**3             1<nu<2        
*/
AFLOAT bspline_kernel_nonorm(AFLOAT nusquared) 
{
  AFLOAT ak, result;
  ak = 2.0 - sqrt(nusquared);
  if ((nusquared) < 1.0) 
    {
      result = (1.0 - 0.75*ak*(nusquared)); 
    }
  else 
    {
      result = 0.25*ak*ak*ak; 
    }
  return result;   /* typically order ~1 */
}


/* this is the unnormalized kernel gradient / nu  (DKERNEL in gasoline) ***/
/* Monaghan & Latanzio 1995, B-spline with compact support, 
 * nusquared = (r/h)**2  (h=="rkernel" or "smoothing length")

 * gradW(nu) = 1/pi/h**4 * [-3nu + 9/4 nu**2]      nu<1 (h==rsmooth) 
             = 1/pi/h**4 * [-3/4(2-nu)**2]       1<nu<2        

     e.g.  gradW(nu) = gradkernelovernunonorm nu/pi/h**4 (*a for proper coords)
     gradW/r = gradW/nu/h
 *   e.g. gradW(r,h)/r = gradkernelovernunonorm /pi / h**5 (*a for proper coords) (=gasoline "rs1")
 * x1/2 for symmetric calc
              gradW(r,h)/r   = gradkernelovernunonorm*M_1_PI*ih2*ih2/ph*a
 */
AFLOAT bspline_grad_kernel_over_nu_nonorm(AFLOAT nusquared) 
{ 
  AFLOAT nu, result;
  nu = sqrt(nusquared);
  if ((nusquared) < 1.0)  
    { 
      result = -3. + 2.25*nu;  
    } 
  else 
    {
      result = -0.75*(2.0-nu)*(2.0-nu)/nu; 
    }   	
  return result; /* typically order ~1 */
}



/** periodic boxes **/
AFLOAT get_separation_1d(AFLOAT x1, AFLOAT x0, int periodic, AFLOAT period)
{
  AFLOAT dx;
  dx = x1 - x0;  
  if (periodic == 1)  /* check particle not across periodic box */
    {                 /* ideally (perhaps) we already ensured that */
      if (dx > period/2.)
	{
	  dx -= period;
	}  
      else if (dx < -period/2.) 
	{
	  dx += period;
	}
    }
  return dx;
}








/* generic initialize every active particle */
/* in e.g. gadget, the flduDot is actually fldEntropyDot */
void initDiaphaneParticledata(AFLOAT *uDot, AFLOAT *dtmax) 
{
  *uDot = 0.;
  *dtmax = HUGE_VAL;
}


/* If code does not compute gradient of energy, we could do it here */
/*********
void calcgradenergypairgen(AFLOAT dx, AFLOAT dy, AFLOAT dz, AFLOAT rs1, AFLOAT aFac, AFLOAT dentropy, AFLOAT* gradenergyincx, AFLOAT* gradenergyincy, AFLOAT* gradenergyincz)
{
  AFLOAT fconst;
  fconst = -rs1 * aFac * dentropy; */ /* (q->u - p->u);   */ /*
  *gradenergyincx = fconst * dx;
  *gradenergyincy = fconst * dy;
  *gradenergyincz = fconst * dz;
}
******/







/* 
 *  compute gradient of kernel over separation (gradW/r) for a particle pair
 *  the separation between 2 particles convolved with the 
 *     the kernel "smoothing" function 
 *     (sum this return over neighbor pairs externally in calling function)
 * 
 *
 *    
 */
AFLOAT calc_grad_kernel_over_r(grad_kernel_overnu_func_type grad_kernel_overnu_func, int periodic, AFLOAT periodx, AFLOAT periody, AFLOAT periodz, AFLOAT* pos, AFLOAT* pos_neighbor, AFLOAT rkernel) // (too many function args?)
{
  AFLOAT dx, dy, dz;
  AFLOAT nu_squared, fDist2, ih2, fNorm; 
  AFLOAT grad_kernel_over_r;
  //  AFLOAT fDist;

  ih2 = 1./rkernel/rkernel; 
  fNorm = M_1_PI * ih2 * ih2 /rkernel;  /* M_1_PI==(1/pi)  const/h**5  */
  // fNorm = 0.5 * M_1_PI * ih2 * ih2 /rkernel; // x 0.5 for symmetric calc?
  //   fNorm = M_1_PI * ih2 * ih2;   /* M_1_PI==(1/pi)   fnorm=1/pi/h**4  */

  dx = get_separation_1d(pos_neighbor[0],pos[0],periodic,periodx);
  dy = get_separation_1d(pos_neighbor[1],pos[1],periodic,periody);
  dz = get_separation_1d(pos_neighbor[2],pos[2],periodic,periodz);

  /* careful: nan, exceeding floating point limits is possible */
  fDist2 = dx*dx + dy*dy + dz*dz;
  //  fDist = sqrt(fDist2);
  nu_squared = fDist2 * ih2; /* r**2/h**2 */
  /* (unnormalized) gradW / nu    /pi/h**5  */
  // grad_kernel_over_r = grad_kernel_overnu_func(nu_squared)*fNorm/fDist;      
  grad_kernel_over_r = grad_kernel_overnu_func(nu_squared)*fNorm;   
  return grad_kernel_over_r;
}




  /* 
   *  convert internal energy (sim units) to temperature (Kelvin)
   */
AFLOAT energysimunit_to_temperature(AFLOAT temperature_to_u_simunit, AFLOAT energy)
{
  AFLOAT temperature;
  temperature = energy/temperature_to_u_simunit;
  return temperature;
}


/* forces Temp. into the range of opacity table limits */
AFLOAT check_temperature_in_range(AFLOAT temperature, AFLOAT mintemperature, AFLOAT maxtemperature)
{
  if(temperature < mintemperature)
    {
      temperature = mintemperature;
    }
  if(temperature > maxtemperature)
    {
      temperature = maxtemperature;
    }
  return temperature;
}





//*** this assumes  dtFLD is just the min of its dtFLD for each neigbhor not the (more sensible?) summed quantity its off-process virtual counterpart  **/
void CombineThisParticleWithVirtualUdot(AFLOAT* uDot1, AFLOAT* dt1, AFLOAT uDot2, AFLOAT dt2)
{
  /* note "1" is a pointer because need to modify its values, "2" is not */
  *uDot1 += uDot2;
  /* optional: FLD timestep criterion can be shortest timestep of each pair */  
  if(dt2 < *dt1) 
    {
      *dt1 = dt2;
    }
}

/* 
   once per timestep
   allocates the neighbor ID list for the FLD calculation */
/* could instead just use part of the FLD data block  */
/* note -- using 4byte ints limits use to runs with <= 2**31 particles */
/* gasoline does not use this function because already have neighbor list */
INTIDTYPE* AllocateNeighborIDList(int nmaxneighbors)
{
  int sizeofdataarray;
  INTIDTYPE *neighbor_id_list;

  /* nneighbors+1 for 2nd copy of "this" particle in i=0 */
  sizeofdataarray = (nmaxneighbors+1)*sizeof(*neighbor_id_list);
  
  neighbor_id_list = (INTIDTYPE *)malloc(sizeofdataarray); 
  if (neighbor_id_list == NULL)
    {
      printf("Failed to allocate memory for neighbor id list\n");
      exit(1); 
    } 
  return neighbor_id_list;
}







