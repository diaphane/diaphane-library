#ifndef DIAPHANE_TYPES_H
#define DIAPHANE_TYPES_H

#ifdef DIAPHANE_STARRAD
#include <mpi.h>
#include "mdl.h"
#endif


#include <sys/param.h> /* for MAXPATHLEN and MAXHOSTNAMELEN, if available */
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // careful not to redefine system variable
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DIAPHANE_FLD
#include "fld_types.h" 
#endif

#ifdef DIAPHANE_STARRAD
#include "sr_types.h"
//#include "mdl.h"
#endif

#include "opacity.h"

// INTIDTYPE should be a makefile option
typedef int INTIDTYPE; // 32 bit integer IDs  // should not be actual IDs but i = 0 N on local process
// typedef long long INTIDTYPE; // 64 bit integer IDs (needed only if if Npart (on local process) > 2**31)



/* This enum is a bitmap, so if you add a RADMODEL ensure it is set to 2x the previous model */
typedef enum {
  DPN_STARRAD  = 1,
  DPN_FLD      = 2,
} RADIATION_MODEL;

#ifndef AFLOAT    // maybe we should just use double everywhere?
//  typedef float AFLOAT;   // single precision  
typedef double AFLOAT;   // double precision       
#endif


/* general simulation parameters */ // could combine with units struct
/* TODO: should enforce that none of these can change with time, except for scale_factor */
typedef struct DiaphaneSimContext
{
  double ConstGamma;
  double MeanMolWeight;
  int comoving;
  int periodic;
  double lengthunit_cgs_z0;  /* the _z0 units are rescale for comoving runs */
  double timeunit_cgs_z0;
  double massunit_cgs;
  /* The two velocities could be solely in DiaphaneUnits */
  double velocityunit_cgs_z0; 
  // double velocityunit_cgs;
  double scale_factor; /* not a constant for comoving runs, so many units also not a constant */
  double periodx_z0;  /* the _z0 quantities are rescaled for comoving runs */
  double periody_z0;
  double periodz_z0;
  double periodx; /* rescaled for comoving (from the _z0 z=0 version)  */
  double periody;
  double periodz;
  double min_temp_cgs;
  double max_temp_cgs;
} DiaphaneSimContext;

/* units and general simulation parameters  
 *   some of these can change with time 
 *    */
typedef struct DiaphaneUnits
{
  double ConstGamma;           // some of these variables are repeated here for convenience -- make sure they are constant
  double MeanMolWeight;       
  double lengthunit_cgs_z0;  /* the _z0 units are rescale for comoving runs */
  double timeunit_cgs_z0;
  double velocityunit_cgs_z0;
  //double dMassUnit; ??
  double lengthunit_cgs; /* rescaled for comoving (from the _z0 z=0 version) */
  double massunit_cgs;
  double timeunit_cgs;
  double velocityunit_cgs;
  double StefanBoltzmannConstantCGS;
  double temperature_to_internalenergy_simunit;
  double temperature_to_internalenergy_cgs;
  double densityunit_cgs;
  double gradenergyunit_cgs;
  double pressureunit_cgs;
  double opacityunit_cgs;
  double heatingunit_cgs;
} DiaphaneUnits;



/*
Diaphane context struct
Containing FLD context and starrad context and other modules context...     
*/
typedef struct DiaphaneContext
{
  DiaphaneSimContext  *sim;
  DiaphaneUnits       *units;
  OpacityContext     *opacity_context;
#ifdef DIAPHANE_FLD           // should we force the struct to contain pointers for all modules? because the simulation code must know the struct to use it. and it is dangerous to require library and sim to use the same compile flags
  struct FluxLimitedDiffusionContext *fld;
#endif
#ifdef DIAPHANE_STARRAD
  struct sr_context          *starrad;
  MDL mdl;
#endif
char param_file_name[MAXPATHLEN];
} DiaphaneContext;

typedef AFLOAT (* grad_kernel_overnu_func_type)(AFLOAT);

#ifdef __cplusplus
}
#endif

#endif
