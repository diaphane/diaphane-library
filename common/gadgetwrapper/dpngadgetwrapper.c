/*
 Diaphane gadget wrapper c file
 Final layer between gasoline wrapper and diaphane itself.  
 Compiled as part of the gadget code.
 each function called per processor:
 Functions in this file are called directly from the main simulation code.
*/

#include "dpngadgetwrapper.h"
#include "diaphane_types.h"
#include "diaphane.h"
#include "setunits.h"
#include <string.h>
#include <math.h>
//#include <fenv.h>
#ifdef DIAPHANE_FLD
#include "fldgadgetwrapper.h"
#endif
#ifdef DIAPHANE_STARRAD
//#include "srgadgetwrapper.h"
#endif
#include "essentials.h"


/* Init Diaphane Library - once at start of run.
                           set units, parameters, persistent memory, etc.
            Driver function.                             */

void GadgetInitDiaphane()
{
  int something_to_do;
  something_to_do = 0;

#ifdef DIAPHANE_STARRAD
  if (All.do_starrad == 1) something_to_do = 1;
#endif
#ifdef DIAPHANE_FLD
  if (All.do_flux_limited_diffusion == 1) something_to_do = 1;
#endif

  if (something_to_do == 0) 
    {
      printf("warning -- No Diaphane modules specified to run -- skipping Diaphane Init\n");
      return;
    }
  
  DiaphaneContext *diaphane_context = dpnInit();  // create the context struct
  printf("Diaphane library context initialized\n");
  All.DiaphaneContextStruct = diaphane_context;


  /* Init Context supplied by code -- sim params and base units */
  SetSimContext(diaphane_context->sim);

  // Get parameter file name
  //NOTE: max path lengthMAXLEN_FILENAME in code should match that of dpn library MAXPATHLEN
  strcpy(diaphane_context->param_file_name, All.diaphane_param_file);
  printf("to set up diaphane context: will read param file %s %s\n",diaphane_context->param_file_name, All.diaphane_param_file);


  // Get remaining units and read dpn param file
  dpnInitUnits(diaphane_context);

  // Init each module to be used -- (create context struct)
#ifdef DIAPHANE_STARRAD
  if(All.do_starrad == 1)
  {
    printf("starrad not supported yet for gadget.  exiting.\n");
    exit(1);
    dpnInitModules(diaphane_context, DPN_STARRAD);
  }
#endif
#ifdef DIAPHANE_FLD
  if(All.do_flux_limited_diffusion == 1)
  {
    printf("init fld for gadget2. param file %s\n",diaphane_context->param_file_name);
    dpnInitModules(diaphane_context, DPN_FLD);
  }
#endif    
}



  /* Call Diaphane Step */
/* All diaphane physics lives on the same timestep */
void GadgetDoDiaphaneStep()  // TODO: can we use All.timestep for dtime of particle i
{
  AFLOAT timestep_cgs;
  DiaphaneContext *diaphane_context;

  diaphane_context = All.DiaphaneContextStruct; 
//  timestep_cgs = All.timestep * diaphan_context->units->timeunit_cgs; // rates are in sim units here



  /* For cosmological runs: Comoving (scales with redshift) versus proper units: 
           convert all units every step to proper units (then convert the result back to code units) */
  if(diaphane_context->sim->comoving)
	{
	  diaphane_context->sim->scale_factor = All.Time;
          rescale_comoving_to_physical_units_with_redshift_gadget(diaphane_context);  // rescales length, vel, time, period
	  compute_diaphane_units_from_base(diaphane_context->units); // rescales all other units 
	}
  else    // non-comoving (non-cosmological runs)
	{
	  diaphane_context->sim->scale_factor = 1.;
	}

  /* Here we call msrDo_module_Step (which includes any init and finish module routine) */
#ifdef DIAPHANE_STARRAD
  printf("Checking if we should do STARRAD step\n");
  if(All.do_starrad) 
    {
      printf("starrad not supported yet for gadget.  exiting.\n");
      exit(1);
      //      printf("Doing STARRAD step\n");
      // Probably something about scaling here a la fld below
      //      DoStarradStep(msr, dTime);
   
      // Update particle temps
      //      dpnUpdateu(msr, dDelta, DPN_STARRAD);
 
      // update particle timesteps here?
      //      dpnUpdateTimestep
   }
#endif
  /// TODO -- finish below function calls 
#ifdef DIAPHANE_FLD
  if(All.do_flux_limited_diffusion)
    {
      printf("Doing Flux Limited Diffusion step\n");

      //  includes init fld step and finish fld step
      // smooth needs dTime.  scale_factor needed for units.
      DoFluxLimitedDiffusionStep(diaphane_context);

      // update temperatures
      printf("updating energies from FLD\n");
      dpnUpdateEntropy(All.TimeStep, DPN_FLD);
      

      // update particle timesteps here?
      // msrDpnUpdateTimestep
    }
#endif
}





/* Fill in Simulation Context Struct.  Gadget version.  
     Get any simuatation parameters needed for diaphane calculations.
     Fill in some variables from gadget struct.  convert base units gadget to cgs.  
                          */
void SetSimContext(DiaphaneSimContext *sim_context)
{
 
  sim_context->ConstGamma = GAMMA;  // gadget global variable
    
   /* keeping with GADGET convention for MEANMOLWEIGHT (should make it a user input parameter)  HYDROGEN_MASSFRAC was de
fined as 0.76 by gadget allvars.h */
  sim_context->MeanMolWeight = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);   /* note: we assume neutral gas here */

  sim_context->comoving = 0; 

//#ifdef LEAN_GADGET2 // millennium gadget2 version 
 //fldcontext->comoving = 1;
//#endif
//#ifdef PUBLIC_GADGET2
  if (All.ComovingIntegrationOn == 1)   // sim units scale with redshit
    {
      sim_context->comoving = 1;  
    }  
//#endif
  sim_context->periodic = 0; 
  if (All.PeriodicBoundariesOn == 1)
    {
      sim_context->periodic = 1;  ; 
    }
  


  sim_context->scale_factor     = 1.; // initialize to default non-comoving 

  /* Convert Gadget units to cgs 
        HubbleParam == H0/100 km/s/Mpc -- always 1 for non-comoving runs  
        It would be cleaner to put these statements in a separate function.
                    */

  //  1 sim length unit == lengthunit_cgs_z0 (at z=0 only for comoving runs)
  sim_context->lengthunit_cgs_z0 = (AFLOAT) (All.UnitLength_in_cm/All.HubbleParam) ;

  // 1 sim mass unit == massunit_cgs
  sim_context->massunit_cgs = (AFLOAT) (All.UnitMass_in_g/All.HubbleParam); 

 //  1 sim length unit == velocityunit_cgs_z0 (at z=0 only for cosmo runs)
 sim_context->velocityunit_cgs_z0 = (AFLOAT) All.UnitVelocity_in_cm_per_s; // seems to be cm/s not cm/h/s

  /* get time unit from length unit and velocity unit -- (e.g. for Gadget2) */
  sim_context->timeunit_cgs_z0 = get_timeunit_in_seconds_givenvelunitcgs(sim_context->lengthunit_cgs_z0, sim_context->velocityunit_cgs_z0);

  /* get time unit from length unit and mass unit -- (e.g. for gasoline)  */
  /*   sim_context->timeunit_cgs_z0 = get_timeunit_in_seconds_givenG(sim_context->lengthunit_cgs_z0, sim_context->massunit_cgs, G_sim); */ 


  /* Gadget2 periodicicity only allows cubes */
  if (sim_context->periodic == 1)
    {
      sim_context->periodx_z0 = All.BoxSize * sim_context->lengthunit_cgs_z0;  /* box period in cgs!!! */  
    }
  else
    {
      sim_context->periodx_z0 = 0.;
    }  
  sim_context->periody_z0 = sim_context->periodx_z0;
  sim_context->periodz_z0 = sim_context->periodx_z0;

 
  sim_context->periodx = sim_context->periodx_z0; // initialize z>0
  sim_context->periody = sim_context->periody_z0;
  sim_context->periodz = sim_context->periodz_z0;

 
//  sim_context->simunittype = 2;  // 1-gasoline.  2-gadget.  3-sphynx. // is this needed?
  ////  gadget code has a minimum but not a maximum Temperature
  // TODO: ? make sure these limits are read from the diaphane .param file 
//  sim_context->fixed_temp_limits = 1; // should we impose a temp min/max this way?  
  sim_context->max_temp_cgs = HUGE_VAL;
  sim_context->min_temp_cgs = All.MinGasTemp; 

}



// Free up context memory 
void GadgetFinishDiaphane()
{
  DiaphaneContext *diaphane_context;
  // Free (common) context for each module to be used 
  diaphane_context = All.DiaphaneContextStruct; // 
#ifdef DIAPHANE_STARRAD   
  if(All.do_starrad == 1)
  {
    printf("starrad not supported yet for gadget.  exiting.\n");
    exit(1);
    dpnFinishModules(diaphane_context, DPN_STARRAD);
  }
#endif
#ifdef DIAPHANE_FLD
  if(All.do_flux_limited_diffusion == 1)
  {
    dpnFinishModules(diaphane_context, DPN_FLD);
  }
#endif  
  dpnFinish(diaphane_context);
}



// TODO:  e.g. update entropy not just DtEntropy
/* update the entropydot for every active particle */
void dpnUpdateEntropy(float dTime, int radiationModel)
{
  int i;
  for(i = 0; i < NumPart; i++)  // NumPart is number of particles on the LOCAL processor
    {
      if(is_gadget_particle_active(i))  // else, ignore, non-active particle
	{      
	  switch(radiationModel)
	    {
#ifdef DIAPHANE_STARRAD
	    case DPN_STARRAD:
	      printf("starrad not supported yet for gadget.  exiting.\n");
		exit(1);	  
//
//	      if(SphP[i].srEntroyDot * dTime > -0.5 * SphP[i].Entropy)  // gadget prevents overcoolling with something like this.
// 		dpnEvolveVariableSingle(&SphP[i].DtEntropy, (float)dTime, (float)SphP[i].srEntropyDot);	  
//	      else 
//		SphP[i].Entropy *= 0.5;
	      break;	    
#endif	    
#ifdef DIAPHANE_FLD     // TODO: should add entropy ceiling and floor here.  For now just update total DtEntropy and gadget evolves Entropy 
	    case DPN_FLD:	      
/////////	      if(SphP[i].EntropyDotFLD * dTime > -0.5 * SphP[i].Entropy) // gadget prevents overcoolling with something like this.
////////// 		dpnEvolveVariableSingle(&SphP[i].Entropy, (float)dTime, (float)SphP[i].EntropyDotFLD);	  
		SphP[i].DtEntropy += SphP[i].EntropyDotFLD;  // Entropy will update directly in gadget
////////////	      else  // something is not-optimal
//		SphP[i].Entropy *= 0.5;
	      // Could impose a temperature floor here as in gadget
	      
	      // TODO?: if timestep increases, make sure do not overcool, as in gadget
	      //  dt_entr = ti_step / 2 * All.Timebase_interval;
	      // if(SphP[i].Entropy + SphP[i].DtEntropy * dt_entr < 0.5 * SphP[i].Entropy)
	      //     SphP[i].DtEntropy = -0.5 * SphP[i].Entropy / dt_entr;
	      break;
#endif	      
	    }
	}
    }
}



/* IDs should be adjustable 32 or 64 bit ints, for runs >> 2**31 particles */
/* gadget -- trim the list of neighbors in a cube to a sphere  */
int neighbor_id_list_cube_to_sphere(SIMFLOAT posx0, SIMFLOAT posy0, SIMFLOAT posz0, void* neighbor_id_list_voidpointer, int numngb, SIMFLOAT rmax_squared, int periodic, SIMFLOAT periodsimunits)
{
  int j;
  int ineighbor;
  int neighborindex;
  SIMFLOAT posx1, posy1, posz1; // using SIM precision (.e.g single in gadget)
  SIMFLOAT dx, dy, dz;
  SIMFLOAT distancesquared;
  INTIDTYPE* neighbor_id_list;
  //  AFLOAT fDist2;

  neighbor_id_list = neighbor_id_list_voidpointer;

  ineighbor = 0;
  for(j = 0; j < numngb; j++) /* all neighbors in a "cube" */
    {
      neighborindex = Ngblist[j];  //  this should be the 0-N index not the actual Particle ID
 
      /*  Gadget2 neighbors are always local -- only "this particle" can be virtual */
      posx1 = P[neighborindex].Pos[0];
      posy1 = P[neighborindex].Pos[1];
      posz1 = P[neighborindex].Pos[2];  


      /*      make a spherical neighbor list from a cubical neighbor list  */
      dx = get_separation_1d(posx1,posx0,periodic,periodsimunits);
      dy = get_separation_1d(posy1,posy0,periodic,periodsimunits);
      dz = get_separation_1d(posz1,posz0,periodic,periodsimunits);
      
      distancesquared = dx*dx + dy*dy + dz*dz;
      if (distancesquared < rmax_squared)
	{
	  neighbor_id_list[ineighbor] = neighborindex;
	  ineighbor++;
	}      
    }
  return ineighbor;
}



/*  This version uses the Gadget2 GAMMA_MINUS1 hard coded variable 
one should usually use: gadgetentropy_to_gadgetenerg in setunits.cpp
   wherin gamma and scale_factor is used from dpncontext  */
SIMFLOAT gadgetentropy_to_gadgetenergy_gadget_style_version(SIMFLOAT entropy, SIMFLOAT density)
{
  //  AFLOAT proper_density;
  SIMFLOAT energy;

  if (All.ComovingIntegrationOn) /* proper density != code density */
    {
      density /= (All.Time*All.Time*All.Time);
    }

  /* in gadget GAMMA, GAMMA_MINUS1 is hard-coded in allvars.h */  
  energy = (AFLOAT)entropy_to_energy((AFLOAT) entropy, (AFLOAT) density, GAMMA_MINUS1);
  return energy; /* units of velocity**2 */
}


