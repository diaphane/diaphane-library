#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"
//#include "protofld.h"
#include "fld_types.h"
#include "fldgadgetwrapper.h"

/*! \file gradentropy.c
 *
 *  using framework of hydra.c 
 *   -- computes gradient of internal energy (vector) per particle 
 *        needed for flux limited diffusion using all the 
 *        standard machinery of gadget2
 *    --uses Sph entropy value, then converts that to internal energy, then 
 *         uses that internal energy for an SPH gradient 
 *    --outputs gradient internal energy in gadget units 
 *      output units are:
 *
 *   --- !It would be faster to first compute all internal energies in a separte sph routine, then take the gradients here ---------!
 */


#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif



/*! This function is the driver routine for the calculation of gradient internal energy for all active particles.  code structure is just a copy of hydro_force.
 */
void compute_grad_energy(void)
{
  long long ntot, ntotleft;
  int i, j, k, n, ngrp, maxfill, source, ndone;
  int *nbuffer, *noffset, *nsend_local, *nsend, *numlist, *ndonelist;
  int level, sendTask, recvTask, nexport, place;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
  MPI_Status status;

#ifdef PERIODIC
  boxSize = All.BoxSize;
  boxHalf = 0.5 * All.BoxSize;
#ifdef LONG_X
  boxHalf_X = boxHalf * LONG_X;
  boxSize_X = boxSize * LONG_X;
#endif
#ifdef LONG_Y
  boxHalf_Y = boxHalf * LONG_Y;
  boxSize_Y = boxSize * LONG_Y;
#endif
#ifdef LONG_Z
  boxHalf_Z = boxHalf * LONG_Z;
  boxSize_Z = boxSize * LONG_Z;
#endif
#endif

  //  if(All.ComovingIntegrationOn)
  //    {
      /*  comoving integration */
  //  atime = All.Time;
  //  }


  /* `NumSphUpdate' gives the number of particles on this processor that want a force update */
  for(n = 0, NumSphUpdate = 0; n < N_gas; n++)
    {
      if(P[n].Ti_endstep == All.Ti_Current)
	NumSphUpdate++;
    }

  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumSphUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);


  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);


  i = 0;			/* first particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */

  while(ntotleft > 0)
    {
      for(j = 0; j < NTask; j++)
	nsend_local[j] = 0;

      /* do local particles and prepare export list */
      tstart = second();
      for(nexport = 0, ndone = 0; i < N_gas && nexport < All.BunchSizeGradEnergy - NTask; i++)
	if(P[i].Ti_endstep == All.Ti_Current) // active gadget particle
	  {
	    ndone++;

	    for(j = 0; j < NTask; j++)
	      Exportflag[j] = 0;

	    compute_grad_energy_particle(i, 0);

	    for(j = 0; j < NTask; j++)
	      {
		if(Exportflag[j])
		  {
		    for(k = 0; k < 3; k++)
		      {
			GradEnergyDataIn[nexport].Pos[k] = P[i].Pos[k];
		      }
		    GradEnergyDataIn[nexport].Hsml = SphP[i].Hsml;
		    GradEnergyDataIn[nexport].Density = SphP[i].Density;
		    GradEnergyDataIn[nexport].Entropy = SphP[i].Entropy;
		    GradEnergyDataIn[nexport].Index = i;
		    GradEnergyDataIn[nexport].Task = j;
		    nexport++;
		    nsend_local[j]++;
		  }
	      }
	  }
      tend = second();
      timecomp += timediff(tstart, tend);

      qsort(GradEnergyDataIn, nexport, sizeof(struct gradenergydata_in), gradenergy_compare_key);

      for(j = 1, noffset[0] = 0; j < NTask; j++)
	noffset[j] = noffset[j - 1] + nsend_local[j - 1];

      tstart = second();

      MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

      tend = second();
      timeimbalance += timediff(tstart, tend);



      /* now do the particles that need to be exported */

      for(level = 1; level < (1 << PTask); level++)
	{
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeGradEnergy)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* get the particles */
		      MPI_Sendrecv(&GradEnergyDataIn[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct hydrodata_in), MPI_BYTE,
				   recvTask, TAG_GRADENERGY_A,
				   &GradEnergyDataGet[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_in), MPI_BYTE,
				   recvTask, TAG_GRADENERGY_A, MPI_COMM_WORLD, &status);
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  /* now do the imported particles */
	  tstart = second();
	  for(j = 0; j < nbuffer[ThisTask]; j++)
	    compute_grad_energy_particle(j, 1);
	  tend = second();
	  timecomp += timediff(tstart, tend);

	  /* do a block to measure imbalance */
	  tstart = second();
	  MPI_Barrier(MPI_COMM_WORLD);
	  tend = second();
	  timeimbalance += timediff(tstart, tend);

	  /* get the result */
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeGradEnergy)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* send the results */
		      MPI_Sendrecv(&GradEnergyDataResult[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_out),
				   MPI_BYTE, recvTask, TAG_GRADENERGY_B,
				   &GradEnergyDataPartialResult[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct hydrodata_out),
				   MPI_BYTE, recvTask, TAG_GRADENERGY_B, MPI_COMM_WORLD, &status);

		      /* add the result to the particles */
		      for(j = 0; j < nsend_local[recvTask]; j++)
			{
			  source = j + noffset[recvTask];
			  place = GradEnergyDataIn[source].Index;

			  for(k = 0; k < 3; k++)
			    SphP[place].GradEnergy[k] += GradEnergyDataPartialResult[source].GradEnergy[k];

			}
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  level = ngrp - 1;
	}

      MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
      for(j = 0; j < NTask; j++)
	ntotleft -= ndonelist[j];
    }

  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);




  /* collect some timing information */

  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if(ThisTask == 0)
    {
      All.CPU_HydCompWalk += sumt / NTask;
      All.CPU_HydCommSumm += sumcomm / NTask;
      All.CPU_HydImbalance += sumimbalance / NTask;
    }
}


/*! This function is the 'core' of the SPH force computation. A target
 *  particle is specified which may either be local, or reside in the
 *  communication buffer.  -- just a copy of hydro_evaluate to get the gradient of the energy
 */
void compute_grad_energy_particle(int target, int mode)
{
  int j, k, n, startnode, numngb;
  FLOAT *pos;
  FLOAT grad_energyx, grad_energyy,  grad_energyz;
  FLOAT entropy, energy, energy_neighbor, delta_energy;
  FLOAT h_i, rho;
  double dx, dy, dz;
  double h_i2, hinv, hinv4;
  double dwk_i;
  double h_j, dwk_j, r, r2, u;

  /********
#ifndef NOVISCOSITYLIMITER
  double dt;
#endif
  ********/
  if(mode == 0)
    {
      pos = P[target].Pos;
      h_i = SphP[target].Hsml;
      rho = SphP[target].Density;
      //      pressure = SphP[target].Pressure;
      entropy = SphP[target].Entropy;
    }
  else
    { 
/* These variables MUST agree with the size of struct gradenergydata_in in allvars.h */
      pos = GradEnergyDataGet[target].Pos;
      h_i = GradEnergyDataGet[target].Hsml;
      rho = GradEnergyDataGet[target].Density;
      //      pressure = HydroDataGet[target].Pressure;
      entropy = GradEnergyDataGet[target].Entropy;
    }

  /* get the internal entropy in (km/s)**2 */
  energy = gadgetentropy_to_gadgetenergy_gadget_style_version(entropy, rho); /* need for FLD */

  /* initialize variables before SPH loop is started */
  grad_energyx = grad_energyy = grad_energyz = 0;
  h_i2 = h_i * h_i;

  /* Now start the actual SPH computation for this particle */
  startnode = All.MaxPart;
  do
    {
      numngb = ngb_treefind_pairs(&pos[0], h_i, &startnode);

      for(n = 0; n < numngb; n++)
	{
	  j = Ngblist[n];
	  energy_neighbor = gadgetentropy_to_gadgetenergy_gadget_style_version(SphP[j].Entropy, SphP[j].Density);	  
	  delta_energy = energy_neighbor - energy; /* neighbor - target or q-p */
 	  dx = pos[0] - P[j].Pos[0];
	  dy = pos[1] - P[j].Pos[1];
	  dz = pos[2] - P[j].Pos[2];	  

#ifdef PERIODIC			/*  find the closest image in the given box size  */
	  if(dx > boxHalf_X)
	    dx -= boxSize_X;
	  if(dx < -boxHalf_X)
	    dx += boxSize_X;
	  if(dy > boxHalf_Y)
	    dy -= boxSize_Y;
	  if(dy < -boxHalf_Y)
	    dy += boxSize_Y;
	  if(dz > boxHalf_Z)
	    dz -= boxSize_Z;
	  if(dz < -boxHalf_Z)
	    dz += boxSize_Z;
#endif
	  r2 = dx * dx + dy * dy + dz * dz;
	  h_j = SphP[j].Hsml; /* this is really 2h   NOT h (?) */
	  if(r2 < h_i2 || r2 < h_j * h_j)
	    {
	      r = sqrt(r2);
	      if(r > 0)
		{
		  //		  p_over_rho2_j = SphP[j].Pressure / (SphP[j].Density * SphP[j].Density);

		  /* begin kernel */
		  if(r2 < h_i2)
		    {
		      hinv = 1.0 / h_i;
#ifndef  TWODIMS
		      hinv4 = hinv * hinv * hinv * hinv;
#else
		      hinv4 = hinv * hinv * hinv / boxSize_Z;
#endif
		      u = r * hinv;
		      /* dwk is the kernel gradient (partial deriv. dw/dr) */
		      if(u < 0.5)
			dwk_i = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		      else
			dwk_i = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
		    }
		  else
		    {
		      dwk_i = 0;
		    }

		  if(r2 < h_j * h_j)
		    {
		      hinv = 1.0 / h_j;
#ifndef  TWODIMS
		      hinv4 = hinv * hinv * hinv * hinv;
#else
		      hinv4 = hinv * hinv * hinv / boxSize_Z;
#endif
		      u = r * hinv;
		      if(u < 0.5)
			dwk_j = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		      else
			dwk_j = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
		    }
		  else
		    {
		      dwk_j = 0;
		    }
		  /* end kernel */


		  /* grad_energy_i = sum(mass_j/rho_j*energy_j*grad_kernel) 
		   *  e.g. Eq. 2.5  Monaghan 1992   */
		  /* !!!!  using grad_kernel = grad_kernel_i + grad_kernel_j / 2   !!!!! check that this is correct */ 
/* ///// check do we only need the neighbor mass (so no comm of mass required) !!!!! */
		  delta_energy *= 0.5 * P[j].Mass / rho  * (dwk_i + dwk_j) / r; /* keep in (comoving) sim units -- convert later if needed  */

		  //		  deltaentropy = SphP[j].Entropy - entropy ; //  (q->u - p->u);   
		  /* use gradU = sum deltaU * grad_kernel / r * a ?? */
		  grad_energyx += delta_energy * dx ;
		  grad_energyy += delta_energy * dy ;
		  grad_energyz += delta_energy * dz ;
		}
	    }
	}
    }
  while(startnode >= 0);

  /* Now collect the result at the right place */
  if(mode == 0)
    {
	SphP[target].GradEnergy[0] = grad_energyx;
	SphP[target].GradEnergy[1] = grad_energyy;
	SphP[target].GradEnergy[2] = grad_energyz;
    }
  else
    { /* These variables MUST agree with the size of struct gradenergydata_out in allvars.h */
      for(k = 0; k < 3; k++)
	GradEnergyDataResult[target].GradEnergy[0] = grad_energyx;
	GradEnergyDataResult[target].GradEnergy[1] = grad_energyy;
	GradEnergyDataResult[target].GradEnergy[2] = grad_energyz;
    }
}




/*! This is a comparison kernel for a sort routine, which is used to group
 *  particles that are going to be exported to the same CPU.
 */
int gradenergy_compare_key(const void *a, const void *b)
{
  if(((struct gradenergydata_in *) a)->Task < (((struct gradenergydata_in *) b)->Task))
    return -1;
  if(((struct gradenergydata_in *) a)->Task > (((struct gradenergydata_in *) b)->Task))
    return +1;
  return 0;
}
