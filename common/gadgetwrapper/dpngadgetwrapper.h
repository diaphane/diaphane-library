#include "allvars.h"   // Gadget2 
#include "fld_types.h"
#include "fldgen.h"


void GadgetInitDiaphane();
void GadgetDoDiaphaneStep();
void SetSimContext(DiaphaneSimContext *sim_context);
void GadgetFinishDiaphane();

void dpnUpdateEntropy(float dTime, int radiationModel);


int neighbor_id_list_cube_to_sphere(SIMFLOAT posx0, SIMFLOAT posy0, SIMFLOAT posz0, void* neighbor_id_list_voidpointer, int numngb, SIMFLOAT rmax_squared, int periodic, SIMFLOAT periodsimunits);

SIMFLOAT gadgetentropy_to_gadgetenergy_gadget_style_version(SIMFLOAT entropy, SIMFLOAT density);

int is_gadget_particle_active(int iparticle);



// functions to calculate extra particle properties using gadget-like methods 
// functions in fldgadgetwrapper_gradenergy.c    
void compute_grad_energy(void);
void compute_grad_energy_particle(int target, int mode);
int gradenergy_compare_key(const void *a, const void *b);


