/*
  Diaphane main cpp file
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "diaphane.h"
#include "diaphane_types.h"
#include "setunits.h"
#include "starrad.h"
#include "setup_fld.h"

#include "paramfile.h"

DiaphaneContext* dpnInit() 
{
  /*
    Base initialization, allocate memory for diaphane context
    Module specific initialization done in InitModules()
    This allows to do stuff like unit initialization in between 
  */
  DiaphaneContext* dpnContext = dpnCreateContext();

  return dpnContext;
}

//  Init Units and opacity gas quantities
//  could clean up context by removing the extra copy of some units in dpn_context->sim and put all into dpn_context->units
void dpnInitUnits(DiaphaneContext *dpn_context)
{

  // Null check dpn_context & in

/*  We get these general diaphane context variables from the main code,
    so we must trust them.
   in->ConstGamma 
   in->MeanMolWeight
   in->periodic
   in->velocityunit_cgs_z0  
   in->lengthunit_cgs_z0
   in->massunit_cgs
   in->periodx_z0 (gasoline: assume periodx=1 in sim units)
   in->periody_z0 (gadget periodx=periody=periodz)
   in->periodz_z0
*/

  DiaphaneSimContext* in  = dpn_context->sim;   // includes base units from sim (mass, lenth, velocity, time)
  DiaphaneUnits* dpnunits = dpn_context->units; // all units 

  // Get units provided as part of the SimContext
  // For cosmological runs, we will recompute the units for 
  // each timestep to match the redshift, z (scale_factor==(1/1+z)1)
  // Can we do this already here? e.g. currently gadget wrapper requires '/ scale_factor' instead of '* scale_factor'?
  dpnunits->massunit_cgs          = in->massunit_cgs;
  dpnunits->lengthunit_cgs_z0     = in->lengthunit_cgs_z0;
  dpnunits->lengthunit_cgs        = in->lengthunit_cgs_z0; /* * in->scale_factor */;
  dpnunits->velocityunit_cgs_z0   = in->velocityunit_cgs_z0;
  dpnunits->velocityunit_cgs      = in->velocityunit_cgs_z0; /* * in->scale_factor? */;
  dpnunits->timeunit_cgs_z0       = in->timeunit_cgs_z0;
  dpnunits->timeunit_cgs          = in->timeunit_cgs_z0; /* * in->scale_factor? */;
  dpnunits->ConstGamma            = in->ConstGamma;
  dpnunits->MeanMolWeight         = in->MeanMolWeight;
  // Set up rest of units via internal CodeUnits object
  compute_diaphane_units_from_base(dpnunits);

  // set up opacity context -- the same opacity parameters will be used for all modules.
  dpn_context->opacity_context->temp_to_internalenergy_cgs = dpnunits->temperature_to_internalenergy_cgs;   // is this copy needed?
  dpn_context->opacity_context->ConstGamma = dpnunits->ConstGamma; 

   // Read the parameter file 
  // Currently done on all processes...
  paramfile params(std::string(dpn_context->param_file_name), false);

  dpn_context->opacity_context->PowerKappa = params.find<int>("bPowerKappa", 0); 
  dpn_context->opacity_context->KappaConst = params.find<double>("dKappaConst", 1.0);
  dpn_context->opacity_context->KappaIndex = params.find<double>("dKappaIndex", 1.0);
  strcpy(dpn_context->opacity_context->opacityTableFilename, params.find<std::string>("opacityTableFilename", "./rosseland.abpoll.p3p5.amax1mm-mod.dat").c_str() ) ;

  /* setup opacity table */
  if(!dpn_context->opacity_context->PowerKappa)  /* if not just using power law opacity */
    {
      setup_opacity_table(dpn_context->opacity_context);
    }
  else  /* leave other opacity table values uninitialized? */
    {
      dpn_context->opacity_context->opacityTableMaxTemperature = HUGE_VAL;
    }

  printf("Opacity parameters set:  bPowerKappa %d dKappaConst %g dKappaIndex  %g opacityTableFilename %s\n",dpn_context->opacity_context->PowerKappa, dpn_context->opacity_context->KappaConst,dpn_context->opacity_context->KappaIndex,dpn_context->opacity_context->opacityTableFilename);  /////////// debugging 
}

void dpnRescaleUnits(DiaphaneContext *dpn_context)
{
 /* In here we can rescale units by dpn->sim->scale_factor */
}

void dpnInitModules(DiaphaneContext* dpn_context, unsigned module_choice)
{
  printf("in dpnInitModules. will init module choice %d param file %s. cgs length %g  gamma %g\n ",module_choice,dpn_context->param_file_name, dpn_context->units->lengthunit_cgs, dpn_context->units->ConstGamma);  ////////////////////////
///////////    printf("mdlinfo in dpnInitModules   mdl rank %d nranks %d\n",dpn_context->mdl->idSelf,dpn_context->mdl->nThreads);
    /* Init each radiation module here */
  switch(module_choice)
  {  
  #ifdef DIAPHANE_STARRAD
  case(DPN_STARRAD):  
     // Initialize starrad itself
	sr_init(dpn_context); 

     // Add bright fake source for testing  -- (solar temperature, L~19000Lsun)
        printf("note -- adding fake source at position 0 0 0 \n");
 	fflush(stdout);
     	sr_set_src(dpn_context->starrad,0,0,0,5800,958E10); // T=5800K  R=958e10cm
        break;
  #endif 

  #ifdef DIAPHANE_FLD
//  if(module_choice & RADIATION_MODEL::DPN_FLD)
  case(DPN_FLD):
//    
     
	// some debugging
      printf("dpnInitModules. will init FLD. param file %s. cgs length %g  gamma %g\n ",dpn_context->param_file_name, dpn_context->units->lengthunit_cgs, dpn_context->units->ConstGamma);  
      InitFluxLimitedDiffusion(dpn_context);
      break;
  #endif 
  }
}

/* at start of run */
DiaphaneContext* dpnCreateContext()
{
  /* Full Diaphane Context struct */
  DiaphaneContext* dpnContext = (DiaphaneContext *)malloc(sizeof(DiaphaneContext));
  if (dpnContext == NULL)
    {
      printf("Failed to allocate diaphane context memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 

  /* sim paramters struct */
  DiaphaneSimContext* SimContext = (DiaphaneSimContext *)malloc(sizeof(DiaphaneSimContext));
  if (SimContext == NULL)
    {
      printf("Failed to allocate Diaphane simcontext memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 
  dpnContext->sim = SimContext;

  /* sim units struct */
  DiaphaneUnits* Units = (DiaphaneUnits*)malloc(sizeof(DiaphaneUnits));
  if (Units == NULL)
    {
      printf("Failed to allocate Diaphane Units memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 
  dpnContext->units = Units;

  /* opacity context struct */  
  OpacityContext* opacity_context = (OpacityContext *)malloc(sizeof(OpacityContext));
  if (opacity_context == NULL)
    {
      printf("Failed to allocate Diaphane opacity context memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 
  dpnContext->opacity_context = opacity_context;

  
  return dpnContext;
}


 /* at end of run */
void dpnFinish(DiaphaneContext *dpn_context) 
{
  free(dpn_context->sim);
  free(dpn_context->units);
  free(dpn_context->opacity_context);
  free(dpn_context);
  dpn_context = NULL;
}



/*
  Update u from dt and uDot
  Expects units to be consistent
*/
void dpnUpdateInternalEnergy(double* u, double dt, double uDot)
{
  *u += dt * uDot;
  //if(*u < 0.0001) *u = 0.0001;
}


//void dpnEvolveVariableSingle(float *y, AFLOAT dt, AFLOAT yDot)  // note mixing precision of simulation and library
void dpnEvolveVariableSingle(float *y, float dt, float yDot)
{
  *y += dt * yDot;
}


//void dpnEvolveVariableDouble(double *y, AFLOAT dt, AFLOAT yDot)  // note mixing with mixing precision of simulation and library
void dpnEvolveVariableDouble(double *y, double dt, double yDot) 
{
  *y += dt * yDot;
}







//  use this function to remove timestep update from main code to library
/* apply maximum timestep criterion to particle */
/*********
void update_timestep(double *dt, double dtmax)
{
  if(dtmax < dt)
    {
      dt = dtmax;
    }
}
*******/



void dpnFinishModules(DiaphaneContext* dpn_context, unsigned module_choice)
{
    /* Init each radiation module here */
  switch(module_choice)
    {
  #ifdef DIAPHANE_STARRAD
    case DPN_STARRAD:
//  if(module_choice & RADIATION_MODEL::DPN_STARRAD)
//  {
     // Initialize starrad itself
   //  sr_end(MDL mdl, sr_context* sr_c); // TODO: check this
     break;
//  }
  #endif 

  #ifdef DIAPHANE_FLD
     case DPN_FLD:
//  if(module_choice & RADIATION_MODEL::DPN_FLD)
 //  {      
      FreeFluxLimitedDiffusionContextMemory(&dpn_context->fld); /* at end of run */	
      break; 
//   }
  #endif 
     }
  dpnFinish(dpn_context); // free context
}
