# DIAPHANE #

A library for radiation & neutrino transport for the next generation of hydrodynamical simulations in astrophysics.

Our philosophy for building the energy transport library is that it must be portable and modular
so that it can be utilized with minimal user effort by any major astrophysical hydrodynamic code.  As such, DIAPHANE provides a common platform for application-independent radiation and neutrino transport in astrophysical simulations.  

This important physics controls the energy flow in a range of astrophysical systems and has a critical impact on model predictions.  The library contains radiation and neutrino transport algorithms to enable our science goals of modelling galaxy formation, black hole formation, and planet formation, as well as supernova stellar explosions.  The computational difficulty of modelling energy transport is the limiting factor in our ability to model these systems; this is largely because the speed of light is fast compared to other hydrodynamic processes.  

Our library is written in c and c++, but many hydrodynamic codes use fortran, so we provide examples of how to interface the library from the fortran codes SPHYNX and RAMSES.  The library is currently able to run on MPI-based supercomputers, and is planned to be extended to multiple computing architectures, including HPC clusters with accelerators such as GPUs or Xeon Phi MIC processors.


# Instructions: #

Quick instructions to for the Diaphane Libarry: https://bitbucket.org/diaphane/diaphane-library/wiki/Diaphane%20quick%20installation%20instructions


The _common_ directory contains functions that may be used by any of the modules.

The _fld_ directory contains the core physics layer of the Flux Limited Diffusion library module. All radiation transfer physics algorithms are containted here. This module does not require MPI and can be called from c/c++ or modern fortran.

The _starrad_ directory contains the core physics layer of the Flux Limited Diffusion library module. All radiation transfer physics algorithms are containted here. This module requires MPI, and an interface is provided for use with gasoline/ChaNGa .

The subdirectory _gasolinewrapper_ contains the files to interface with the library from gasoline. Update the diaphane section of the gasoline Makefile with the relevant paths for your system and compile.

The subdirectory _gadgetwrapper_ contains the files to interface with the library from gadget. Update the diaphane section of the gasoline Makefile with the relevant paths for your system and compile.

The subdirectory _sphynxwrapper_ contains the files to interface with the sphynx library from gasoline. Copy them into the gasoline source directory before compiling gadget. Sphynx is a fortran90 code. The interface between sphynx in fortran and the FLD module in c/c++ is handled by the iso_c_binding module.

The wrapper directories can be used as templates to interface the library with a different hydro code.

Let us know of any problems.

-Darren Reed, for the DIAPHANE team