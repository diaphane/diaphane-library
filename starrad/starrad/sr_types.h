 #ifndef DIAPHANE_STARRAD_TYPES_H
#define DIAPHANE_STARRAD_TYPES_H

#include <mpi.h>   // include mpi and mdl here because cannot be inside the extern C
#include "mdl.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "sr_units.h"
#include "diaphane_types.h"
#include "opacity.h"

/*
	TYPE DEFINITIONS: some common way of defining this stuff...
	Repeat pkdgrav path length convention

*/
#include <sys/param.h> /* for MAXPATHLEN */
#ifndef MAXPATHLEN
#define MAXPATHLEN 256
#endif

#define CID_RAYS_TAU 0101
#define CID_RAYS_INTENSE 0102

/*
	Temporary star source, contains position, intensity and radius
*/
typedef struct starsrc{
	float pos[3];
	float temp; // K
	float radius; // cm,
	float intensity; // erg / s / sr 
	float kappa;
} starsrc;

// typedef struct ray {
// 	float* tau;
// 	long int count;
// 	float d;
// } ray;
typedef struct sr_context{
/*
	Put a bunch of starrad context stuff here
	Maybe extract this to 'diaphane context' and include fld as well?
	This can be passed around gasoline etc, to keep track of things like ray structures?
*/
	int res;	
	int n_rays;	
	double l_ray;
	double l_seg;
	int n_seg;

	int rank;
	int n_ranks;
	int n_local_rays;
	float* local_rays_tau;
	float* local_rays_intensity;
	
	int rays_per_rank;

/*
	(temp) Keep track of source location and intensity
*/
	starsrc src;
	OpacityContext* oc;

/*
	Units 
*/
	struct 	DiaphaneUnits* units;
/*
	ray* global_rays?
*/
	int logging;
	float eta;

	// Path for parameter file
	char param_file_name[MAXPATHLEN];

	// Ptr to mdl so we can use it in resmooth
	MDL mdl;
	int step;

} sr_context;



#ifdef __cplusplus
 }
#endif

#endif
