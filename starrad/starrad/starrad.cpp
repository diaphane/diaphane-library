/*
	Some blurb about starrad.cpp
*/

#include <fenv.h>
#include <string.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>

#include <mpi.h>   // include mpi and mdl here because cannot be inside the extern C
extern "C" {
#include "mdl.h"
}


#include "starrad.h"
#include "sr_units.h"
#include "chealpix.h"

#include "paramfile.h"

#ifdef __cplusplus
extern "C" {
#endif

 void neighbours_nest(int*, int*, int*, int*);

#ifdef __cplusplus
 }
#endif

/*
	Enable floating point exceptions
	This isnt totally portable...
*/
#define STARRAD_ENABLE_FEXCEPTIONS

// Some extra options for debugging
//#define LOGGING_STARRAD
//#define VERBOSE_STARRAD

/*
	Forward declare local functions
*/
void get_affected_ray_neighbours(std::vector<int>& todo, int nside, float x, float y, float z, float l, float r2);

/* 
	Initialize and finalize Starrad
*/
void sr_init(DiaphaneContext *dpn_c)
{
	// Alloc for the context
	printf("inside sr_init\n");
	fflush(stdout);
	sr_context *sr_c = (sr_context*)malloc(sizeof(sr_context));
	if(sr_c == NULL)
	{
		// Failed to allocate memory, throw error somehow
		printf("Failed to allocate memory for starrad context!\n");
	}

	// Any contextual initialization follows...

	dpn_c->starrad = sr_c;
	sr_context* context = sr_c;
	context->units = dpn_c->units;
	MDL mdl = dpn_c->mdl;
	context->oc = dpn_c->opacity_context;  // opacity context should already be initialized

//	printf("mdl rank %d nranks %d\n",mdl->idSelf,mdl->nThreads); ///////////////// debugging

	// Read the parameter file
	// Currently done on all processes...
	paramfile params(std::string(dpn_c->param_file_name), false);

	// Validate parameters when they are read from parameter file
	// Currently in sim units
	context->l_ray = params.find<double>("ray_length", 1800);
	context->l_seg = params.find<double>("segment_length", 1.0);
	context->n_seg = context->l_ray/context->l_seg;
	context->res = params.find<int>("hpx_res", 32);
	context->eta =params.find<float>("eta", 0.1);
	context->mdl = mdl;
	
	// Parallel stuff
	int rank = mdl->idSelf;
	int n_ranks = mdl->nThreads;
	context->rank = rank;
	context->n_ranks = n_ranks;

	// Opacity parameters are already read in.  
	// Note that all modules share the same opacity context parameters --
	//   but this could be changed to read different ones here.
	// Remember to initialize table limits if setting const
	// context->oc = (opacity_context*)malloc(sizeof(opacity_context));
	// context->oc->power_kappa = params.find<float>("power_kappa", 0);
	// context->oc->kappa_idx = params.find<float>("kappa_idx", 0);
	// context->oc->kappa_const = params.find<float>("kappa_const", 0);
	// context->oc->const_gamma = context->units->const_gamma;
	// context->oc->temp_to_internalenergy_cgs = context->units->temp_to_internalenergy_cgs; 
	// setup_opacity_table(context->oc, (char const*)"rosseland.abpoll.p3p5.amax1mm-mod.dat");

	// Temporary logging
#ifdef LOGGING_STARRAD
	context->logging = 1;
#else
	context->logging = 0;
#endif
	context->step = 0;
	// Allocate memory for rays
	alloc_rays(mdl, context, rank, n_ranks);

	// Put this in some verbose condition...
	printf("Rank %d of %d: Starrad initialised with res: %d, global n_rays: %d, local n_rays: %d\n", rank, n_ranks, context->res, context->n_rays, context->n_local_rays);

#ifdef STARRAD_ENABLE_FEXCEPTIONS
	feenableexcept(FE_DIVBYZERO|FE_INVALID|FE_OVERFLOW);
#endif
}

void sr_end(MDL mdl, sr_context* sr_c)
{
	free_rays(mdl, sr_c);
	// free_opacity_table(sr_c->oc); // done in dpnFinish
	// free(sr_c->oc);
	free(sr_c);
}

/* 
	Set the current source for STARRAD
*/
void sr_set_src(sr_context* sr_c, double x, double y, double z,double temp, double radius){
	sr_c->src.pos[0] = x;
	sr_c->src.pos[1] = y;
	sr_c->src.pos[2] = z;
	sr_c->src.temp = temp;
	sr_c->src.radius = radius;

	// energy flux (erg cm^-2 s^-1),
	double F = temperature_to_flux(1.0, sr_c->src.temp);
	// Then luminosity (erg s^-1) 
	double L = flux_to_luminosity(F, sr_c->src.radius);
	// Then intensity (assume isotropic) (erg s^-1 sr^-1 ) 
	sr_c->src.intensity = L;//luminosity_to_intensity_iso(L);
}

/* this is the unnormailed kernel ***/
/* Monaghan & Latanzio 1995, B-spline with compact support, 
 *    eqn 21 (with the typo from that paper fixed to be nu**2??)! 
 *  (or reference Monaghan 1992, p. 554 (section 7)
 *  nu_squared =(gasoline ar2) = (r/h)**2 (nu=r/h=sqrt(ar2))
 *   e.g. W(r,h) = kernelnonorm / pi / h**3  (comoving)
 * x 1/2 for symmetric calc
 */
/** w(r,h) = 8/pi/h**3 * 1-6(r/h)**2+6*(r/h)**3  (r/h <= 1/2)  (h==2rsmooth)
 **   = 8/pi/h**3 * 2(1-r/h)**3          (1/2 < r/h <+ 1)  **/

/* or   w(nu) = 1/pi/h**3 * [1 - 3/2 nu**2 + 3/4 nu**3]  nu<1 (h==rsmooth) 
              = 1/pi/h**3 * 1/4(2-nu)**3             1<nu<2        
*/


double bspline_kernel_nonorm(double nusquared, double h2) 
{
  double ak, result;
  // Account for a particle being directly on path of ray, nusquared is then 0
  if(nusquared == 0)
  	ak = 2.0;	
  else
  	ak = 2.0 - sqrt(nusquared);
  
  if ((nusquared) < 1.0) 
    {
      result = (1.0 - 0.75*ak*(nusquared)); 
    }
  else 
    {
      result = 0.25*ak*ak*ak; 
    }
  return result;   /* typically order ~1 */
}

/*
	Do optical depth
*/

void compute_tau(sr_context* sr_c, float h2, float rho, float m, float x, float y, float z, float u)
{
	// Everything in sim units until necessary to convert, use sr_c->units...

	float h3 = h2 * sqrt(h2);
	float norm = m * (M_1_PI / h3);


	// Loop over sources here...

	// First affected ray
	long int rayid;
	double nvec[3];
	double ray_vec[3];
	int nside = sr_c->res;
	double l_seg = sr_c->l_seg;
	//double l_seg_cgs = sr_c->l_seg * sr_c->units.lengthunit_cgs;
	int n_seg = sr_c->n_seg;

	// Ray from source to particle
	float xr = x - sr_c->src.pos[0];
	float yr = y - sr_c->src.pos[1];
	float zr = z - sr_c->src.pos[2];

	// Normalize vector
	float len = sqrt(xr*xr + yr*yr + zr*zr);
	nvec[0] = x/len;
	nvec[1] = y/len;
	nvec[2] = z/len;

	// Get affected pixel
	vec2pix_nest(nside, (const double*) nvec, &rayid);

	// Create todo list of all affected pixels
	std::vector<int> todo;
	todo.push_back(rayid);
	get_affected_ray_neighbours(todo, nside, xr, yr, zr, len, h2);
	
	// Loop through each affected pixel and calculate number of affecting particles
	for(unsigned i = 0; i < todo.size(); i++)
	{
		// Get ray
		pix2vec_nest(nside, todo[i], ray_vec);

		// Project particle onto ray
		double a1 = (xr * ray_vec[0]) + (yr * ray_vec[1]) + (zr * ray_vec[2]);
		double a12 = a1*a1;

		// Get distance2 from particle center to ray
		double a2 = (len*len) - a12;

		// r2 is half length of ray segment affected by gas
		double r2 = (4*h2 - a2);

		if(r2 < 0)
			continue;

		// NOTE
		// Probably can reorganise this to cut down number of sqrts

		// Intersection
		// Calculate range of affected tau segments: segment at a1 - r2 to segment at a1 + r2
		// Use r2/4 to put optical depth over half of the tau segments (top hat - cloud in cell).
		// Better choice would be triangular shape cloud...
		int left = 0;
		int right = n_seg-1;
		/// If ray doesnt begin inside particle...
		if((a12 - r2/4) > 0)
		{
			left = (int) (sqrt(a12 - r2/4)/l_seg) ;
		}

		// And doesnt end inside particle
		if ((a12 + r2/4) / (2 * l_seg * l_seg) < (n_seg*n_seg) ) 
		{
			right = (int) (sqrt(a12 + r2/4) / l_seg);
		}

		// 
		// Calculate max rho (at a2 from center)
		//	This should be different for each segment...
		// Length of ray affected 
		float L = 2*sqrt(r2);

		// Get rho
		float d2 = 0;
		// Account for floating point inaccuracies when particle is 
		// almost/directly on path of ray causing a2 < 0
		if(a2 > 0)
			d2 = a2/h2;

		// This should be integral of rho along ray through particle
		// Currently just max rho along linez
		float maxrho = bspline_kernel_nonorm(d2, h2) * norm;

		//
		// Do unit conversions
		L *= sr_c->units->lengthunit_cgs;
		maxrho *= sr_c->units->densityunit_cgs;	

		// Estimate depth as 1/2 max_rho * L * k, this should be solving the actual kernel...
		// Spread depth over ray segments with top hat
		double kappa = calc_opacity(sr_c->oc, maxrho, u / sr_c->units->temperature_to_internalenergy_simunit);  // 1.0; // 10.0;
		double depth = (maxrho *  (L / (right-left+1)) * kappa)/2;
		
		// Ray index
		int idx = todo[i];
		int owner = idx/sr_c->rays_per_rank;
		//nprintf("Doing ray idx: %d\n", idx);
		// Final rank might have overflow
		if(owner >= sr_c->n_ranks) owner = sr_c->n_ranks-1;
		// Remap to local ray index
		idx -= owner * sr_c->rays_per_rank;
		//printf("Local idx: %d\n", idx);
		int sidx = 0;
		for(int j = left; j <= right; j++)
		{
			// Remap to segment index
			sidx = (idx * n_seg) + j; 
			//printf("rank %d, idx %d, sidx %d, owner %d\n",sr_c->rank, idx, sidx, owner);
			//printf("Segment idx: %d\n", idx);
			// Aquire ray segment from cache
			float* r_seg;
			if(owner != sr_c->rank)
				r_seg = (float*)mdlAquire(sr_c->mdl, CID_RAYS_TAU,  sidx, owner);
			else
				r_seg = &sr_c->local_rays_tau[sidx];
			
			// Temp hack to remap remote rays to local rays
			//if( idx >= (sr_c->n_rays/sr_c->n_ranks)) idx -= (sr_c->n_rays/sr_c->n_ranks);
			//idx = (idx * n_seg) + j;
			*r_seg += depth;

			// Release it again
			if(owner != sr_c->rank)
				mdlRelease(sr_c->mdl, CID_RAYS_TAU,  (void*)r_seg);
		}
			

		// For ray struct
		//for(int j = left; j <= right; j++)
		//	sr_c->local_rays[neighbours[i]].tau[j] += depth;

		// Logging stuff
		//sr_c->local_rays[neighbours[i]].count++;
		//sr_c->local_rays[neighbours[i]].d += r2;		
	}
}


/*
	Local integration of rays
*/
void sr_integrate_tau(sr_context* sr_c)
{
#ifdef VERBOSE_STARRAD
	printf("Local intensity integration\n");
#endif
	//int l_seg = sr_c->l_seg;
	int ns = sr_c->n_seg;
	int nsm1 = ns-1;
	float* rays = sr_c->local_rays_tau;

	// Integrate optical depth tau along ray
	for(int i = 0; i < sr_c->n_local_rays; i++)
	{
		// Simple trapezoidal reimann sum
		for(int j = 0; j < nsm1-1; j++)
		{
			rays[(i * ns) + j+1] = rays[(i * ns) + j] +  ((rays[(i * ns) + j+1] + rays[(i * ns) + j + 2]) / 2);
		}
		// Boundary
		rays[(i * ns) + nsm1] = rays[(i * ns) + nsm1 - 1] + rays[(i * ns) + nsm1]/2;
	}	

#ifdef LOGGING_STARRAD
	std::ofstream ofile;
	std::stringstream ss;

 	// Logging
	if(sr_c->logging)
	{
		ss << "ray_tau" << sr_c->rank << ".log";
		ofile.open(ss.str().c_str()); 	
		for(int i = 0; i < sr_c->n_local_rays; i++)
		{
			for(int j = 0; j < sr_c->n_seg; j++) 
				ofile << rays[i * ns + j] << "\t";

			ofile << "\n";
		}

		ofile.close();
	}
#endif

}
/*
	Iterate over rays converting tau to intensity
*/
void sr_tau2intensity(sr_context* sr_c)
{
#ifdef VERBOSE_STARRAD
	printf("Local intensity integration\n");
#endif
	//int l_seg = sr_c->l_seg;
	int ns = sr_c->n_seg;
	//int nsm1 = ns-1;
	float* rays_tau = sr_c->local_rays_tau;
	float* rays_intensity = sr_c->local_rays_intensity;
 
	// Apply tau to intensity
	for(int i = 0; i < sr_c->n_local_rays; i++)
	{
		// Store first segment depth and replace with intensity 
		rays_intensity[(i * ns)] = sr_c->src.intensity;
		
		for(int k = 0, j = 1; j < ns; j++, k++)
		{
			rays_intensity[(i * ns) + j] = sr_c->src.intensity * exp(-rays_tau[(i * ns) + k]);
		}	
	}

#ifdef LOGGING_STARRAD

		std::ofstream ofile2;
		std::stringstream ss2;
 	// Logging
	if(sr_c->logging)
	{

		ss2 << "ray_intensity" << sr_c->rank << ".log";
		ofile2.open(ss2.str().c_str()); 	
		for(int i = 0; i < sr_c->n_local_rays; i++)
		{
			for(int j = 0; j < sr_c->n_seg; j++) 
				ofile2 << rays_intensity[i * ns + j] << "\t";

			ofile2 << "\n";
		}

		ofile2.close();
	}

#endif

}



/*
	Do intensity, gives rate of change of energy
*/

void sr_absorb_energy(sr_context* sr_c, float h2, float rho, float m, float x, float y, float z, float u, double* uDot, double *od)
{
	// Everything in sim units until necessary to convert, use sr_c->units...

	float h3 = h2 * sqrt(h2);
	float norm = m * (M_1_PI / h3);
	double radius = sr_c->src.radius;

	// Loop over sources here...
	// SHOULD CONSIDER SOURCE RADIUS currently source is just a point...


	// First affected ray
	long int rayid;
	double nvec[3];
	double ray_vec[3];
	int nside = sr_c->res;
	double l_seg = sr_c->l_seg;
	//double l_seg_cgs = sr_c->l_seg * sr_c->units.lengthunit_cgs;
	int n_seg = sr_c->n_seg;

	// Ray from source to particle
	float xr = x - sr_c->src.pos[0];
	float yr = y - sr_c->src.pos[1];
	float zr = z - sr_c->src.pos[2];

	// Normalize vector
	float len = sqrt(xr*xr + yr*yr + zr*zr);
	nvec[0] = x/len;
	nvec[1] = y/len;
	nvec[2] = z/len;

	// Get affected pixel
	vec2pix_nest(nside, (const double*) nvec, &rayid);

	// Create todo list of all affected pixels
	std::vector<int> todo;
	todo.push_back(rayid);
	get_affected_ray_neighbours(todo, nside, xr, yr, zr, len, h2);
	
	*uDot = 0;
	// Loop through each neighbouring pixel and calculate number of affecting particles
	for(unsigned i = 0; i < todo.size(); i++)
	{
		// Get ray
		pix2vec_nest(nside, todo[i], ray_vec);

		// Project particle onto ray
		double a1 = (xr * ray_vec[0]) + (yr * ray_vec[1]) + (zr * ray_vec[2]);
		double a12 = a1*a1;

		// Get distance2 from particle center to ray
		double a2 = (len*len) - a12;

		// r2 is half length of ray segment affected by gas (within 2h distance of particle)
		double r2 = (4*h2 - a2);

		// if its negative theres no intersection
		if(r2 < 0)
			continue;

		// NOTE
		// Probably can reorganise this to cut down number of sqrts

		// Intersection
		// Calculate range of affected segments: segment at a1 - r2 to segment at a1 + r2
		// Account for limits - if ray begins 'inside' particle, if particles influence extends past ray end
		// Use r2/4 to put optical depth over half of the tau segments (top hat - cloud in cell).
		// Better choice would be triangular shape cloud...

		
		int left = 0;
		int right = n_seg-1;
		/// If ray doesnt begin inside particle...
		if((a12 - r2/4) > 0)
		{
			left = (int) (sqrt(a12 - r2/4)/l_seg) ;
		}

		// And doesnt end inside particle
		if ((a12 + r2/4) / (2 * l_seg * l_seg) < (n_seg*n_seg) ) 
		{
			right = (int) (sqrt(a12 + r2/4) / l_seg);
		}


		// 
		// Calculate max rho (at a2 from center)
		//	This should be different for each segment...
		// Length of ray affected 
		float L = 2*sqrt(r2);

		// Get rho
		
		float d2 = 0;
		// Account for floating point inaccuracies when particle is 
		// almost/directly on path of ray causing a2 < 0
		if(a2 > 0)
			d2 = a2/h2;

		// This should be integral of rho along ray through particle
		// Currently just max rho along linez
		float maxrho = bspline_kernel_nonorm(d2, h2) * norm;

		//
		// Do unit conversions
		L *= sr_c->units->lengthunit_cgs;
		maxrho *= sr_c->units->densityunit_cgs;	

		// Estimate depth as 1/2 max_rho * L * k, this should be solving the actual kernel...
		// Calculate absorbed intensity from impinging intensity at each segment
		double kappa = calc_opacity(sr_c->oc, maxrho, u / sr_c->units->temperature_to_internalenergy_simunit); // 10.0;
		double depth = (maxrho *  (L / (right-left+1)) * kappa)/2;
		double ai = 0;

		// Do caching instead
		// Ray index
		int idx = todo[i];
		int owner = idx/sr_c->rays_per_rank;
		
		// Final rank might have overflow
		if(owner >= sr_c->n_ranks) owner = sr_c->n_ranks-1;
		// Remap to local ray index
		idx -= owner * sr_c->rays_per_rank;
		int sidx = 0;
		float* r_seg;
		for(int j = left; j <= right; j++)
		{
			// Remap to segment index
			sidx = (idx * n_seg) + j; 
			// Aquire ray segment from cache
			if(owner != sr_c->rank)
				r_seg = (float*)mdlAquire(sr_c->mdl, CID_RAYS_INTENSE,  sidx, owner);
			else
				r_seg = &sr_c->local_rays_intensity[sidx];

			// Get absorbed intensity
			ai += *r_seg *  (1 - exp(-depth) );

			// Release it again
			if(owner != sr_c->rank)
				mdlRelease(sr_c->mdl, CID_RAYS_INTENSE,  (void*)r_seg);
		}
		
		// Store optical depth for cooling
		// int j = left;
		// // Remap to segment index
		// sidx = (idx * n_seg) + j; 
		// // Aquire ray segment from cache
		// if(owner != sr_c->rank)
		// 	r_seg = (float*)mdlAquire(sr_c->mdl, CID_RAYS_TAU,  sidx, owner);
		// else
		// 	r_seg = &sr_c->local_rays_tau[sidx];

		// Just store the optical depth calculated for the particle, not the depth
		// between the particle and the source...
		if(i==0) *od = depth;//*r_seg;

		// Release it again
		//if(owner != sr_c->rank)
		//	mdlRelease(sr_c->mdl, CID_RAYS_TAU,  (void*)r_seg);	

		// Tau is really impinging intensity here (could use union to clarify this)
		//for(int j = left; j <= right; j++)
		//	ai += sr_c->local_rays[(neighbours[i] * n_seg) + j] *  depth;

		// Older ray struct version
		//for(int j = left; j <= right; j++)
		//	ai += sr_c->local_rays[neighbours[i]].tau[j] *  depth;

		// Scale  with square of distance to particle
		// This isnt necessary because 'intensity' is really luminosity
		//double F = ai * pow((radius/(len * sr_c->units.lengthunit_cgs)),2);
		// So we scale like so instead:

		// Scale to min(particle cross section, healpix pixel area)
		double pcs = M_PI * h2;
		double sphere_area = (4 * M_PI * len*len);
		double hpa = sphere_area /  sr_c->n_rays;
		double scale_factor = (pcs < hpa) ? pcs : hpa;

		// This is absorbed luminosity (erg s-1)
		double L0 = ai * (scale_factor / sphere_area);

		// Divide by mass  (erg s-1 g-1) = (cm2 s-3)
		double Ud = L0 / (m * sr_c->units->massunit_cgs);

		// Convert units and set particle udot (cm2 s-3 -> adim?)
		(*uDot) += (float)(Ud / sr_c->units->heatingunit_cgs);

	}
	// Tiny uDot causes float exceptions calculating timestep - it becomes too big and overflows

//  Radiative cooling was used for the first disk test case.  Turning off here.
	// Do cooling
//	  double m_cgs = m * sr_c->units->massunit_cgs;
//	  double rho_cgs = rho * sr_c->units->densityunit_cgs;
//        double T4 = pow(u / sr_c->units->temperature_to_internalenergy_simunit, 4); // convert u to temp & raise to fourth
//        double T4min = 10000;//pow(10,4); //k
//        if(T4 < T4min) T4 = T4min;
//        double tau_over_tau2p1 = *od / (pow(*od, 2) + 1);
//        double crt_pi36 = 4.8359758;//pow(36 * M_PI,1./3.);
//        double s = pow((m_cgs / rho_cgs), 1/3.);
//        double o_over_s = 5.6704E-5 / s;
//        double cool = crt_pi36 * o_over_s * (T4 - T4min) * tau_over_tau2p1;

        // Cooling is in erg/s/cm^3, convert to erg/s/g
//        double vol = m_cgs / rho_cgs;
//        cool *= (vol/m_cgs);

//        *uDot -= (float)(cool / sr_c->units->heatingunit_cgs);
        // Think about a 'good' value for this limit...
	if(*uDot < 1e-15 && *uDot > -1e-15) *uDot = 0;

//        printf("starrad diags udot u x y z cool udotcoolpart T4 %g %g %g %g %g %g %g %g\n",*uDot,u,x,y,z,cool,(float)(-cool / sr_c->units->heatingunit_cgs),T4); ///////////////////// debugging
//////////////////////        printf("starrad diags udot u x y z %g %g %g %g %g\n",*uDot,u,x,y,z); ///////////////////// debugging

	// Super big uDot also causes problems - timestep becomes too small.
	// I guess its not able to handle randomly putting a sun in the middle of a box of gas...
	// For now can just put a max uDot of 1../
	//if(*uDot > 1 ) *uDot = 1;
}

/*
	Get timestep from du and dt
*/
float get_sr_timestep_from_dudt(double u, double uDot, double eta)
{
	double dt = HUGE_VAL;
	if(uDot>0)
		dt = u/uDot;

	double dtMax = (dt * eta);
	float dtm = (float)dtMax;
	return dtm;
}	

/*
	Allocate memory for rays

	Could do this better - single buffer for all rays and index.
	Leave it like this until understanding what MDL needs
*/
void alloc_rays(MDL mdl, sr_context* sr_c, int rank, int n_ranks)
{
	// Alloc for rays
	int n_ray = nside2npix(sr_c->res);
	int local_n_ray = n_ray / n_ranks;
	int n_spare = 0;
	n_spare = n_ray % n_ranks;

	// Last rank takes uneven spread
	if(n_spare && rank == (n_ranks-1))
	{
		local_n_ray += n_spare;
	}

	// Alloc and clear rays
	// Should be some allocation fail check? 
	int n_seg =  sr_c->n_seg;
	sr_c->local_rays_tau = NULL;
	sr_c->local_rays_tau = (float*)mdlMalloc(mdl, local_n_ray*n_seg*sizeof(float));
	sr_c->local_rays_intensity = NULL;
	sr_c->local_rays_intensity = (float*)mdlMalloc(mdl, local_n_ray*n_seg*sizeof(float));
	if(sr_c->local_rays_tau == NULL || sr_c->local_rays_intensity == NULL)
	{
		printf("Ray allocation error on rank %d, mdlMalloc returned NULL!\n", rank);
	}
	memset((void*)sr_c->local_rays_tau, 0, sizeof(float)*(local_n_ray * n_seg));
	
	sr_c->n_local_rays = local_n_ray;
	sr_c->n_rays = n_ray;
	sr_c->rays_per_rank = n_ray/n_ranks;
}

/*
	Free memory for rays
*/
void free_rays(MDL mdl, sr_context* sr_c)
{
	/*for(long int i = 0; i < sr_c->n_rays; i++)
	{
		free(sr_c->local_rays[i].tau);
	}*/

	mdlFree(mdl, sr_c->local_rays_tau);
	mdlFree(mdl, sr_c->local_rays_intensity);
}

/*
	Wipe the ray storage for next step
*/
void clear_rays(sr_context* sr_c)
{
	memset((void*)sr_c->local_rays_tau, 0, sizeof(float)*(sr_c->n_local_rays * sr_c->n_seg));
	/*for(int i = 0 ; i < sr_c->n_rays; i++)
	{
		memset((void*)sr_c->local_rays[i].tau, 0, sizeof(float)*(sr_c->l_ray / sr_c->l_seg));
		sr_c->local_rays[i].count = 0;
		sr_c->local_rays[i].d = 0;
	}*/
	//sr_c->local_rays[id].tau = 0;
	//sr_c->local_rays[id].n_affecting_ps = 0;
	
	// Source
	//starsrc src = sr_c->src;
	//sr_c->local_rays[id].intensity = sr_c->src.intensity;

}


void print_ray_status(sr_context* sr_c)
{
	/*for(long i = 0; i < sr_c->n_rays; i++)
	{
		float averaged2 = 0;
		if(sr_c->local_rays[i].count > 0)
			averaged2 = sr_c->local_rays[i].d / sr_c->local_rays[i].count; 
		printf("Ray %ld np %ld, d %f, av_d %f, tau[0]: %f\n", i, sr_c->local_rays[i].count, sr_c->local_rays[i].d, sqrt(averaged2), sr_c->local_rays[i].tau[0]);
	}*/
}


void get_affected_ray_neighbours(std::vector<int>& todo, int nside, float x, float y, float z, float l, float r2)
{
	// Shoudl have better error handling 
	if(todo.size() != 1)
	{
		printf("Starrad get_affected_ray_neighbours(): todo vector must contain initial affected ray\n");
		exit(0);
	}

	double ray_vec[3];
	int neighbours[8];
	int nneigh;
	int irid = todo[0];
	for(unsigned i = 0; i < todo.size(); i++)
	{
		// get list of neighbours
		neighbours_nest(&nside, &irid, neighbours, &nneigh);

		for(int j = 0; j < nneigh; j++)
		{
			// Get ray
			pix2vec_nest(nside, neighbours[j], ray_vec);

			// Project particle onto ray
			double a1 = (x * ray_vec[0]) + (y * ray_vec[1]) + (z * ray_vec[2]);
			double a12 = a1*a1;

			// Get distance2 from particle center to ray
			double a2 = (l*l) - a12;

			// rl2 is half length of ray segment affected by gas
			double rl2 = (4*r2 - a2);

			// if its negative theres no intersection
			if(rl2 < 0)
				continue;

			// If it intersects, check its not already in the todo list
			bool found = false;
			for(unsigned k = 0; k < todo.size(); k++)
			{
				if(neighbours[j] == todo[k])
				{
					found = true;
					break;	
				}
			}

			// If it wasnt in the todo list, add it
			if(!found)
				todo.push_back(neighbours[j]);
		}

	}	
}
