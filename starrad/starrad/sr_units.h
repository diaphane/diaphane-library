#ifndef DIAPHANE_STARRAD_UNITS_H
#define DIAPHANE_STARRAD_UNITS_H
/*
	Temporary units structure
	Would be nice to use fld CodeUnits - perhaps have that in 'common'
*/

//----------------------------------------------------------------------
// Sim to CGS 
//----------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif


//----------------------------------------------------------------------
// Radiometry 
//----------------------------------------------------------------------

double temperature_to_flux(double e, double T);
double temperature_to_spectral_exitance(double v, double T);
double flux_to_luminosity(double F, double r);
double luminosity_to_intensity_iso(double L);
double intensity_to_luminosity_iso(double I);
double energy_to_temperature(double U, double gamma, double mu);
//double energy_to_temperature_planck(double v, )


#ifdef __cplusplus
 }
#endif

#endif
