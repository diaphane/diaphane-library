#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "chealpix.h"

// extern void __pix_neighbours_MOD_neighbours_nest(int*, int*, int*, int*);
 extern void __pix_neighbours_MOD_neighbours_nest(int*, int*, int*, int*);


double x[] = {12.2, 1, 0.2, 5.5, 9.0};
double y[] = {6.5, 3.2, 9.0, 0.1, 7.7};
double z[] = {9.9, 8.7, 2.4, 0.4, 5.5};

double ts[] = {0.03, 2.32, 1.52, 0.77, 3.11};
double ps[] = {2.77, 5.99, 0.23, 1.11, 0.88};
double vecp[] = {0,0,0};


int main(int argc, char** argv)
{
	// Get nsides and print corresponding npixels
    if(argc!=2)
    {
        printf("Pass nside as argument\n");
        exit(-1);
    }
	long nside = atoi(argv[1]);
	long npix = nside2npix(nside);

	printf("Nside: %ld, npix: %ld\n", nside, npix);

	// Print all of the angles
	/*
	double theta;
	double phi;
	for(long i = 0; i < npix; i++)
	{
		pix2ang_nest(nside, i, &theta, &phi);
		printf("Pixel %ld: theta %f, phi %f\n", i, theta, phi);
	}
	*/

	// Take a couple of random angles and give the pixel id, and cartesian coordinate
	// Theta is north to south 0 -> pi, Phi is angle 0 -> 2pi
	long pid;
	for(unsigned i = 0; i < 5; i++)
	{
		ang2pix_nest(nside, ts[i], ps[i], &pid);
		pix2vec_nest(nside, pid, vecp);

		printf("For theta %f and phi %f: Pixel %ld, Vector: %f, %f, %f\n", ts[i], ps[i], pid, vecp[0], vecp[1], vecp[2]);
	}

	int pix = 4;
	int* neighbours = malloc(sizeof(int)*8);
	int count = 0;
	int inside = nside;
	printf("neighbours ptr: %p\n", neighbours);
	__pix_neighbours_MOD_neighbours_nest(&inside, &pix, neighbours, &count);

	printf("pix: %d, neighour count: %d\n", pix, count);
	for(unsigned i = 0; i < 8; i++)
		printf("Pixel neighbour %d: %d\n", i, neighbours[i]);
	//free neighbours
	// Take some random points, calculate the pixel they fall into and how far their center is from the pixel center (i.e. ray)
	double len;
	double nvec[3];
	double a11[3];
	for(unsigned i = 0; i < 5; i++)
	{
		// Normalize vector
		len = sqrt(x[i] * x[i] + y[i] * y[i] + z[i] * z[i]);
		nvec[0] = x[i]/len;
		nvec[1] = y[i]/len;
		nvec[2] = z[i]/len;

		// Get affected pixel
		vec2pix_nest(nside, (const double*) nvec, &pid);

		// Get ray
		pix2vec_nest(nside, pid, vecp);

		// Project particle onto ray
		double a1 = (x[i] * vecp[0]) + (y[i] * vecp[1]) + (z[i] * vecp[2]);

		// Get distance from particle center to ray
		double r = sqrt((len*len) - (a1 * a1));


		printf("For particle {%.1f,%.1f,%.1f}, pid: %ld, distance: %f\n", x[i], y[i], z[i], pid, r);

	}


	return 0;
}
