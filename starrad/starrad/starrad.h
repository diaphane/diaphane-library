#ifndef DIAPHANE_STARRAD_H
#define DIAPHANE_STARRAD_H

/* 
	Some blurb about starrad main header
*/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <mpi.h>   // include mpi and mdl here because cannot be inside the extern C
#include "mdl.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "sr_types.h"
/* 
	Init/end
*/
void sr_init(DiaphaneContext* diaphane_context);
void sr_end(sr_context* sr_c);

/*
	Real starrad
*/
void sr_set_src(sr_context* sr_c, double x, double y, double z,double temp, double radius);
void compute_tau(sr_context* sr_c, float smooth2, float rho, float m, float x, float y, float z, float u);
void sr_integrate_tau(sr_context* sr_c);
void sr_tau2intensity(sr_context* sr_c);
void sr_absorb_energy(sr_context* sr_c, float h2, float rho, float m, float x, float y, float z, float u, double* uDot, double* od);
float get_sr_timestep_from_dudt(double u, double udot, double eta);

/* 
	Ray handling
 */
void alloc_rays(MDL mdl, sr_context* sr_c, int rank, int n_ranks);
void free_rays(MDL mdl, sr_context* sr_c);
void clear_rays(sr_context* sr_c);
void print_ray_status(sr_context* sr_c);

#ifdef __cplusplus
 }
#endif

#endif
