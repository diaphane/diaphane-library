
#include <math.h>
#include <cstdio>
#include "sr_units.h"
//#include "setunits.h"
//
//
// All this temporary - swap to CodeUnits object asap
//
//
//


// Constants (Can use CodeUnits statics instead of these...)

// Stefan-Boltzmann constant 
#define SR_SIGMA 	5.67051E-5
// Boltmann constant (g cm^2 s^-2 K^-1) (erg K^-1)
#define SR_BOLTZ	1.380658E-16
// Proton mass (g)
#define SR_PMASS	1.672621E-24
// Planck constant 
#define SR_PLANCK	6.6260755E-27
// Speed of light squared
#define SR_C2		8.9875518e+20
// Gravitational constant (CGS) only to get time unit
#define SR_GRAV	6.67259E-8	
#define SR_PI		3.14159265359


//----------------------------------------------------------------------
// Radiometry 
//----------------------------------------------------------------------

// Convert temperature to radiant exitance or flux
// using Stefan-Boltzmann law
//
// J (erg cm^-2 s^-1) = emissivity (unitless) * sigma (erg cm^-2 s^-1 K^-4) * T^4 (K)
//

double temperature_to_flux(double e, double T)
{
	return e * SR_SIGMA * (pow(T,4));
}

double temperature_to_spectral_exitance(double v, double T)
{
	// planck
	return ((2 * SR_PLANCK * (v*v*v)) / SR_C2 ) * (1 / (exp( (SR_PLANCK * v) / (SR_BOLTZ * T)  ) -1 ));
}

// Convert flux (or exitance) to luminosity
// 
// luminosity (erg s^-1) = F (erg cm^-2 s^-1) * surface_area (cm^2)
// 

double flux_to_luminosity(double F, double r)
{
	return F * (4 * SR_PI * (pow(r,2)));
}

// Convert luminosity to an isotropic radiant intensity 
//
// intensity (erg s^-1 sr^-1) =  luminosity (erg s^-1) / 4 * pi (sr)
//

double luminosity_to_intensity_iso(double L)
{
	return L/(4*SR_PI);
}

// Do the reverse (assuming intensity received everywhere)
double intensity_to_luminosity_iso(double I)
{
	return I * (4 * SR_PI);
}


// Convert energy received to Temp
// Using equivalent equations of state 
//
// (rho * Kb * T) / (mu * Mp)  = rho * U (gamma - 1)
//
// Units  (erg * adim * adim * g  / erg * K^-1  )

double energy_to_temperature(double U, double gamma, double mu)
{
	return (U * (gamma-1) * mu * SR_PMASS) / SR_BOLTZ; 
}


