#ifndef SR_GASOLINEWRAPPER_MASTER_H
#define SR_GASOLINEWRAPPER_MASTER_H

/*
 Starrad gasoline wrapper master header
 Contains msr functions to be called from pkd master layer
*/

/* pkd master layer header */
#include "master.h"
#include "srgasolinewrapper_pst.h"

//void msrInitStarrad(MSR msr, double dTime);
void msrInitStarradStep(MSR msr);
void msrDoStarradStep(MSR msr, double dTime);
void msrFinishStarradStep(MSR msr);

#endif
