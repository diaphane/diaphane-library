/*
 Starrad gasoline wrapper master c file
 Contains msr layer function definitions  
*/

#include "srgasolinewrapper_master.h"
 #include "sr_types.h"
#include <string.h>
/* 
 One time Starrad initialization [called from msrInitSph()]
*/
//void msrInitStarrad(MSR msr, double dTime)
//{
//	// Some initialization
//	// Work out timestep?
//
//	// Allocate an msr context 
//	sr_context* msr_sr_c = (sr_context*)malloc(sizeof(sr_context));
//	if(!msr_sr_c)
//	{
//		// Throw error
//	}
//	
//	// Put units and diaphane parameter file from gasoline parameters into context
// 	sr_set_units(&(msr_sr_c->units),  msr->param.dKpcUnit, msr->param.dMsolUnit, msr->param.dMeanMolWeight, msr->param.dConstGamma);
// 	strcpy(msr_sr_c->params_fn, msr->param.achDpnParams);
//
//	// Pass on to pst layer
//	pstInitStarrad(msr->pst, msr_sr_c, sizeof(sr_context));
//
//	// Do a starrad step to init timestep etc
//	msrDoStarradStep(msr, dTime);
//
//	// Dont need this anymore
//	free(msr_sr_c);
//}
//
/*
 Per step starrad initialization [msrDoStarradStep()]
 ... Could just put these directly in msrDoStarradStep...
*/
void msrInitStarradStep(MSR msr)
{
	// Some initialization

	// Pass on to pst layer
	pstInitStarradStep(msr->pst, NULL, sizeof(NULL), NULL, NULL); // null fields to match mdl 
}

/*
 Master starrad step [TopStepKDK()]
*/
void msrDoStarradStep(MSR msr, double dTime)
{
	msrInitStarradStep(msr);

	// Step one -> calculate optical depth
	// 0 means asymmetric (gather only), which I guess is true for starrad
	msrReSmooth(msr, dTime, SMX_STARRAD1, 0);

	// Step two -> integrate intensity locally
	pstIntegrateIntensity(msr->pst, NULL, sizeof(NULL), NULL, NULL); // null fields to match mdl 

	// Step three -> apply to particles
	msrReSmooth(msr, dTime, SMX_STARRAD2, 0);

	msrFinishStarradStep(msr);
}

/*
 Per step starrad finalization [msrDoStarradStep()]
*/
void msrFinishStarradStep(MSR msr)
{
	// Some finalization

	// Pass onto pst layer
	pstFinishStarradStep(msr->pst, NULL, sizeof(NULL), NULL, NULL); // null fields to match mdl 
}
