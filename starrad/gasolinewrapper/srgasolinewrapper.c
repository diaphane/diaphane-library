/*
 Starrad gasoline wrapper c file
 final layer between gasoline wrapper and starrad itself
 each function called per processor:
 	msr -> pst -> via mdl for parallel calls -> here
*/
//#define VERBOSE_STARRRAD
#include "srgasolinewrapper.h"

// void InitStarrad(MDL mdl, PKD pkd, sr_context* msr_sr_c)
// {
// #ifdef VERBOSE_STARRAD
// 	printf("STARRAD: One time init context & alloc rays\n");
// #endif
// 	// Initialize starrad itself
// 	sr_init(mdl, &pkd->dpn_context->starrad, &msr_sr_c->units, msr_sr_c->params_fn);

// 	// Get unit conversions from master struct
// 	//pkd->dpn_context->starrad->units->= msr_sr_c->units;

// 	// Add fake source for the moment in cgs
// 	pkd->dpn_context->starrad->src.pos[0] = 0;
// 	pkd->dpn_context->starrad->src.pos[1] = 0;
// 	pkd->dpn_context->starrad->src.pos[2] = 0;
// 	pkd->dpn_context->starrad->src.temp = 5800; // Roughly solar surface temp
// 	pkd->dpn_context->starrad->src.radius = 6.958E10; // sol r in cm2

// 	sr_init_srcs(pkd->dpn_context->starrad);
// }

void InitStarradStep(MDL mdl, PKD pkd)
{
#ifdef VERBOSE_STARRAD	
	printf("STARRAD: Per step ray initialization\n");
#endif
	/*
		Cycle through particles, calculate ray from particle to source
		Initialise rays, e.g. zero tau etc.
		Communicate ray segments to other processors  (do parallel bit later?)
	*/
	sr_context* sr_c = pkd->dpn_context->starrad;
	// Initialize rays
	clear_rays(sr_c);

	// Open combiner cache
	mdlCOcache(mdl,CID_RAYS_TAU, sr_c->local_rays_tau, sizeof(float), (sr_c->n_local_rays * sr_c->n_seg),initRay,combRay);
#ifdef VERBOSE_STARRAD
	printf("Combiner cache opened\n");
#endif
}

// Step 1: optical depth
void DoStarradStep1(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf)
{

	sr_context* sr_c = smf->pkd->dpn_context->starrad;

	compute_tau(sr_c, ppart->fBall2/4, ppart->fDensity, ppart->fMass, ppart->r[0], ppart->r[1], ppart->r[2], ppart->u);

	// pass kernel type? default bspline

}

void IntegrateIntensity(MDL mdl, PKD pkd)
{
	// Close combiner cache
	mdlFinishCache(mdl, CID_RAYS_TAU);
	
#ifdef VERBOSE_STARRAD
	printf("Combiner cache closed\n");
#endif

	sr_context* sr_c = pkd->dpn_context->starrad;

	// First integrate optical depth
	sr_integrate_tau(sr_c);

	// Then intensity
	sr_tau2intensity(sr_c);

	// Open read only cache
	mdlROcache(mdl,CID_RAYS_INTENSE, sr_c->local_rays_intensity, sizeof(float), (sr_c->n_local_rays * sr_c->n_seg));	
	mdlROcache(mdl,CID_RAYS_TAU, sr_c->local_rays_tau, sizeof(float), (sr_c->n_local_rays * sr_c->n_seg));	

#ifdef VERBOSE_STARRAD
	printf("Readonly cache opened\n");
#endif
}



// Step 2: absorption
void DoStarradStep2(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf)
{

	sr_context* sr_c = smf->pkd->dpn_context->starrad;

	sr_absorb_energy(sr_c, ppart->fBall2/4, ppart->fDensity, ppart->fMass, ppart->r[0], ppart->r[1], ppart->r[2], ppart->u, &(ppart->sruDot), &(ppart->od));

	//printf("Particle uDot: %e\n", ppart->sruDot);

	// pass kernel type? default bspline


}

void FinishStarradStep(MDL mdl, PKD pkd)
{
#ifdef VERBOSE_STARRAD
	printf("STARRAD: Per step finalization\n");
#endif
	// Close read only cache
	mdlFinishCache(mdl, CID_RAYS_INTENSE);
	mdlFinishCache(mdl, CID_RAYS_TAU);
#ifdef VERBOSE_STARRAD
	printf("Readonly cache closed\n");
	print_ray_status(pkd->dpn_context->starrad);
#endif

if (pkd->dpn_context->starrad->logging != 0)
{
	 char name[128];
	 sprintf(name, "iorder_xyztu_od_uDot_rank%d_substep%d.log", mdl->idSelf, pkd->dpn_context->starrad->step);
	 pkd->dpn_context->starrad->step++;
	 FILE* fp = fopen(name, "w");
	 PARTICLE* p = pkd->pStore;
	 long i = 0;
	 for(; i < pkdLocal(pkd); i++)
	 {
	 	float k =  p[i].u / pkd->dpn_context->starrad->units->temperature_to_internalenergy_simunit;
                fprintf(fp, "%i\t%f\t%f\t%f\t%f\t%f\t%f\t%E\t%E\t%E\t%E\n", p[i].iOrder, p[i].r[0], p[i].r[1], p[i].r[2],k,k*pkd->dpn_context->starrad->units->temperature_to_internalenergy_cgs,p[i].od, p[i].sruDot,p[i].sruDot*pkd->dpn_context->starrad->units->heatingunit_cgs,p[i].fDensity,p[i].fDensity*pkd->dpn_context->starrad->units->densityunit_cgs); 
	}
	 fclose(fp);
	pkd->dpn_context->starrad->logging = 0;
}


}

void FinishStarrad(PKD pkd)
{

/* 
	Deallocate memory

	mdlFree
*/ 
}

void init_SR_particle(void* vp)
{
	// Could use some c++ cast
	PARTICLE* p = (PARTICLE*)vp;
	p->sruDot = 0;
	p->dtSRmax = HUGE_VAL;
}


void get_SR_maxdt_particle(PARTICLE* p, SMF* smf)
{
	// pass u, udot, eta 
	if(is_gas_particle_active(p))
		p->dtSRmax = get_sr_timestep_from_dudt(p->u, p->sruDot, smf->pkd->dpn_context->starrad->eta);
}

  /*   active on this time(sub)step   */
int is_gas_particle_active(PARTICLE *ppart)
{
  if(TYPEQueryACTIVE(ppart))  
    {
      return 1;
    }
  return 0;
}


void initRay(void* r)
{
 	*((float*)r) = 0;
}

void combRay(void* r1, void* r2)
{
	*((float*)r1) += *((float*)r2);
}
