#ifndef SR_GASOLINEWRAPPER_PST_H
#define SR_GASOLINEWRAPPER_PST_H

/*
 Starrad gasoline wrapper processor set tree layer c file
 Contains pst layer function declarations
*/

#include "srgasolinewrapper.h"
/* pkd pst layer header*/
#include "pst.h"

//void pstInitStarrad(PST pst, void* msr_sr_c, size_t sr_c_size);
//void pstInitStarradStep(PST pst);
//void pstFinishStarradStep(PST pst);
//void pstIntegrateIntensity(PST pst);
void pstInitStarradStep(PST pst, void* vin, int nIn, void* vout, int* pnOut);
void pstFinishStarradStep(PST pst, void* vin, int nIn, void* vout, int* pnOut);
void pstIntegrateIntensity(PST pst, void* vin, int nIn, void* vout, int* pnOut);


#endif
