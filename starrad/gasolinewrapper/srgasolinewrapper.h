#ifndef SR_GASOLINEWRAPPER_H
#define SR_GASOLINEWRAPPER_H

/*
 Starrad gasoline wrapper header
 final layer between gasoline wrapper and starrad itself
 each function called per processor:
 	msr -> pst -> via mdl for parallel calls -> here
*/

#include <stdio.h>
#include <mpi.h>
#include "mdl.h"


/* pkd headers */
#include "pkd.h"
#include "smoothfcn.h"

#include "starrad.h"

/*  functions called through pst layer */
 // Pass mdl to do parallel in starrad.
//void InitStarrad(MDL mdl, PKD pkd, sr_context* msr_sr_c);
void InitStarradStep(MDL mdl, PKD pkd);
void DoStarradStep1(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf);
void DoStarradStep2(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf);
void FinishStarradStep(MDL mdl, PKD pkd);
void IntegrateIntensity(MDL mdl, PKD pkd);


void initRay(void* r);
void combRay(void* r1, void* r2);
void init_SR_particle(void* vp);
void get_SR_maxdt_particle(PARTICLE* p, SMF* smf);
// Should get this from common, as it comes from fldgasolinewrapper...
int is_gas_particle_active(PARTICLE *ppart);
#endif
