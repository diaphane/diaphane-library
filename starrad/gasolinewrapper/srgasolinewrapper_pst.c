/*
 Starrad gasoline wrapper processor set tree layer c file
 Contains pst layer function definitions
*/

 #include "srgasolinewrapper_pst.h"


/*
 PST functions walk the processor set tree via the mdl layer, calling the equivalent 
 starrad gasoline wrapper function on each processor as necessary.
*/

/*
One time starrad initialization [msrInitStarrad()]
*/
//void pstInitStarrad(PST pst, void* msr_sr_c, size_t sr_c_size)
//{
//
//  	if(pst->nLeaves > 1) 
//  	{
//  	  	/* Recursive tree walk, request service and call pst init on lower
//  	  	    NULL is input data to service, 0 is size of data in bytes 
//                ReqService does a shallow send, i.e. dont send a structure containing 
//                allocated pointers and expect them to be valid...
//            */
//  		mdlReqService(pst->mdl, pst->idUpper, PST_INITSTARRAD, msr_sr_c, sr_c_size);
//  		pstInitStarrad(pst->pstLower, msr_sr_c, sr_c_size);
//  		mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
//  	} 
//  	else
//  	{
//  		/* FLD is using PKD struct to store fld context, copy this for
//	  	    now as we might need tree anyway for starrad setup... */
//		InitStarrad(pst->mdl, pst->plcl->pkd, (sr_context*)msr_sr_c); 
//  	}
//}

/*
 Per step starrad initialization [msrInitStarradStep()]
*/
//void pstInitStarradStep(PST pst)
void pstInitStarradStep(PST pst, void* vin, int nIn, void* vout, int* pnOut)
{
  	if(pst->nLeaves > 1) 
  	{
  		mdlReqService(pst->mdl, pst->idUpper, PST_INITSTARRADSTEP, NULL, 0);
//		pstInitStarradStep(pst->pstLower);
                pstInitStarradStep(pst->pstLower, vin, nIn, NULL, NULL);
  		mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
  	} 
  	else
  	{
		InitStarradStep(pst->mdl, pst->plcl->pkd); 
  	}
}

/*
 Per step starrad finalization [msrFinishStarradStep()]
*/
void pstFinishStarradStep(PST pst, void* vin, int nIn, void* vout, int* pnOut)
{
  	if(pst->nLeaves > 1) 
  	{
  	  	/* Recursive tree walk, request service and call pst init on lower
  	  	    NULL is input data to service, 0 is size of data in bytes */
  		mdlReqService(pst->mdl, pst->idUpper, PST_FINISHSTARRADSTEP, NULL, 0);
  		pstFinishStarradStep(pst->pstLower, vin, nIn, NULL, NULL);
  		mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
  	} 
  	else
  	{
  		/* FLD is using PKD struct to store fld context, copy this for
	  	    now as we might need tree anyway for starrad setup... */
		FinishStarradStep(pst->mdl, pst->plcl->pkd); 
  	}
}


/*
  Local integration between SR Step1 & SR Step2
*/
void pstIntegrateIntensity(PST pst,void* vin, int nIn, void* vout, int* pnOut)
{
    if(pst->nLeaves > 1) 
    {
        /* Recursive tree walk, request service and call pst init on lower
            NULL is input data to service, 0 is size of data in bytes */
      mdlReqService(pst->mdl, pst->idUpper, PST_INTEGRATEINTENSITY, NULL, 0);
      pstIntegrateIntensity(pst->pstLower, vin, nIn, NULL, NULL);
      mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
    } 
    else
    {
      /* FLD is using PKD struct to store fld context, copy this for
          now as we might need tree anyway for starrad setup... */
    IntegrateIntensity(pst->mdl, pst->plcl->pkd); 
    }
}
