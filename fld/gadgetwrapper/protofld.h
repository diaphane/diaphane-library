/*! \file proto.h
 *  \brief this file contains all function prototypes of the code -- the bits for Flux Limited Diffusion added to gadget-like functions
 */


/* added functions for Flux Limited Diffusion -- gradentropy.c */
int    gradenergy_compare_key(const void *a, const void *b);
void   compute_grad_energy_particle(int target, int mode);
void   compute_grad_energy(void);

/* FLD in fldrates.c */
int    fld_compare_key(const void *a, const void *b);
void   compute_FLD_rates_particle(int target, int mode, int *neighbor_id_list_int, float* flddatablockfloat);
void   compute_FLD_rates(int* neighbor_id_list_int, float* flddatablockfloat);

