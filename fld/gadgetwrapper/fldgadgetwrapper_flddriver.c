#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"
//#include "protofld.h"
#include "fldgadgetwrapper.h"

/*! \file fldrates.c
 *  \brief calculate FLD energy (and entropy) heating cooling rate
 *
 *   based on hydra.c 
 * 
 *   we *should* rewrite this driver in a more modular way -- this function 
 *      is really quite a mess
 */


static double hubble_a, atime, hubble_a2, fac_mu, fac_vsic_fix, a3inv, fac_egy;

#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif



/*! This function is the driver routine for the calculation of 
 *  Flux Limited Diffusion ydrodynamical rate of change of entropy for all active
 *  particles.

     -- it prepares the neighbor list etc

 */
void compute_FLD_driver(DiaphaneContext *diaphane_context)
{
  long long ntot, ntotleft;
  int i, j, k, n, ngrp, maxfill, source, ndone;
  int *nbuffer, *noffset, *nsend_local, *nsend, *numlist, *ndonelist;
  int level, sendTask, recvTask, nexport, place;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
  MPI_Status status;

//  FLDContextStruct *fldcontext = diaphane_context->fld;


#ifdef PERIODIC
  boxSize = All.BoxSize;
  boxHalf = 0.5 * All.BoxSize;
#ifdef LONG_X
  boxHalf_X = boxHalf * LONG_X;
  boxSize_X = boxSize * LONG_X;
#endif
#ifdef LONG_Y
  boxHalf_Y = boxHalf * LONG_Y;
  boxSize_Y = boxSize * LONG_Y;
#endif
#ifdef LONG_Z
  boxHalf_Z = boxHalf * LONG_Z;
  boxSize_Z = boxSize * LONG_Z;
#endif
#endif

  if(All.ComovingIntegrationOn)
    {
      /* Factors for comoving integration of fld */
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;

      hubble_a = All.Hubble * sqrt(hubble_a);
      hubble_a2 = All.Time * All.Time * hubble_a;

      fac_mu = pow(All.Time, 3 * (GAMMA - 1) / 2) / All.Time;

      fac_egy = pow(All.Time, 3 * (GAMMA - 1));

      fac_vsic_fix = hubble_a * pow(All.Time, 3 * GAMMA_MINUS1);

      a3inv = 1 / (All.Time * All.Time * All.Time);
      atime = All.Time;
    }
  else
    hubble_a = hubble_a2 = atime = fac_mu = fac_vsic_fix = a3inv = fac_egy = 1.0;


  /* `NumSphUpdate' gives the number of particles on this processor that want a force update */
  for(n = 0, NumSphUpdate = 0; n < N_gas; n++)  /* N_gas (on local processor) */
    {
      if(P[n].Ti_endstep == All.Ti_Current)
	NumSphUpdate++;
      //      initFLDParticlestepfloat(P[n]); /* zero initialize  gradentropy, */
    }

  numlist = malloc(NTask * sizeof(int) * NTask); /* looks like a 1-d array -- why is this malloc'd as a 2-d array?????????? */
  MPI_Allgather(&NumSphUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);

  
  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);


  i = 0;			/* first particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */

  while(ntotleft > 0)
    {
      for(j = 0; j < NTask; j++)
	nsend_local[j] = 0;

      /* do local particles and prepare export list of task(s) to send to */
      tstart = second();
      for(nexport = 0, ndone = 0; i < N_gas && nexport < All.BunchSizeFLD - NTask; i++)
	if(P[i].Ti_endstep == All.Ti_Current)
	  {
	    ndone++;

	    for(j = 0; j < NTask; j++)
	      Exportflag[j] = 0;
	    /* for local particle contribution  */
	    /* (neighbor_id_list_int is still a blank memory block) */
	    compute_FLD_driver_particle(i, 0, diaphane_context);
	    for(j = 0; j < NTask; j++) 
	      {
		if(Exportflag[j])  /* (global extern) modified in ngb_treefind_pairs() */
		  { /* everything needed for the FLD must be exported here */
		    /* it would be slower, but much simpler to export the entire particle struct........ */
		    for(k = 0; k < 3; k++)
		      {
			FLDDataIn[nexport].Pos[k] = P[i].Pos[k];
		      }
		    FLDDataIn[nexport].Density = SphP[i].Density;
		    FLDDataIn[nexport].Entropy = SphP[i].Entropy;
		    FLDDataIn[nexport].Hsml = SphP[i].Hsml;
		    FLDDataIn[nexport].Mass = P[i].Mass;
		    //		    FLDDataIn[nexport].Pressure = SphP[i].Pressure; 
		    for(k = 0; k < 3; k++)
		      {
			FLDDataIn[nexport].GradEnergy[k] = SphP[i].GradEnergy[k];
		      }
		    FLDDataIn[nexport].Index = i;
		    FLDDataIn[nexport].Task = j; /* export to Task j? */
		    nexport++;
		    nsend_local[j]++;
		  }
	      }
	  }
      tend = second();
      timecomp += timediff(tstart, tend);

      qsort(FLDDataIn, nexport, sizeof(struct flddata_in), fld_compare_key);

      for(j = 1, noffset[0] = 0; j < NTask; j++)
	noffset[j] = noffset[j - 1] + nsend_local[j - 1];

      tstart = second();

      MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

      tend = second();
      timeimbalance += timediff(tstart, tend);



      /* now do the particles that need to be exported 
	 i.e. -- get the contribution from particles on other processes
       */

      for(level = 1; level < (1 << PTask); level++)
	{
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeFLD)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* get the particles -- should rename the FLDDataGet struct to look more like a stripped down particle instead of a new struct with unknown members */
		      MPI_Sendrecv(&FLDDataIn[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct flddata_in), MPI_BYTE,
				   recvTask, TAG_FLD_A,
				   &FLDDataGet[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct flddata_in), MPI_BYTE,
				   recvTask, TAG_FLD_A, MPI_COMM_WORLD, &status);
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  /* now do the imported particles */
	  /* this is all very repetitive code and should be modularized and abstracted */
	  tstart = second();
	  for(j = 0; j < nbuffer[ThisTask]; j++)
	    compute_FLD_driver_particle(j, 1, diaphane_context);
	  tend = second();
	  timecomp += timediff(tstart, tend);

	  /* do a block to measure imbalance */
	  tstart = second();
	  MPI_Barrier(MPI_COMM_WORLD);
	  tend = second();
	  timeimbalance += timediff(tstart, tend);

	  /* get the result */
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeFLD)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* send the results */
		      MPI_Sendrecv(&FLDDataResult[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct flddata_out),
				   MPI_BYTE, recvTask, TAG_FLD_B,
				   &FLDDataPartialResult[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct flddata_out),
				   MPI_BYTE, recvTask, TAG_FLD_B, MPI_COMM_WORLD, &status);

		      /* add the result to the particles */
		      for(j = 0; j < nsend_local[recvTask]; j++)
			{
			  source = j + noffset[recvTask];
			  place = FLDDataIn[source].Index;
      /* increment dentropy/dt for non-local (virtual, ghost) particle -- already done for normal particles */
	/* we will update entropy due to FLD in dpngadgetwrapper, so do not add FLD to DtEntropy */
////			  SphP[place].DtEntropy += FLDDataPartialResult[source].EntropyDotFLD;
			  SphP[place].EntropyDotFLD += FLDDataPartialResult[source].EntropyDotFLD;
			}
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  level = ngrp - 1;
	}

      MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
      for(j = 0; j < NTask; j++)
	ntotleft -= ndonelist[j];
    }

  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);


  /* do final operations on results  -- add FLD Entropy rate change to total Entropy rate change -- this was moved to gadget wrapper */

  tstart = second();

  for(i = 0; i < N_gas; i++)
    if(P[i].Ti_endstep == All.Ti_Current)
      {
	/* check units -- need to be already in internal gadget units  */
	//	SphP[i].DtEntropy *= GAMMA_MINUS1 / (hubble_a2 * pow(SphP[i].Density, GAMMA_MINUS1));
	//	SphP[i].DtEntropy += FLDP[i].FLDEntropyDot;
	
#ifdef SPH_BND_PARTICLES   /* turns off SPH Accel if id=0 for testing */
	if(P[i].ID == 0)
	  {
	    SphP[i].DtEntropy = 0;
	    //	    for(k = 0; k < 3; k++)
	    //	      SphP[i].HydroAccel[k] = 0;
	  }
#endif
      }

  tend = second();
  timecomp += timediff(tstart, tend);

  /* collect some timing information */

  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  /*  ! should change these timing to FLD not Hydro */
  if(ThisTask == 0)
    {
      All.CPU_HydCompWalk += sumt / NTask;
      All.CPU_HydCommSumm += sumcomm / NTask;
      All.CPU_HydImbalance += sumimbalance / NTask;
    }
}


/*! This function is the 'core' of the SPH force computation. A target
 *  particle is specified which may either be local, or reside in the
 *  communication buffer.
 */
void compute_FLD_driver_particle(int target, int mode, DiaphaneContext *diaphane_context)
{
  int startnode, numngb;
  FLOAT *pos;
  FLOAT h_i;
  FLOAT entropyDotFLD, entropyDotFLDtemp;
  //  void* thisparticle;

  /* neighbors: here to XXX should probably be in the fldgadgetwrapper.c  */

/* These variables MUST agree with the size of struct flddata_in in allvars.h */
  //  FLOAT Pos[3];  FLOAT Hsml;  FLOAT Mass;  FLOAT Entropy; /* needed as input for FLD routine */   FLOAT Density;   FLOAT GradEnergy[3];  

  /* this 1st subset is needed to find the neighbors */

  if(mode == 0)
    {
      pos = P[target].Pos;
      h_i = SphP[target].Hsml;
    }
  else
    {
/* These variables MUST agree with the size of struct flddata_in in allvars.h */
      pos = FLDDataGet[target].Pos;
      h_i = FLDDataGet[target].Hsml;
    }

  entropyDotFLD = 0.; // initialize 
  entropyDotFLDtemp = 0.; 
 
  /* Now start the actual SPH computation for this particle */
  startnode = All.MaxPart;
  do
    {
      /******** 
       During 1st time through (mode=0), this gathers each neighbor 
       (within h_i) on this processor 
       and trips the export flag if a neighbor particle is on another process
        -- During 2nd time through, this particle (if exported) gathers 
           neighbors on each process (can be > 1) it was exported to, 
	   allowing it to gather all neighbors on other domains
	  --so this is not really a complete neighbor at any time 
       ********/
/* I think this creates global Ngblist of all neighor particles 
     that live on the local process.  
     "This" particle might be non-local ghost */

      numngb = ngb_treefind_pairs(&pos[0], h_i, &startnode); 


  /* for simplicity, want gather only, not symmetric loop, do not modify neighbors */
  /* Note that the kernel length is passed implicitly by global gas particle */
  /* The kernel function is computed within the FLD calc */
  /* every neighbor on the numngb NN list should be on local process, even if target is a ghost particle */

      /* Flux Limited Diffusion */
	// the particle (and imported ghost) particle data is global
      calc_fld_rates_particle(diaphane_context, numngb, target, mode, &entropyDotFLDtemp);     
      entropyDotFLD += entropyDotFLDtemp;
    }
  while(startnode >= 0);


  /* Now collect the result at the right place */
  if(mode == 0) /* the neighbors on this process */
    {
      /*  Might be better to make an FLDP particle (with entropyDotFLD member) instead of using the SphP DtEntropy (requires units be in correct internal entropy units now  */
      //      FLDP[target].EntropyDotFLD = entropyDotFLD;
      /* increment dentropy/dt for normal particle -- gather only */
        /* we will update entropy due to FLD in dpngadgetwrapper, so do not add FLD to DtEntropy */
////      SphP[target].DtEntropy += entropyDotFLD; // entropy DotFLD Init every step in hydra.c
      SphP[target].EntropyDotFLD = entropyDotFLD; // Inits every step
    }
  else  /* the neighbors on other processes */
    { /* These variables MUST agree with the size of struct flddata_out in allvars.h */
      /* do NOT increment dentropy/dt for virtual particle until result is export to home process */
      //      FLDDataResult[target].EntropyDotFLDDtEntropy = entropyDotFLD;
      FLDDataResult[target].EntropyDotFLD = entropyDotFLD;
    }
}




/*! This is a comparison kernel for a sort routine, which is used to group
 *  particles that are going to be exported to the same CPU.
 */
int fld_compare_key(const void *a, const void *b)
{
  if(((struct flddata_in *) a)->Task < (((struct flddata_in *) b)->Task))
    return -1;
  if(((struct flddata_in *) a)->Task > (((struct flddata_in *) b)->Task))
    return +1;
  return 0;
}
