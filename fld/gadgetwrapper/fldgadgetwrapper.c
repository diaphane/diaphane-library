///* fldgadgetwrapper.c */

/* interface between FLD library and simulation code layer */
/* Flux-Limited Diffusion  */

#include <math.h>
#include <assert.h> 
#include <malloc.h>
#include <string.h>


#include "proto.h"
#include "fld_types.h"
#include "fldgen.h"
#include "setunits.h"
#include "setup_fld.h"
#include "fldgadgetwrapper.h"
#include "essentials.h"

/* The FLD calculation, called each step -- called by each process */
/*    leaving this function is a wrapper because it has some sim-code dependent variables */
void DoFluxLimitedDiffusionStep(DiaphaneContext *diaphane_context)
{
  double tstart, tend, tdiff; 
  int nmaxneighbors;
  
  int neednewneighborlist;
  FLDContextStruct *fldcontext;
  fldcontext = diaphane_context->fld;

  neednewneighborlist = 1; /* set to 1 only if need to modify the codes internal neighbor list before calling    convert_sim_data_to_cgs_arrays.  i.e. 1 for gadget, 0 for gasoline */

  if(ThisTask == 0)
    {
      printf("Start Flux Limited Diffusion computation...\n");
      fflush(stdout);
    }
  tstart = second();            /* measure the time for FLD */


  nmaxneighbors = MAX_NGB;   /* gadget default of 20000 set in allvars.h (seems larger than needed) */ 

  printf("Allocating FLD memory for this step");   fflush(stdout); ///////////////////////////
  AllocateFluxLimitedDiffusionMemory(fldcontext, nmaxneighbors, neednewneighborlist); /* allocates FLD memory data block and for gadget a list of neighbors in a sphere */

  printf("computing fld step \n"); fflush(stdout); /////////////////////////////////////////// 
  computeFLDstep(diaphane_context); /* FLD and prerequisites */
  

  FreeFluxLimitedDiffusionMemory(fldcontext); // why free in a function?

  

  tend = second();
  tdiff = timediff(tstart, tend);
  if(ThisTask == 0)
    {
      printf("FLD computation done. took %10.4f sec\n",tdiff);
      fflush(stdout);
    }
}


/* for all particles */
void computeFLDstep(DiaphaneContext *diaphane_context)
{

  if(ThisTask == 0)
    {
      printf("computing energy gradients for FLD\n");
      fflush(stdout);
    }

  printf("will compute grad energy xxxxxxxxxxxxxxxxxxxxxxx \n"); fflush(stdout); ////////////////////////////////

  compute_grad_energy(); /* units: gadget internal */ /* energy gradient is calculated for gadget particle */
  /* Note the FLD diffusion constant *could* be done in main code as with grad_energy before calling FLD --- instead we wait, which means repeating calc for each nneibhor  */
  /* these hydro routines essentially replicate hydra.c, but what we want is a particle and its neighbors and their properties */

  /* 1st code must get neighbor properties, including import/export for ghosts */
  /* the code overhead is messy, but we use the code machinery for the neighbor finding  -- should make it more modular */
  /* FLD rates (entropy dot is incremented by the FLD contribution ) */
  /* entropy is later updated in kick (advance_and_find_timesteps) */

  if(ThisTask == 0)
    {
      printf("computing FLD rates\n");
      fflush(stdout);
    }
  printf("will compute fld rates xxxxxxxxxx xxxxxxxxxxxxxxxxxxxxxxx \n"); fflush(stdout); ////////////////////////////////

  compute_FLD_driver(diaphane_context); 

  if(ThisTask == 0)
    {
      printf("checking FLD timestep criterion\n");
      fflush(stdout);
    }
  getFLDmaxtimestep(diaphane_context->fld); /* store in P[i].dtFLDmax in sim units */
     /* Gadget already has a forced limit on cooling "we prevent that the entropy (and hence temperature decreases by more than a factor 0.5 */
}






/* done once per active particle, every step */
/* to initialize FLD fields, sets uDot=0, dtfld=huge  */
/* call before any FLD calc's are done each step  */
void InitFLDParticle(int iparticle)
{
  AFLOAT EntropyDotFLD;
  AFLOAT dtFLDmax;

  if (is_gadget_particle_active(iparticle)) /* else: ignore non-active particle */
    {
      initFLDParticledata(&EntropyDotFLD, &dtFLDmax);

      // typecast in case main code is using a different precision than this lib
      SphP[iparticle].EntropyDotFLD = (SIMFLOAT)EntropyDotFLD;
      SphP[iparticle].dtFLDmax = (SIMFLOAT)dtFLDmax;
    }
}






/* 
This function is called for every particle from the simulation code.
 Philosophically, this means that we let the main simulation code machinery
 run through its usual loop over all particles and make a list of its
 neighbors and take care of all of the communication across domains.
 This means quite a bit of code overhead that is more or less the wrapper/driver
 for the FLD library, and which must be recreated to match each simulation code
 that the library is used with.   
 Can we make this more modular?
  */
/* In gadget this particle may be a ghost -- but the neighbor list is local */
/* gadget particle info is global -- gadget cube neighbor list is global */
void calc_fld_rates_particle(DiaphaneContext *diaphane_context, int nneighbors_cube, int iparticle, int iamghost, SIMFLOAT *entropyDotFLD)
{
  int nneighbors;
  //  AFLOAT scale_factor;  /* This is just the scale factor 1/(1+z)? */
  AFLOAT dtfldmax;
  AFLOAT flduDot;
  AFLOAT rkernel, rkernel_cgs; /* smoothing length only need for *this* particle */
  AFLOAT *gradkernel(AFLOAT);
  SIMFLOAT rkernelx2, rkernelx2_squared;
  SIMFLOAT posx0, posy0, posz0, density;
  SIMFLOAT periodsimunits;
  FLDContextStruct *fldcontext = diaphane_context->fld;

  periodsimunits = All.BoxSize; /* gadget periodx=periody=periodz */
 
  /* everything needed for the FLD is pointed or copied to local variables here */

  /* this 1st subset is needed to find the neighbors */
  if (iamghost) /* "ghost particle" is a special partial particle struct */
    {
      posx0 = FLDDataGet[iparticle].Pos[0];
      posy0 = FLDDataGet[iparticle].Pos[1];
      posz0 = FLDDataGet[iparticle].Pos[2];
      rkernelx2 = FLDDataGet[iparticle].Hsml;     
      density = FLDDataGet[iparticle].Density;
    }
  else /* particle on local process is "normal" particle + gas partiacle */
    {
      posx0 = P[iparticle].Pos[0];
      posy0 = P[iparticle].Pos[1];
      posz0 = P[iparticle].Pos[2];
      rkernelx2 = SphP[iparticle].Hsml;    
      density = SphP[iparticle].Density;
    }
  
  rkernelx2_squared = rkernelx2 * rkernelx2;

  /* still in Gadget units for distances here */  
  nneighbors = neighbor_id_list_cube_to_sphere(posx0, posy0, posz0, fldcontext->neighbor_id_list, nneighbors_cube, rkernelx2_squared, fldcontext->sim->periodic, periodsimunits);


  convert_sim_data_to_cgs_arrays(nneighbors, diaphane_context, iparticle, iamghost);



  /* should make this definable from a param file ! and put in fldcontext  */
  /* kernel gradient/(r/rsmooth) without the prefactors  */
  grad_kernel_overnu_func_type grad_kernel_overnu_func = &bspline_grad_kernel_over_nu_nonorm;

  rkernel = rkernelx2/2.;
  //  rkernel = rkernel_given_fball2((AFLOAT)ppart->fBall2); /* gasoline --> rkernel */
  rkernel_cgs = rkernel * fldcontext->units->lengthunit_cgs ;

  /* clean up this call and put grad_kernel into fldcontext ........ */
      /* calculates flduDot == FLD d(internal energy)/dt in cgs */
  /* should FLDgen still return the dtFLDmax based on max of all pairs? */
  FLDgen(nneighbors, rkernel_cgs, grad_kernel_overnu_func, fldcontext, &flduDot, &dtfldmax);

  *entropyDotFLD = cgsenergydot_to_gadgetentropydot(flduDot, density, diaphane_context);  // density here is in sim units


      /* dtfldmax is not computable until flduDot inclues all neighbors */
      /* dtfldmax updates to main timestep in timestep.c */
      /* uDot, dtfldmax updated later, when all particles can be summed  */

}





/*    This functions fills in the data block of this particle and its neighbors, needed for FLD
 input: particle data sim units  -->   output: data as cgs arrays             
 P[iparticle] + SphP[iparticle] or  FLDDataGet[iparticle]  is "this particle")
     P, SphP, or FLDDataGet[neighbor_id_list[j]] is "neighbor particle"
 For cosmological runs, unit conversion context is already in proper units */
// should rename function to something fld-specific
void convert_sim_data_to_cgs_arrays(int nneighbors, DiaphaneContext *diaphane_context, int iparticle, int iamghost)
{
  AFLOAT *flddataarray;
  int i, j, index, indextmp;
  AFLOAT valuefloat;
  AFLOAT *gradkernel(AFLOAT);
  INTIDTYPE *neighbor_id_list;
  FLDContextStruct *fldcontext = diaphane_context->fld;

  /* pass all neighbor data, even for inactive particles, which we do not need because will gather only */

  flddataarray = fldcontext->flddatablock;
  neighbor_id_list = fldcontext->neighbor_id_list;

 /* There are nneighbors+1 particles stored and 2 of them are this particle,
  1 copy at index 0 in flddataarray and another at some other index.
  I think we do not know which particle it is in the neighbor list. */
 /* local particle is its own neigbhor. ghost particle cannot be. */

 /* if (iamghost) these are neighors to a particle on a different mpi process 
    else   local process particle  */

  index = 0;

  /* maybe we should have a pointer to each property of flddataarray intead of relying on this long sequence */


 /* -------- position */ 
 /* 1st store this particle */
 if (iamghost) 
   { /* imported particle */
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].Pos[0];
     index += 1;
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].Pos[1];
     index += 1;
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].Pos[2];
     index += 1;
   }
 else  
   { /* local particle */
     flddataarray[index] =  (AFLOAT)P[iparticle].Pos[0];
     index += 1;
     flddataarray[index] =  (AFLOAT)P[iparticle].Pos[1];
     index += 1;
     flddataarray[index] =  (AFLOAT)P[iparticle].Pos[2];
     index += 1;
   }
 for (i = 0; i < nneighbors; ++i)  /* store neighbor particles */
   {
     j = neighbor_id_list[i];
     
     flddataarray[index] = (AFLOAT)P[j].Pos[0];
     index += 1;
     flddataarray[index] = (AFLOAT)P[j].Pos[1];
     index += 1;
     flddataarray[index] = (AFLOAT)P[j].Pos[2];
     index += 1;	 
   }     

  /* convert pos units into cgs (cm) */	
 indextmp = index - 3*(nneighbors + 1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * fldcontext->units->lengthunit_cgs;
      flddataarray[i] = valuefloat;
    }


  /* -------- gradenergy  (internal energy gradient) */
  /* 1st store this particle */
 if (iamghost) 
   {    /* imported particle */ 
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].GradEnergy[0];
     index += 1;
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].GradEnergy[1];
     index += 1;
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].GradEnergy[2];
     index += 1;
   }
 else  
   {  /* local particle */
     flddataarray[index] =  (AFLOAT)SphP[iparticle].GradEnergy[0];
     index += 1;
     flddataarray[index] =  (AFLOAT)SphP[iparticle].GradEnergy[1];
     index += 1;
     flddataarray[index] =  (AFLOAT)SphP[iparticle].GradEnergy[2];
     index += 1;
   }
 for (i = 0; i < nneighbors; ++i)  /* then store neighbor particles */
   {
     j = neighbor_id_list[i];
     
     flddataarray[index] = (AFLOAT)SphP[j].GradEnergy[0];
     index += 1;
     flddataarray[index] = (AFLOAT)SphP[j].GradEnergy[1];
     index += 1;
     flddataarray[index] = (AFLOAT)SphP[j].GradEnergy[2];
     index += 1;	 
   }   


 /* convert gradEnergy units into cgs (erg/cm) */
 indextmp = index - 3*(nneighbors+1);
 for (i = indextmp; i < index; ++i)
   {
     valuefloat = flddataarray[i] * fldcontext->units->gradenergyunit_cgs ;
     flddataarray[i] = valuefloat;
    }



  /* -------- density */
  /* 1st store this particle */
 if (iamghost) 
   {    /* imported particle */ 
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].Density;
     index += 1;
   }
 else  
   {  /* local particle */
     flddataarray[index] =  (AFLOAT)SphP[iparticle].Density;
     index += 1;
   }
 for (i = 0; i < nneighbors; ++i)  /* then store neighbor particles */
   {
     j = neighbor_id_list[i];
     
     flddataarray[index] = (AFLOAT)SphP[j].Density;
     index += 1;
   }   

 /*  code density units to c.g.s. units (g/cm**3) */
  indextmp = index - (nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * fldcontext->units->densityunit_cgs;
      flddataarray[i] = valuefloat;
    }



  /* -------- mass */
  /* 1st store this particle */
 if (iamghost) 
   {    /* imported particle */ 
     flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].Mass;
     index += 1;
   }
 else  
   {  /* local particle */
     flddataarray[index] =  (AFLOAT)P[iparticle].Mass;
     index += 1;
   }
 for (i = 0; i < nneighbors; ++i)  /* then store neighbor particles */
   {
     j = neighbor_id_list[i];
     
     flddataarray[index] = (AFLOAT)P[j].Mass;
     index += 1;
   }   

 /*  code mass units to c.g.s. units (g) */
  indextmp = index - (nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * fldcontext->units->massunit_cgs;
      flddataarray[i] = valuefloat;
    }



  /* -------- Temperature (from entropy) */ 
                   /* -- units 1st since need 2 particle quantities */
 if (iamghost) 
   {    /* imported particle */ 
     valuefloat =  gadgetentropy_to_temperature(FLDDataGet[iparticle].Entropy, FLDDataGet[iparticle].Density, diaphane_context);	  
     flddataarray[index] = valuefloat;
     index += 1;
   }
 else  
   {  /* local particle */
     valuefloat =  gadgetentropy_to_temperature(SphP[iparticle].Entropy, SphP[iparticle].Density, diaphane_context);	  
     flddataarray[index] = valuefloat;
     index += 1;
   }
 for (i = 0; i < nneighbors; ++i)  /* then store neighbor particles */
   {
     j = neighbor_id_list[i];
     
     valuefloat =  gadgetentropy_to_temperature(SphP[j].Entropy, SphP[j].Density, diaphane_context);	  
     flddataarray[index] = valuefloat;
     index += 1;
   }   
 /* Temperature now in Kelvin */



 /* An untested example of how to import ones own kernel values */
#ifdef NOCALCULATEKERNEL  // must have bCalculateKernel = 0 in fld paramfile 
  /* -------- Kernel Gradient / r  -- if importing it from sim code  */ 
 /* gradW(nu=r/rsmooth)/r   dimensions are 1/length**4 */
                   /* -- units 1st since need 2 particle quantities */
 if (fldcontext->CalculateGradKernel == 0)  // copy kernel gradient from all particles in neighbor list 
   {
     if (iamghost) 
       {    /* imported particle */ 
	 flddataarray[index] =  (AFLOAT)FLDDataGet[iparticle].grad_kernel_over_r;
	 index += 1;
       }
     else  
       {  /* local particle */
	 flddataarray[index] =  (AFLOAT)P[iparticle].grad_kernel_over_r;
	 index += 1;
       }
     for (i = 0; i < nneighbors; ++i)  /* then store neighbor particles */
       {
	 j = neighbor_id_list[i];
	 flddataarray[index] = (AFLOAT)P[j].grad_kernel_over_r;
	 index += 1;
       }
 /* convert  code units to c.g.s. units (1/cm**4) */
     indextmp = index - (nneighbors+1);
     for (i = indextmp; i < index; ++i)
       {
	 valuefloat = flddataarray[i] / pow(fldcontext->lengthunit_cgs,4);
	 flddataarray[i] = valuefloat;
       }
   }
#endif




}






/* gadget loop of all local active particles and get dtFLDmax after FLD calc */
/* using dTemperature/dt_FLD / Temperature for each complete particle */
/*  input and output is in sim units  */
void getFLDmaxtimestep(FLDContextStruct *fldcontext)
{
  //  AFLOAT energy, dtFLDmax_cgs;
  int i;

  /* this should be ok -- i runs from 0 to N_gas on each process
                            i !=  Particle ID in snapshots  */
  for(i = 0; i < N_gas; i++) // (global) N_gas == gadget local N gas particles
    {
      if(is_gadget_particle_active(i)) // else, ignore, non-active particle
	{
	  SphP[i].dtFLDmax = (SIMFLOAT)  get_fld_timestep_from_dentropydt(SphP[i].EntropyDotFLD, SphP[i].Entropy, fldcontext->EtaFLD);

	}
      
    }
}

  
  /*   active on this time(sub)step   */
int is_gadget_particle_active(int iparticle)
{
  if(P[iparticle].Ti_endstep == All.Ti_Current) // active particle
    {
      return 1;
    }
  return 0;
}





