#include "allvars.h"
#include "fld_types.h"
#include "fldgen.h"



void DoFluxLimitedDiffusionStep(DiaphaneContext *diaphane_context);
void computeFLDstep(DiaphaneContext *diaphane_context);
void InitFLDParticle(int iparticle);
int neighbor_id_list_cube_to_sphere(SIMFLOAT posx0, SIMFLOAT poxy0, SIMFLOAT posz0, void* neighbor_id_list_voidpointer, int numngb, SIMFLOAT rmax_squared, int periodic, SIMFLOAT periodsimunits);
void calc_fld_rates_particle(DiaphaneContext *diaphane_context, int nneighbors_cube, int iparticle, int iamghost, SIMFLOAT *entropyDotFLD);
void convert_sim_data_to_cgs_arrays(int nneighbors, DiaphaneContext *diaphane_context, int iparticle, int iamghost);
// SIMFLOAT gadgetentropy_to_gadgetenergy(SIMFLOAT entropy, SIMFLOAT density, FLDContextStruct *fldcontext);
SIMFLOAT gadgetentropy_to_gadgetenergy_gadget_style_version(SIMFLOAT entropy, SIMFLOAT density); // uses gadget global variables 
//AFLOAT gadgetentropy_to_temperature(SIMFLOAT entropy, SIMFLOAT density, FLDContextStruct *fldcontext);
//AFLOAT cgsenergy_to_gadgetentropy(AFLOAT energy, AFLOAT density, FLDContextStruct *fldcontext);
//AFLOAT cgsenergydot_to_gadgetentropydot(AFLOAT energydot, AFLOAT density, FLDContextStruct *fldcontext);
void getFLDmaxtimestep(FLDContextStruct *fldcontext);
int is_gadget_particle_active(int iparticle);


// other files

// for fldgadgetwrapper_gradenergy.c    
void compute_grad_energy(void);
void compute_grad_energy_particle(int target, int mode);
int gradenergy_compare_key(const void *a, const void *b);


// for fldgadgetwrapper_flddriver.c
void compute_FLD_driver(DiaphaneContext *diaphane_context);
void compute_FLD_driver_particle(int target, int mode, DiaphaneContext *diaphane_context);
int fld_compare_key(const void *a, const void *b);
