/* fld_types.h */

/* Header for Flux-Limited Diffusion data variable types. */
/* 
*/

#ifndef FLD_TYPES_H   /* inclusion guard needed because gasoline headers are nested */
#define FLD_TYPES_H

#include <mpi.h>   // must be outside of "extern C" for some mpi versions

#include "mdl.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "opacity.h"
#include "diaphane_types.h"


//#ifndef MAX_FLD_CHAR_LENGTH
//#define MAX_FLD_CHAR_LENGTH 256
//#endif

//#ifndef MAX_FLD_PARAMS
//#define MAX_FLD_PARAMS 10  /* >= number variables in FLD param file */
//#endif



/* should have some way of switching float or double -- note gasoline already defined FLOAT */
/* WARNING -- because FLD uses cgs units, double precision is necessary to avoid floating point overflows!!!!   */
/* c should automatically convert floats to doubles in operations where they are mixed and double is needed */
//  typedef float AFLOAT;   // single precision  
typedef double AFLOAT;   // double precision                                                                                            

typedef int INTIDTYPE; // 32 bit integer IDs  // should not be actual IDs but i = 0 N on local process
// typedef long long INTIDTYPE; // 64 bit integer IDs (needed only if if Npart (on local process) > 2**31)

  typedef float SIMFLOAT;   // single precision for gadget main code particle data
//typedef double SIMFLOAT;   // double precision for gadget main code particle data





      /* Flux-Limited Diffusion context and data space */
          /* All FLD needed constants and variables go here -- this struct serves as a layer between some of the main code and c++ bits */
typedef struct FluxLimitedDiffusionContext
{
  double EtaFLD;
  double minEnergy, maxEnergy, minPressure, maxPressure;
  int CalculateGradKernel; /* Set to 0 to NOT calculate kernel gradient in lib and instead use values from sim code */
  struct DiaphaneSimContext *sim;  
  struct DiaphaneUnits *units; 
  OpacityContext *opacity_context;
  // the temporary data blocks will be allocated and freed each step
  void* flddatablock; /* this a pointer to the memory block reserved for the FLD calculation */
  /* optional */
  void* neighbor_id_list; /* optional -- pointer to the memory bloack reserved for FLD neighbor list if different from existing particle neighbor list */
} FLDContextStruct;


typedef AFLOAT (* grad_kernel_overnu_func_type)(AFLOAT);

#ifdef __cplusplus
 }
#endif

#endif /*  ifndef  FLD_TYPES_H */
