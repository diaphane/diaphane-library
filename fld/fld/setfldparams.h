#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <assert.h>


void AllocateFluxLimitedDiffusionContextMemory(FLDContextStruct **);

void FreeFluxLimitedDiffusionContextMemory(FLDContextStruct **);

void AllocateFluxLimitedDiffusionMemory(FLDContextStruct *fldcontext, int nmaxneighbors, int neednewneighborlist);

AFLOAT* AllocateFLDDataBlock(int nmaxneighbors, int nfields);

INTIDTYPE* AllocateNeighborIDList(int nmaxneighbors);

void FreeFluxLimitedDiffusionMemory(FLDContextStruct *);

void free_FLD_data_block(void **);

void free_neighbor_id_list(void **);

void read_fld_param_file(FLDContextStruct*, char*);

void update_fld_scale_factor(FLDContextStruct *, double *);

