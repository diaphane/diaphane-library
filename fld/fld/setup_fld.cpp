#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <assert.h>

#include <mpi.h>
#include "mdl.h"

#include "fld_types.h"
#include "setup_fld.h"
#include "diaphane_types.h"
#include "essentials.h"

#include "paramfile.h"

/* This file contains the FLD run parameter reader and routines to 
    allocate and free FLD memory, 
    and fill in FLD-specific context 
      */



/* init FLD library- once at start of run */
/* Init the context (persistent variables), including unit conversion factors */
void InitFluxLimitedDiffusion(DiaphaneContext* dpn_c) 
{
  FLDContextStruct* fldcontext;
 //  std::string dummystring; 
  // Alloc for the context
  printf("Initializing FLD\n");
  printf("will read param file %s for fld init\n",dpn_c->param_file_name);
fflush(stdout); 
  /* 1st add the code input parameters to the diffusion struct */

  /* allocate the context struct -- (1 copy for every process) */
  AllocateFluxLimitedDiffusionContextMemory(&fldcontext);
  dpn_c->fld = fldcontext;
  fldcontext->opacity_context = dpn_c->opacity_context;
  fldcontext->units = dpn_c->units;
  fldcontext->sim = dpn_c->sim; 
 
  // Read the parameter file 
  // Currently done on all processes...
  paramfile params(std::string(dpn_c->param_file_name), false);

  //   Should we have these fld-specific parameters into the opacity_context
  //     argument against is that  it assumes that all RT modules must use the same opacity model?
  fldcontext->EtaFLD = params.find<double>("dEtaFLD", 0.01);  
  fldcontext->CalculateGradKernel = params.find<int>("CalculateGradKernel",1);
//   fldcontext->HeatingRate = params.find<double>("dHeatingRate", 0.); //  uniform heating could be implemented

//    - opacitiy context creation is now in dpnInitUnits
//  printf("fld context   %g %g %g %g %g\n",fldcontext->EtaFLD, fldcontext->minEnergy, fldcontext->maxEnergy, fldcontext->minPressure, fldcontext->maxPressure); 
//  printf("fld context   %s %d\n",dpn_c->opacity_context->opacityTableFilename, dpn_c->opacity_context->PowerKappa); 

  printf("FLD context created\n");
  fflush(0);


}



/* at start of run */
void AllocateFluxLimitedDiffusionContextMemory(FLDContextStruct **fldcontext)
{
  *fldcontext = (FLDContextStruct *)malloc(sizeof(FLDContextStruct));
  if (*fldcontext == NULL)
    {
      printf("Failed to allocate context memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 

}

void FreeFluxLimitedDiffusionContextMemory(FLDContextStruct **fldcontext) /* at end of run */
{
  free(fldcontext);
}



/* Each timestep.  Each process. Allocate FLD temporary data block, 
and initialize FLD particle data field to: uDot=0, dtfld=huge    */
/* could have instead allocated memory only once at run start */
//void AllocateFluxLimitedDiffusionMemory(AFLOAT **flddatablock, INTIDTYPE **neighbor_id_list, int nmaxneighbors)
void AllocateFluxLimitedDiffusionMemory(FLDContextStruct *fldcontext, int nmaxneighbors, int neednewneighborlist)
{ 
  /*  

  FLD datablock array:    

    pos (xyzxyzxyz)     3*(nneighbors+1)  (input)
    gradenergy (xyzxyz) 3*(nneighbors+1)  (input)  -
    --the main code should compute either gradU or gradEntropy, depending on what SPH formalism is used
    density             nneighbors+1     (input)
    mass                nneighbors+1     (input)
    temperature         nneighbors+1     (input) (interchangble with u, x const)
    opacity             nneighbors+1     (intermediate output)
    grad_kernel_over_r  nneighbors+1     (intermediate output)
    fldk                nneighbors+1     (intermediate output) (could make part of particle struct)

    r_kernel (smoothing length) for this particle is passed as a separate variable --
    check we do not need r_kernel for the neighbors for FLD?

    Reminder: neighbor "0" is actually "this" particle, 
      There are actually nneighors-1 non-self neighbors, 
      neighbor 0 is part of the SPH sum. "this" particle must be stored twice -- once as neighbor 0 and again as another neighbor on the list and we do not know which one

 */


  AFLOAT *flddatablock = NULL; /* pointer to temp FLD memory block */
  INTIDTYPE *neighbor_id_list = NULL;

  int numberfields;

  numberfields = 12; /* WARNING hard-coded number for allocation, values per particle */


  /* some codes -- e.g. gadget require a neighbor ID list just for FLD */

  if (neednewneighborlist == 1)
    {
    /* could instead have a separate list of only neighbor ids */
      neighbor_id_list = AllocateNeighborIDList(nmaxneighbors);
/* gasoline already has a neighbor list struct, though it contains extra data besides the ids -- may as well use it */
    }
  else /* be sure to never try to access this pointer */
    {
      neighbor_id_list = NULL;
    }


  printf("will allocate FLD particle data block for his step\n"); fflush(stdout); ////////////////////////////////////////////////////
  flddatablock = AllocateFLDDataBlock(nmaxneighbors, numberfields);

    if (flddatablock == NULL)
    {
      printf("Failed to allocate memory\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 

  fldcontext->flddatablock = flddatablock; 
  fldcontext->neighbor_id_list = neighbor_id_list; 
//  printf("fld data block allocated \n");
}





/* 
   once per timestep
   allocates the small block of per particle data used by the FLD calculation */
AFLOAT* AllocateFLDDataBlock(int nmaxneighbors, int nfields)
{
  //  int ierror; 
  AFLOAT *flddatablock;
  //  ierror = 0;

  /* nneighbors+1 for 2nd copy of "this" particle in i=0 */
  int sizeofdataarray = nfields*(nmaxneighbors+1)*sizeof(*flddatablock);
  flddatablock = (AFLOAT *)malloc(sizeofdataarray); 
  if (flddatablock == NULL)
    {
      printf("Failed to allocate memory for FLD data block\n");
      //   ierror = 1;
      exit(1); /* should have error types */
    } 
  return flddatablock; /* local memory allocation */
}





/* done once per timestep to free FLD fields  */
//void FreeFluxLimitedDiffusionMemory(AFLOAT **flddatablock, INTIDTYPE **neighbor_id_list)                                                
void FreeFluxLimitedDiffusionMemory(FLDContextStruct *fldcontext)
{
  free_FLD_data_block(&fldcontext->flddatablock); 
  if (fldcontext->neighbor_id_list != NULL) // else we did not allocate this
    {
      free_neighbor_id_list(&fldcontext->neighbor_id_list); /* if use separate neighbor list for FLD */
    }
 }




/* 
   once per timestep -- free data block after FLD finished
*/
void free_FLD_data_block(void **flddatablock)
{
  free(*flddatablock);
}


/* free list of neighbor ids after each step */
void free_neighbor_id_list(void **neighbor_id_list)
{
  free(*neighbor_id_list);
}


