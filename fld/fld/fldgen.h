/* fld.h */

/* Header for Flux-Limited Diffusion data. */

#ifdef __cplusplus
extern "C" {
#endif



/* should have some way of switching float or double -- note gasoline already defined FLOAT*/
//  typedef float AFLOAT;   // single precision 
//  typedef double AFLOAT;   // double precision




  int* allocate_neighbor_id_list(int nmaxneighbors);

  void SetupOpacityTable(FLDContextStruct *fldcontext);

  void initFLDParticledata(AFLOAT *flduDot, AFLOAT *dtFLD);



  AFLOAT calcfldk(AFLOAT density, AFLOAT energy, AFLOAT gradenergyx, AFLOAT gradenergyy, AFLOAT gradenergyz, AFLOAT temperature, AFLOAT opacity, AFLOAT StefanBoltzmannConstantCGS);

  AFLOAT calcflduDot(int nneighbors, AFLOAT* density, AFLOAT* fldk, AFLOAT* rs1, AFLOAT* mass, AFLOAT* temperature, AFLOAT);

  AFLOAT get_fld_timestep_from_dudt(AFLOAT energyDotFLD, AFLOAT energy, AFLOAT etafld);

  AFLOAT get_fld_timestep_from_dentropydt(AFLOAT entropyDotFLD, AFLOAT entropy, AFLOAT etafld);



  void FLDgen(int nneighbors, AFLOAT rkernel, grad_kernel_overnu_func_type grad_kernel_overnu_func, FLDContextStruct *fldcontext, AFLOAT *uDot, AFLOAT *dtfldmax);

void combinethisparticlewithvirtualFLD(AFLOAT* flduDot1, AFLOAT* dtFLD1, AFLOAT flduDot2, AFLOAT dtFLD2);


  void free_FLD_data_block(void **flddatablock);

  void free_neighbor_id_list(void **neighbor_id_list);


#ifdef __cplusplus
}
#endif
