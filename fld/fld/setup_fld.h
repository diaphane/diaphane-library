
#include "diaphane_types.h"
#include "fld_types.h"

#ifdef __cplusplus
extern "C" {
#endif



void InitFluxLimitedDiffusion(DiaphaneContext* diaphane_context);
void AllocateFluxLimitedDiffusionContextMemory(FLDContextStruct **fldcontext);
void FreeFluxLimitedDiffusionContextMemory(FLDContextStruct **fldcontext); /* at end of run */
void AllocateFluxLimitedDiffusionMemory(FLDContextStruct *fldcontext, int nmaxneighbors, int neednewneighborlist);
AFLOAT* AllocateFLDDataBlock(int nmaxneighbors, int nfields);
void FreeFluxLimitedDiffusionMemory(FLDContextStruct *fldcontext);
void free_FLD_data_block(void **flddatablock);
void free_neighbor_id_list(void **neighbor_id_list);
void fld_init(DiaphaneContext* dpn_c);


#ifdef __cplusplus
}
#endif
