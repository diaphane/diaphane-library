/* fld.cpp */


/* This file contains the FLD physics routines  */


/* single versus double precision:
    in cgs units --- mass can easily exceed the ~10**38 floating point limit 
      and other variables distance**n, etc can also
      so make everything double?
*/

/* assumes all particles have same kernel -- Is that a problem for SPH with different smoothing lengths? */

/* 

Equations can be found in Whitehouse & Bate 2004, MNRAS, 353, 1078
or   Tuner & Stone 2001, ApJS, 135, 95
This method is described in Mayer et al. 2007, ApJ, 661, L77
FLd for sph method based on Cleay and Monaghan 1999, Journal Comp Phys, 148, 227

* Note that the assumption of Tgas = Tradiation for this FLD method implicitly assumes that the gas is optically thick.

*/

//This whole file only makes sense for simulations with gas

#include <cassert>
#include <cstdio>
#include <fstream>
#include <vector>
#include <iterator>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>

#include "Interpolate.h" /* c++ functions */
//#include "Vector3D.h"    /* c++ functions */

#include "fld_types.h" // structs, AFLOAT (single or double)
#include "fldgen.h"
#include "essentials.h"


/* generic initialize every active particle */
/* in e.g. gadget, the flduDot is actually fldEntropyDot */
void initFLDParticledata(AFLOAT *flduDot, AFLOAT *dtFLDmax) 
{
  *flduDot = 0.;
  *dtFLDmax = HUGE_VAL;
}


/* If main simulation code does not compute gradient of energy, we could do it here */
/*********
void calcgradenergypairgen(AFLOAT dx, AFLOAT dy, AFLOAT dz, AFLOAT rs1, AFLOAT aFac, AFLOAT dentropy, AFLOAT* gradenergyincx, AFLOAT* gradenergyincy, AFLOAT* gradenergyincz)
{
  AFLOAT fconst;
  fconst = -rs1 * aFac * dentropy; */ /* (q->u - p->u);   */ /*
  *gradenergyincx = fconst * dx;
  *gradenergyincy = fconst * dy;
  *gradenergyincz = fconst * dz;
}
******/




/***   the FLD k "conduction constant" calc 
       requires input of energy gradient 
*/
AFLOAT calcfldk(AFLOAT density, AFLOAT energy, AFLOAT gradenergyx, AFLOAT gradenergyy, AFLOAT gradenergyz, AFLOAT temperature, AFLOAT opacity, AFLOAT StefanBoltzmannConstantCGS)
{
  /* 
     input units: check this description is correct!
     opacity                     cgs (cm**2/g)
     temperature                 K   
     energy                      cgs
     gradenergy (xyz)            cgs (erg/cm)
     StefanBoltzmannConstant     cgs  (erg/s/cm**2/K**4)

     output: (cgs)
     "fld_k"  = fluxlimiter / opacity / density  (Whitehouse & Bates eqn 22)
	       

  */
  AFLOAT fld_k, R, fluxLimiter, maggradenergy;

  maggradenergy = sqrt(gradenergyx * gradenergyx + gradenergyy * gradenergyy + gradenergyz * gradenergyz);
  /* R: Turner & Stone eqn 18 without rho? (Whitehouse&Bate eqn 11) 
   there is a trick here to avoid computing Erad directly: 
    This is NOT exactly gradient(Erad) / Erad but is instead the 1st 
     order taylor expansion, which happens to  be 4x gradU / U
 i.e. erad=4sigmabT**4/c  ~ 4sigmabT**4/c + 16sigmabT**3/c dT
        grad erad ~ 16sigmabT**3/c dT --> grad erad/erad ~ 16T**3 dT / 4T**4
	~ 4  dT / T which happens to be = 4 du / u
	Since this entire FLD calc is 1st order, it should be ok.
  */
  R = 4. * maggradenergy / (energy * density * opacity); /* unitless */
  fluxLimiter = (2 + R) / (6 + 3 * R + R * R); /* Turner & Stone eqn 16 */
  /* following whitehouse & bates eqn 19-22: 
     k = fluxlimiter/opacity/density 
       Mayer etal 2007 eqn 2 (this is the 1st order taylor expansion of the 
       energy difference btw 2 particles dE/dT   where E = 4sigma_bT**4 (/c)
  */

  fld_k = 16 * StefanBoltzmannConstantCGS / density / opacity * fluxLimiter * temperature * temperature * temperature;
  return fld_k;
}



/* FLD Interaction */
  /* SPH gather
   *  This func could calc a symmetric uDot for neighbors and add it their 
   *  uDot, but for simplicity especially if neighbor is nonlocal 
   */
 /** *assumes* this is called only for active particles  **/
AFLOAT calcflduDot(int nneighborsplus1, AFLOAT* density, AFLOAT* fldk, AFLOAT* grad_kernel_over_r, AFLOAT* mass, AFLOAT *temperature) 
{ 
  /* input:  cgs  
     output: cgs  rate of energy change per unit mass due to Flux Limited Diffusion (flduDot) */
  /* temperature_to_u_simunit: conversion factor between temperature and energy? */
  /* maybe better to do indata as a struct or object */
  AFLOAT valuefloat;
  AFLOAT flduDot;
  AFLOAT density0, density1;
  AFLOAT fldk0, fldk1;
  AFLOAT mass1;
  AFLOAT grad_kernel_over_r_pair;
  AFLOAT temperature0, temperature1;
    /* x[0] is current particle, x[1-nn] are neighbors */ 
    /* (f(gradenergy, temp., density, pressure, opacity) */

  /* parse the input array  */
  density0=density[0]; // 0 is this particle, 1 is the neighbor
  fldk0=fldk[0];
  temperature0 = temperature[0];
  flduDot = 0.;
  
  /* Whitehouse & Bates eqn 21 -- FLD treated like thermal conduction */
  /* du0/dt = sum[m1/rho0rho1]*(4k0k1/(k0+k1) * (T0-T1) * (gradkernelr/r) */

  /* potential bug? -- need to remove own contribution for when self is neighbor (or make sure it is zero) -- i.e. deltaT is zero, but what if gradkern --> nan?? */
  for(int i = 1; i < nneighborsplus1; ++i) // "this" particle is here twice (i=0 and some other i). its contribution to uDot must be zero
    {
      grad_kernel_over_r_pair = grad_kernel_over_r[i];
      density1 = density[i];
      mass1 = mass[i];
      temperature1 = temperature[i];
      fldk1 = fldk[i];

       /* Mayer+ 2007 eqn1/Whitehouse & Bates Eqn 21 (without the T0-T1 term)  */
   /*  Note: It is assumed that Tgas = Trad 
	     --> meaning only valid for optically thick gas    
     and Note: Instead of computing delta Erad (~T**4) exactly,
     we take the 1st order Taylor expansion to get delta Erad 
        (while assuming Tgas = Trad)
	i.e. taylor expansion  f = f + df/dx * dx -->  
           Erad = 4sigmabT**4/c ~ 4sigmabT**4/c + 16sigmabT**3/c deltaT   
             --> gradient Erad ~ 16 sigmab T**3 / c ~ 16 sigmab T**3/c deltaT  
             --> grad Erad / Erad ~ 16sigmabT**3/c deltaT  / 4sigmabT**4/c ~ 4dt / T        
                 =  4deltaT     */
      valuefloat = 4.*mass1 / (density0 * density1) * (fldk0 * fldk1) / (fldk0 + fldk1) * grad_kernel_over_r_pair;  
      valuefloat *= (temperature0 - temperature1); // dTemp=0 for "this" part.
      flduDot += valuefloat;  /* cgs  */
    }

      /* return uDot in cgs */
  return flduDot; 
}


/* Diffusion timestep criteria: delta_energy < etafld * energy 
   For each particle:
       FLD timescale: dtfld == u/uDot 
       FLD max timestep: dtfld_max = dtfld * etafld 
                                   = u/uDotFLD * etafld
                                   = T/TDotFLD * etafld
                      dtFLDmax     = T/uDotFLD * T_to_u_conversion * etafld
       
previous version did something very different:
   found dtFLD for each pair assuming that T1-T2 = T1
   dtFLDmax_previous = dtfld_pair * etafld
   where   dtfld_pair = T * T_to_u_conversion / du/dt(DeltaT_pair=T)  
   --> This should greatly underestimate the maximum timestep length
*/
/* units cgs (seconds) in / out */
/* WARNING -- this will not work as intended if neighbors on multiple processor domains unless called after rates are summed on all neighbors */
/* returns what ever time units as input (can be cgs or code units) */
AFLOAT get_fld_timestep_from_dudt(AFLOAT energyDotFLD, AFLOAT energy, AFLOAT etafld)
{
  AFLOAT dtfld, dtfld_particlemax;
  /* FLD timescale:  tfld = u/(du/dtfld)  */
  // Think about a 'good' value for this limit...
  if(energyDotFLD < 1e-15 && energyDotFLD > -1e-15) energyDotFLD = 0;
  if (energyDotFLD != 0)   /* (infinite timestep if all neighbors are at same Temp.) */ 
	{
	  dtfld = energy / fabs(energyDotFLD);  
	}
  else
 	{
	  dtfld = HUGE_VAL;
	}
  dtfld_particlemax = etafld * dtfld;    
  return dtfld_particlemax;
}

/* This function is identical to  get_fld_timestep_from_dudt  */
/* WARNING -- this will not work as intended if neighbors on multiple processor domains unless called after rates are summed on all neighbors */
AFLOAT get_fld_timestep_from_dentropydt(AFLOAT entropyDotFLD, AFLOAT entropy, AFLOAT etafld)
{
  AFLOAT dtfld, dtfld_particlemax;
  /* FLD timescale */
  // Think about a 'good' value for this limit...
  if(entropyDotFLD < 1e-15 && entropyDotFLD > -1e-15) entropyDotFLD = 0;
  if (entropyDotFLD != 0)   /* (infinite timestep if all neighbors are at same Temp.) */
        {  
	  dtfld = entropy / fabs(entropyDotFLD);
	}
  else
	{
	  dtfld = HUGE_VAL;
	}
  dtfld_particlemax = etafld * dtfld;
  return dtfld_particlemax;
}

//  note.  we store not nneighbors buts nneighbors+1   -- "this" particle must be stored twice because we do not know where in the neighbor list it is 

/*
 *   
 *  This is the code independent layer of the FLD routine 
 *    to be called for every "active particle" 
 *
 * 
   global datablock array:    length
   pos (xyzxyzxyz)     3*(nneighbors+1)  (input)
   gradenergy (xyzxyz) 3*(nneighbors+1)  (input)
   density             nneighbors+1      (input)
   mass                nneighbors+1      (input)
   temperature         nneighbors+1      (input) (==energy x constant)
   opacity             nneighbors+1      (intermediate output)
   gradkernel_over_r   nneighbors+1      (intermediate output) 
   fldk                nneighbors+1      (intermediate output)

    r_kernel (smoothing length) for this particle is passed as a separate variable 

  Reminder: neighbor "0" is actually "this" particle, and there is another copy
      There are actually nneighors-1 non-self neighbors, 
      neighbor 0 is part of the SPH sum. "this" particle must be stored twice -- once as neighbor 0 and again as another neighbor on the list because we do not know which particle of the neighbor list is "this particle"


we could store internal energy as part of this data block, but instead
are converting from temperature (via a constant factor) 2? times.
   
*/
   /* assumes positions and gradients are 3dimensional quantities */

/*  
Units are:
cgs in, cgs out
 */

/* passing fldcontext struct -- otherwise quite a few values to pass */
/* output of this function is uDot for each particle 
                                       (or ghost particle if non-local)  */
void FLDgen(int nneighbors, AFLOAT rkernel, grad_kernel_overnu_func_type grad_kernel_overnu_func, FLDContextStruct *fldcontext, AFLOAT *uDot, AFLOAT *dtfldmax) 
{
  /* length = nneigbors + 1*/
  AFLOAT *pos, *pos_neighbor; /* xyz */
  AFLOAT* gradenergy; /* xyz */
  AFLOAT *density;
  AFLOAT *mass;
  AFLOAT *temperature;
  AFLOAT *opacity;
  AFLOAT *grad_kernel_over_r;
  AFLOAT *fldk; /* for this particle and neighbor */
  AFLOAT energy;
  AFLOAT valuefloat;
  int index, index3d; 
  AFLOAT *datablock;
  int nneighborsplus1;
  int i;
  //  AFLOAT (*kernelfunc)(AFLOAT);
  
  datablock = (AFLOAT *) fldcontext->flddatablock;
  nneighborsplus1 = nneighbors+1;

  /* make sure these pointers are parsed correctly to the input array!!! */
  index = 0;

  /* these first fields of the datablock values are passed from the wrapper  */
  pos = &datablock[index];
  index += 3 * nneighborsplus1;

  gradenergy =  &datablock[index];
  index += 3 * nneighborsplus1;

  density = &datablock[index];
  index += nneighborsplus1;

  mass = &datablock[index];  /* mass[0] is here but only need mass[neighbors] for gather-only */
  index += nneighborsplus1;

  temperature = &datablock[index];
  index += nneighborsplus1;


  /* just setting pointers -- below parts of datablock array 
   * contains NO values yet -- it is just a temp memory block  */

  opacity = &datablock[index];
  index += nneighborsplus1;

  grad_kernel_over_r = &datablock[index]; /* may or may not already be filled */
  index += nneighborsplus1;
  
  fldk = &datablock[index]; /* this is already calculated ? */

  

  /* fill the intermediate data arrays */
  /* need each quantity for the particle and for its neighbor */
  /* particle 0 is "this" particle */
  for(i = 0; i < nneighborsplus1; ++i) 
    { 
      valuefloat = calc_opacity(fldcontext->opacity_context, density[i], temperature[i]);
      opacity[i] = valuefloat;     
    }
  

  /* get [gradient of kernel] / r   (=="rs1" in gasoline) for each pair */ 

  if (fldcontext->CalculateGradKernel != 0) /* else: already have kernel values */
    {
      for(i = 0; i < nneighborsplus1; ++i)  /* i=0 is its own neighbor */
	{
	  pos_neighbor = &pos[3*i]; /* start with xcoord of neighbor: ndims=3, pos[0] = this particle */
	  valuefloat = calc_grad_kernel_over_r(grad_kernel_overnu_func, fldcontext->sim->periodic, fldcontext->sim->periodx, fldcontext->sim->periody, fldcontext->sim->periodz, pos, pos_neighbor, rkernel); 
	  grad_kernel_over_r[i] = valuefloat;
	}
    }

  /* get fld_k  --- FLD "conduction" constant */ 
  /*   We compute fldk for each neighbor, for every particle.  
      To save flops, fldk could be calculated and stored for every particle 
      before the FLD routine is called, and after gradient energy is computed.
*/

  for(i = 0; i < nneighborsplus1; ++i)  
    {
      index3d = i*3;	    
      energy= temperature[i] * fldcontext->units->temperature_to_internalenergy_cgs;
      valuefloat =  calcfldk(density[i], energy, gradenergy[index3d], gradenergy[index3d+1], gradenergy[index3d+2], temperature[i], opacity[i], fldcontext->units->StefanBoltzmannConstantCGS);
      fldk[i] = valuefloat;
    }

  /* neighbor loop is inside calcflduDot  */
  valuefloat = calcflduDot(nneighborsplus1, density, fldk, grad_kernel_over_r, mass, temperature);
  *uDot = valuefloat; 


  //   we should not try to get dtfldmax until all rates are summed OR
  //      we should take the min of dtfldmax over all pairs 
  //  *dtfldmax = get_fld_timestep(*uDot, temperature[0], fldcontext->temperature_to_internalenergy_cgs, fldcontext->EtaFLD);
 
  
}  



//*** this assumes  dtFLD is just the min of its dtFLD for each neigbhor not the (more sensible?) summed quantity its off-process virtual counterpart  **/
void combinethisparticlewithvirtualFLD(AFLOAT* flduDot1, AFLOAT* dtFLD1, AFLOAT flduDot2, AFLOAT dtFLD2)
{
  /* note "1" is a pointer because need to modify its values, "2" is not */
  *flduDot1 += flduDot2;
  /* optional: FLD timestep criterion can be shortest timestep of each pair */  
  if(dtFLD2 < *dtFLD1) 
    {
      *dtFLD1 = dtFLD2;
    }
}



