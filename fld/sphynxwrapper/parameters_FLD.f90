
!  ? It is not necessary to "bind" the struct to its c counterparts???
!      Does the binding force the compilers to check that the morphology 
!        of the structs are identical in fortran and c??


!!!  TODO:  careful MAXPATHLEN should be either all-hard coded or only system value. e.g. for c:  #include <sys/param.h> 
#ifndef MAXPATHLEN     
#define MAXPATHLEN 256  // TODO: fix. it is MATHPATHLEN below.  
#endif


module parameters_FLD

  use,intrinsic:: ISO_FORTRAN_ENV

  IMPLICIT NONE


  INTEGER,parameter:: MATHPATHLEN=256   !!!!!!! TODO: make sure this path length is same as in the rest of the library




  
  

  TYPE somestruct

     SEQUENCE
     real (real64) :: EtaFLD
     real (real64) :: minEnergy, maxEnergy, minPressure, maxPressure
     integer(int32) :: PowerKappa   ! /* option to use power law opacity instead of table Kappa = dKappaCont**dKappaIndex */
     real(real64) :: KappaConst
     real(real64) :: KappaIndex
     real(real64) :: HeatingRate
     real(real64) :: ConstGamma
     real(real64) :: MeanMolWeight
     integer(int32) :: comoving
     integer(int32) :: periodic
     real(real64) :: scale_factor   !/* not a constant for comoving runs, so many units also not a constant */
     real(real64) :: periodx_z0     !/* the _z0 quantities are rescale for comoving runs */
     real(real64) :: periody_z0  
     real(real64) :: periodz_z0
     real(real64) :: lengthunit_cgs_z0  !/* the _z0 units are rescale for comoving runs */
     real(real64) :: timeunit_cgs_z0
     real(real64) :: velocityunit_cgs_z0
     real(real64) :: periodx   !/* rescaled for comoving (from the _z0 z=0 version)  */
     real(real64) :: periody
     real(real64) :: periodz
     real(real64) :: dMassUnit
     real(real64) :: lengthunit_cgs   !/* rescaled for comoving (from the _z0 z=0 version) */
     real(real64) :: massunit_cgs
     real(real64) :: timeunit_cgs
     real(real64) :: velocityunit_cgs
     real(real64) :: StefanBoltzmannConstantCGS
     real(real64) :: temperature_to_internalenergy_simunit
     real(real64) :: temperature_to_internalenergy_cgs
     real(real64) :: densityunit_cgs
     real(real64) :: gradenergyunit_cgs
     real(real64) :: pressureunit_cgs
     real(real64) :: opacityunit_cgs
     real(real64) :: heatingunit_cgs
     integer(int32) :: CalculateGradKernel
     character(MATHPATHLEN) :: opacityTableFilename
     real(real64) :: opacityTableMinTemperature  !/* log_10(Kelvin) */
     real(real64) :: opacityTableMaxTemperature   !/* log_10(Kelvin) */
     real(real64) :: opacityTableMinPressure   !/* log_10(cgs pressure) */
     real(real64) :: opacityTableMaxPressure   !/* log_10(cgs pressure) */
     integer(int64) :: IntegrationContext
     integer(int64) :: opacity_interpolator  !/* this is the bilinear interpolar c++ function */
     !  // the temporary data blocks will be allocated and freed each step
     !  //  void* flddatablock; /* this a pointer to the memory block reserved for the FLD calculation */
     integer(int64) :: flddatablock   ! a forced pointer
     !  /* optional */
     !  integer(int64) :: neighbor_id_list_address ; /* pointer to the memory bloack reserved for FLD neighbor list if different from existing particle neighbo!r list */
     !//} FLDContextStruct;
  END TYPE somestruct




end module parameters_FLD


