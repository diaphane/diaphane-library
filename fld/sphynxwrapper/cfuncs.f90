

module cfuncs

    ! essential to bind any capitalized c/c++ function names to a lower case fortran name
  interface  
 !    subroutine setup_opacity_table_c(fldcontext, opacityTableFilename) bind(C, name='SetupOpacityTable')
     subroutine setup_opacity_table_c(fldcontext) bind(C, name='SetupOpacityTable') 
       USE parameters_FLD
       use, intrinsic :: iso_c_binding  !! , only: c_char, c_size_t   
       implicit none       
       type (somestruct) fldcontext
!       character opacityTableFilename*MAXPATHLEN
       !      character(kind=c_char) :: str(*)         
       !      integer(c_size_t),value :: len                                         
     end subroutine setup_opacity_table_c
     
     !    more c++ subroutine bindings can go here     
     
  end interface
  
  
end module cfuncs
