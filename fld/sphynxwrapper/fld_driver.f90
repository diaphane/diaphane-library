MODULE FLD_driver
use iso_c_binding
contains

  SUBROUTINE FLD_init
    USE, INTRINSIC :: ISO_C_BINDING
    USE parameters,only:gamma,xbox,ybox,zbox
    USE parameters_FLD
    use cfuncs 

    IMPLICIT NONE

    character fld_param_filename*MAXPATHLEN
    type (somestruct) fldcontext

    fld_param_filename = "./fld.param"!//CHAR(0)
    print *,"allocate fld context and read fld param file", fld_param_filename
    call allocate_fldcontextstruct(fldcontext)
    call read_fld_param_file(fldcontext, fld_param_filename)

    print *,fldcontext%opacityTableFilename
    print *,sizeof(fldcontext%EtaFLD)
    print *,sizeof(fldcontext%PowerKappa)
    print *,HUGE(fldcontext%EtaFLD)
    print *,HUGE(fldcontext%PowerKappa)

    fldcontext%ConstGamma=gamma
    fldcontext%MeanMolWeight=4.d0/(1.d0+3.d0*.76d0)
    fldcontext%scale_factor=1.d0
    fldcontext%periodic=1
    fldcontext%velocityunit_cgs_z0=1.d0
    fldcontext%lengthunit_cgs=1.d0
    fldcontext%massunit_cgs=1.d0
    fldcontext%timeunit_cgs=1.d0

    print *,fldcontext%lengthunit_cgs,fldcontext%massunit_cgs,fldcontext%timeunit_cgs
    print *,fldcontext%EtaFLD,fldcontext%KappaConst
    print *,'************************'

    call get_units_for_fld_calc(fldcontext)

    fldcontext%comoving=0
    fldcontext%periodx_z0=xbox
    fldcontext%periody_z0=ybox
    fldcontext%periodz_z0=zbox
!! TODO: fix this function call 
    if(fldcontext%PowerKappa.eq.0) then
!       call SetupOpacityTable(fldcontext,fldcontext%opacityTableFilename)
!       call setup_opacity_table_c(fldcontext,fldcontext%opacityTableFilename)
       call setup_opacity_table_c(fldcontext) 
    else
       fldcontext%opacityTableMaxTemperature=HUGE(1.d0)
    endif

  END SUBROUTINE FLD_init

END MODULE FLD_driver



!!$SUBROUTINE FLD_driver
!!$
!!$  USE parameters_FLD
!!$  
!!$  IMPLICIT NONE
!!$
!!$  INTEGER::iq,i
!!$  DOUBLE PRECISION 









!!$  if(flags)write(*,*) 'Leakage contribution'
!!$
!!$  dt_idsa=dt
!!$  s0=s
!!$ 
!!$!=========================Leakage contribution========================
!!$  call leakage(l,n,dt_idsa,tt,promro,u,temp,ye,yn,yp,yh,ya,ynu,znu,&
!!$       &etae,etanu,Amass,Zmass,nuedot,yedot,ynudot,znudot)
!!$ 
!!$  ye(:)=ye(:)+yedot(:)*dt_idsa
!!$  ynu(:,:)=ynu(:,:)+ynudot(:,:)*dt_idsa
!!$  u(:)=u(:)+nuedot(:)*dt_idsa
!!$
!!$!  if(l.eq.iterini) then
!!$!     dtold=dt
!!$!     nuedotold=nuedot
!!$!  endif
!!$!  val1=dt*dt*.5d0/dtold
!!$!  val2=dt+val1  
!!$!  u(:)=u(:)+nuedot(:)*val2-nuedotold(:)*val1
!!$!  nuedotold(:)=nuedot(:)  
!!$
!!$  !call EOS with the new U.
!!$  call eostot
!!$
!!$  if(.not.znueq) then
!!$     znu(:,:)=znu(:,:)+znudot(:,:)*dt_idsa
!!$     dmy=units%MeV/units%mb
!!$     nuenergy(:)=dmy*(znu(len,:)+znu(lea,:)+4.d0*znu(lmn,:))
!!$     if(nupress) then 
!!$        nupresstot(:)=nuenergy(:)*promro(:)/3.d0/ro2(:)
!!$     else
!!$        nupresstot(:)=0.d0
!!$     endif
!!$  endif
!!$
!!$  RETURN
!!$END SUBROUTINE leakage_driver
