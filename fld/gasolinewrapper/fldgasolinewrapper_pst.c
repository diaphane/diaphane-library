/* fldgasolinewrapper.c */

/* code dependent layer */
/* Flux-Limited Diffusion  */

//This whole file only makes sense for simulations with gas
//#ifdef GASOLINE

#include <stdio.h>///////////////////////////////////////

#include "pst.h"
#include "fldgasolinewrapper_pst.h"
#include "fldgasolinewrapper.h"




/* allocate temporary memory each step -- could do at start of run */
/* pstInitDiffusionStep walks the domain tree via the mdl layer to call pkdInitDiffusion by each process */
void pstInitFluxLimitedDiffusionStep(PST pst, void* vin, int nIn, void* vout, int* pnOut) 
{
  /* vin is just this to know how many neighbors worth of data to allocate:
     typedef struct inInitFluxLimitedDiffusionStep {
     int nmaxneighbors;
     } inInitFluxLimitedDiffusionStepStruct;
  */

  /* pointer type needs to be defined (from void in)  */
  inInitFluxLimitedDiffusionStepStruct *in = vin; /* c version */
  //	inInitDiffusion* in = reinterpret_cast<inInitDiffusion *>(vin); /* c++ version */
  if(pst->nLeaves > 1) 
    {
      mdlReqService(pst->mdl, pst->idUpper, PST_INITFLUXLIMITEDDIFFUSIONSTEP, vin, nIn);
      pstInitFluxLimitedDiffusionStep(pst->pstLower, vin, nIn, NULL, NULL);
      mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
    } 
  else
    {
      InitFluxLimitedDiffusionStep(pst->plcl->pkd, in->nmaxneighbors, in->scale_factor);
    }
  if(pnOut)
    {
      *pnOut = 0;
    }

}





void pstFinishFluxLimitedDiffusionStep(PST pst, void* vin, int nIn, void* vout, int* pnOut) 
{   
  /* in struct not used here */
  //  inInitDiffusion *in = vin; /* c version */  
  //	inInitDiffusion* in = reinterpret_cast<inInitDiffusion *>(vin); /* c++ version */
  if(pst->nLeaves > 1) 
    {
      mdlReqService(pst->mdl, pst->idUpper, PST_FINISHFLUXLIMITEDDIFFUSIONSTEP, vin, nIn);
      pstFinishFluxLimitedDiffusionStep(pst->pstLower, vin, nIn, NULL, NULL);
      mdlGetReply(pst->mdl, pst->idUpper, NULL, NULL);
    } 
  else
    {
      FinishFluxLimitedDiffusionStep(pst->plcl->pkd);
    }
  if(pnOut)
    {
      *pnOut = 0;
    }
}



//#endif //GASOLINE
