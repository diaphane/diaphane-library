/* fldgasolinewrapper.h */

/* Header for Flux-Limited Diffusion data. */
/* 
*/

/* can we clean up and get the header includes out of the header? */
#include "fld_types.h"
#include "fldgasolinewrapper_types.h"
//#include "fldgasolinewrapper_pst.h"
#include "fldgen.h"
#include "pkd.h"   /* typedef struct  PKD  is needed for the wrapper function definitions */
#include "smoothfcn.h"


void InitFluxLimitedDiffusionStep(PKD pkd, int nmaxneighbors, double scale_factor);
void InitFLDParticle(void *vp);
void FinishFLDParticle(void* vp1, void* vp2);
void FinishFluxLimitedDiffusionStep(PKD pkd);
AFLOAT rkernelsquared_given_fball2(AFLOAT fBall2);
AFLOAT rkernel_given_fball2(AFLOAT fBall2);
void calc_fld_rates_particle(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf);
void convert_sim_data_to_cgs_arrays(PARTICLE *ppart, int nneighbors, NN* nnList, FLDContextStruct *fldcontext);
void getFLDmaxtimestep_particle(PARTICLE *ppart, SMF *smf);
int is_gasoline_particle_active(PARTICLE *ppart);


















