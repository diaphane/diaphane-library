/* fldgasolinewrapper_pst.h */

/* Header for Flux-Limited Diffusion data. */
/* 
*/

#ifndef FLDGASOLINEWRAPPER_PST_H   /* inclusion guard needed because gasoline headers are nested */
#define FLDGASOLINEWRAPPER_PST_H



void pstInitFluxLimitedDiffusionStep(PST pst, void* vin, int nIn, void* vout, int* pnOut);
void pstFinishFluxLimitedDiffusionStep(PST pst, void* vin, int nIn, void* vout, int* pnOut);

#endif
