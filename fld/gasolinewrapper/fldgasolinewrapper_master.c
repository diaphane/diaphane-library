/* fldgasolinewrapper.c */

/* code dependent layer */
/* Flux-Limited Diffusion  */

//This whole file only makes sense for simulations with gas
//#ifdef GASOLINE

#include <string.h>
#include <malloc.h>
#include <stdio.h>

#include "master.h"
#include "fld_types.h"
#include "fldgasolinewrapper_types.h"
#include "fldgasolinewrapper_pst.h"
#include "fldgasolinewrapper_master.h"



/* (master)  "msr" functions have a specific format 
--They call to a pst function, which is recursively defined -- this walks down the domain tree to each process
--pst function call has 5 args 
1-(msr->psr
2-input struct (whatever real args get passed to function), 
3-(int)sizeof(input struct), 
4-void* == an output?  -- always NULL for FLD
5-int* ==              -- alwys  NULL for FLD

*/




/* Note: gradU[3] (needed for FLD) and smoothU (optional for FLD)                                                                         
 *        are already calculated for gasoline for each particle  
 */
/* in gasoline: maximum possible nneighbors == nSmooth + 1  = (me + all neighbors) */
void msrInitFluxLimitedDiffusionStep(MSR msr, int nneighbors, double scale_factor) 
{
  inInitFluxLimitedDiffusionStepStruct *in;
  in = (inInitFluxLimitedDiffusionStepStruct *)malloc(sizeof(inInitFluxLimitedDiffusionStepStruct));
  in->nmaxneighbors = nneighbors;
  in->scale_factor = scale_factor;
  pstInitFluxLimitedDiffusionStep(msr->pst, in, sizeof(*in), NULL, NULL);
  free(in);
}



void msrDoFluxLimitedDiffusionStep(MSR msr, double dTime, double scale_factor)
{
   /* Flux Lmited Diffusion */
  // initFLDstepfloat(): ??
  int nneighbors;
  printf("begin FLD: ReSmooth to get GradU\n");
  msrReSmooth(msr, dTime, SMX_SPHGRADU, 1); /* 1st get energy gradient */

  /* allocate memory */
  nneighbors = msr->param.nSmooth*10.;   // careful -- this is nmax neighbors 10xnSmooth as coded in smooth.c 
  printf("Init the FLD memory\n");

  msrInitFluxLimitedDiffusionStep(msr, nneighbors, scale_factor);
  printf("ReSmooth for FLD\n");

  msrReSmooth(msr, dTime, SMX_FLUXLIMITEDDIFFUSION, 0); // the 0 means not symmetric, but gather-only

  printf("ReSmooth done for FLD\n");
  msrFinishFluxLimitedDiffusionStep(msr);   // can we update the timestep here instead?
  printf("FLD finished\n");
}




void msrFinishFluxLimitedDiffusionStep(MSR msr)
{
  pstFinishFluxLimitedDiffusionStep(msr->pst, NULL, 0, NULL, NULL);
}


//#endif //GASOLINE
