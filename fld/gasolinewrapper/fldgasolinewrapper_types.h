/* fld.h */

/* Header for Flux-Limited Diffusion data. */
/* 
*/

/* everything here is double -- do we want to change to optional single/double (AFLOAT)? */

#ifndef FLDGASOLINEWRAPPER_TYPES_H   /* inclusion guard needed because gasoline headers are nested */
#define FLDGASOLINEWRAPPER_TYPES_H


/* PST_INITDIFFUSION */
typedef struct inInitFluxLimitedDiffusion 
{
  double dConstGamma;
  double dMeanMolWeight;
  int bPeriodic;
  int bComove;
  double dKpcUnit;
  double dMassUnit;
} inInitFluxLimitedDiffusionStruct;


/* PST_INITDIFFUSION each step */
typedef struct inInitFluxLimitedDiffusionStep {
  int nmaxneighbors;
  double scale_factor;
} inInitFluxLimitedDiffusionStepStruct;




#endif /*  ifndef  FLDGASOLINEWRAPPER_TYPES_H */
