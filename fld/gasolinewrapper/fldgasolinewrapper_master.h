/* fld.h */

/* Header for Flux-Limited Diffusion data. */
/* 
*/


void msrInitFluxLimitedDiffusion(MSR msr, double dTime, double scale_factor);
void msrInitFluxLimitedDiffusionStep(MSR msr, int nneighbors, double scale_factor);
void msrDoFluxLimitedDiffusionStep(MSR msr, double dTime, double scale_factor);
void msrFinishFluxLimitedDiffusionStep(MSR msr);




