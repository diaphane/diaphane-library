///* fldgasolinewrapper.c */

/* interface between FLD library and simulation code layer */
/* Flux-Limited Diffusion  */

#include <math.h>
#include <assert.h> 
#include <malloc.h>


//This whole file only makes sense for simulations with gas
//#ifdef GASOLINE

#include "fld_types.h"
#include "fldgen.h"
#include "setunits.h"
///#include "setfldparams.h"
#include "setup_fld.h"
#include "essentials.h"
#include "fldgasolinewrapper.h"
#include "smooth.h"



/* The FLD calculation, called each step -- called by each process */
void InitFluxLimitedDiffusionStep(PKD pkd, int nmaxneighbors, double scale_factor)
{
  FLDContextStruct *fldcontext;
  int neednewneighborlist;

  fldcontext = pkd->dpn_context->fld; /* fldcontext will be part of struct smf->pkd */

  neednewneighborlist = 0; /* set to 1 only if need to modify the codes internal neighbor list before calling    convert_sim_data_to_cgs_arrays.  i.e. 1 for gadget, 0 for gasoline */

  AllocateFluxLimitedDiffusionMemory(fldcontext, nmaxneighbors, neednewneighborlist); /* allocates FLD memory data block and for gadget a list of neighbors in a sphere */

 
}



/* done once per active particle, every step */
/* to initialize FLD fields, sets uDot=0, dtfld=huge  */
/* call before any FLD calc's are done each step  */
/* defined by smooth.c  case SMX_FLUXLIMITEDDIFFUSION, comb=initFLDparticle */
void InitFLDParticle(void *vp)
{
  AFLOAT flduDot;
  AFLOAT dtFLD;
  //  PARTICLE* p = static_cast<PARTICLE *>(vp); /* c++ version */
  PARTICLE *ppart = vp; /* c version */
  if(is_gasoline_particle_active(ppart)) 
    {
      initFLDParticledata(&flduDot, &dtFLD); 
      ppart->flduDot = (SIMFLOAT)flduDot; // typecast in case main code is using a different precision than this lib
      ppart->dtFLDmax = (SIMFLOAT)dtFLD;
    }
}




/* FinishFLDParticle:  not needed for Gather-only FLD */

/* for each particle (or element), after all fld uDots are calculated, 
 *  and summed with its off-process virtual (ghost) counterpart(s) */
/* called by smooth.c  case SMX_FLUXLIMITEDDIFFUSION, comb=finishFLDparticle */
/* In gasoline, each particle gets its list of neighbors on other domains
        imported -- i.e. "this" particle always remains on "this" domain.
        This function is then not needed for gather-only calculations, which
        is what this FLD is at this time.  If we make a symmetric FLD library, 
	a "Finish" function is needed to combine "this" particle with the ghost
        of "this" particle that was transported to a neighboring domain.
 */
void FinishFLDParticle(void* vp1, void* vp2) 
{
  AFLOAT flduDot1, flduDot2;
  AFLOAT dtFLDmax1, dtFLDmax2;
  PARTICLE* ppart1 = vp1;   
  PARTICLE* ppart2 = vp2;  /* c version */
  
/*   short version of this function 
  if(is_gasoline_particle_active(ppart1)) 
    {
      ppart1->flduDot += (SIMFLOAT) ppart2->flduDot;
      if(ppart2->dtFLDmax < ppart1->dtFLDmax)
 	{
	  ppart1->dtFLDmax = ppart2->dtFLDmax;
	}  
    }
*/

  flduDot1 = (AFLOAT)ppart1->flduDot;
  flduDot2 = (AFLOAT)ppart2->flduDot;
  dtFLDmax1 = (AFLOAT)ppart1->dtFLDmax;
  dtFLDmax2 = (AFLOAT)ppart2->dtFLDmax;
 /* p1 uDot +=p2 uDot, and dtfldmax1 = min(dtfldmax1, dtfldmax2) */
  if(is_gasoline_particle_active(ppart1)) 
    {
      combinethisparticlewithvirtualFLD(&flduDot1, &dtFLDmax1, flduDot2, dtFLDmax2);  
    }
  /* p1 now has the correct combined values */
  ppart1->flduDot = (SIMFLOAT)flduDot1;
  ppart1->dtFLDmax = (SIMFLOAT)dtFLDmax1;
 }



/* now to call this by each process ?????????????????????  */
/* done once per timestep to free FLD fields  */
void FinishFluxLimitedDiffusionStep(PKD pkd)
{
  FreeFluxLimitedDiffusionMemory(pkd->dpn_context->fld); 
}


/* gasoline kernel length (half the distance to kth most distant neighbor) */
AFLOAT rkernelsquared_given_fball2(AFLOAT fBall2)
{
  return fBall2/4.; /* rkernel**2 */
}

/* gasoline (spline) kernel is half the distance to nsmooth^th neighbor  */
AFLOAT rkernel_given_fball2(AFLOAT fBall2)
{
  return sqrt(fBall2/4.); /* gasoline */
}



/* THIS FUNCTION NOT USED  */
/* the neighbor list of gasoline has a bunch of variables -- get only IDs */
/******* 
void get_neighbor_id_list(PARTICLE* ppart, NN* nnList, int* nnIDList, int numngb)
{
  int j;
  int ineighbor;

  ineighbor = 0;
  for(j = 0; j < numngb; j++)
    {
      nnIDList[ineighbor] = nnList[j].iPid;
      ineighbor++;
    }
}
******/

/* the function is called for every particle */
/***  because this goes through smooth framework, this is the only possible 
 * form for the function call.  All context is contained in SMF ***/
/* called from smooth.c */
/* all gasoline neighbor/smoothing calcs use this function format  */
/* SMF is the context data */
/* occasionally, there are < 32 neighbors, why? */
void calc_fld_rates_particle(PARTICLE* ppart, int nneighbors, NN* nnList, SMF* smf)
{
  //  double scale_factor;  /* This is just the scale factor 1/(1+z)? */
  AFLOAT dtfldmax;
  AFLOAT flduDot;
  AFLOAT rkernel, rkernel_cgs; /* smoothing length only need for *this* particle */
  AFLOAT *gradkernel(AFLOAT);
  FLDContextStruct *fldcontext;

  fldcontext = smf->pkd->dpn_context->fld;
  //  scale_factor = smf->a;
  //  bcomove = smf->pkd->bComove;

  /* for portability -- could make a list of neighbor IDs only -- the gasoline NNList many fields for each neighbor, but then this requires alllocating a new array and we do not need IDs once neighbor info is stored */
  //  get_neighbor_id_list(PARTICLE* ppart, NN* nnList, int* nnIDList, int numngb);
  

  if(is_gasoline_particle_active(ppart))  // we will gather only -- ignore this particle if it is inactive (but we will still use neighbors that are inactive)
      {
      convert_sim_data_to_cgs_arrays(ppart, nneighbors, nnList, fldcontext);

  /* make this definable from a param file ! and put in fldcontext  */
  /* kernel gradient/(r/rsmooth) without the prefactors  */
      grad_kernel_overnu_func_type grad_kernel_overnu_func = &bspline_grad_kernel_over_nu_nonorm;
  
      rkernel = rkernel_given_fball2((AFLOAT)ppart->fBall2); /* gasoline --> rkernel */
      rkernel_cgs = rkernel * fldcontext->units->lengthunit_cgs ;
  /* clean up this call and put grad_kernel into fldcontext ........ */
      FLDgen(nneighbors, rkernel_cgs, grad_kernel_overnu_func, fldcontext, &flduDot, &dtfldmax);

      /* convert flduDot cgs to uDot sim units */
      flduDot /= fldcontext->units->heatingunit_cgs ;  /* denergy/dt to sim units  */
      dtfldmax /= fldcontext->units->timeunit_cgs; 

      /* uDot updates u in pkdKick (uDot in pkdUpdateuDot from msrTopStepKDK) */
      /* cannot update uDot here because 1st call just sets timestep? */
      ppart->flduDot = (SIMFLOAT)flduDot; /* u and uDot is updated in pkd.c */
      ///////////////////////////////     check units! ---- at the moment the fld timestep seems to have no effect (all particles stay on main rung)
      ppart->dtFLDmax = (SIMFLOAT)dtfldmax; /* particle timestep (ppart->dt) is updated later */ 
      }
}





/* input: particle data sim units  -->   output: data as cgs arrays */
/* ppart is "this particle"     qpart is "neighbor particle"*/
void convert_sim_data_to_cgs_arrays(PARTICLE *ppart, int nneighbors, NN* nnList, FLDContextStruct *fldcontext)
{
  PARTICLE* qpart = 0; 
  //  int nmaxneighbors;
  AFLOAT *flddataarray;
  int i, index, indextmp;
  AFLOAT valuefloat;
  AFLOAT *gradkernel(AFLOAT);

  DiaphaneUnits* units;
  units = fldcontext->units;

  /*  nneighbors ~= nSmooth  n_neighbors is not constant here */

  /* pass all neighbor data, even for inactive particles, which we do not need because will gather only */


  flddataarray = fldcontext->flddatablock;


  /* -------- pos */
  index = 0;

  /* 1st me, xyz, then my neighbors */
  /* this can be optimied, since neighbor 1 is me */
  flddataarray[index] = (AFLOAT)ppart->r[0];
  index += 1;
  flddataarray[index] = (AFLOAT)ppart->r[1]; 
  index += 1;
  flddataarray[index] = (AFLOAT)ppart->r[2]; 
  index += 1;

  /* Note: nnList[0] is this particle (ppart) and is counted as a "neighbor"
           nneighbors = actual number of neighbors + 1 
                (e.g. usually 31 if nSmooth=32, but sometimes nneighbors < nSmooth+1?)
*/

  /* we could use the relative neigbor positions (dx,dy,dz) directly from nnList  -- will instead recompute them later */  
  for (i = 0; i < nneighbors; ++i) /* particle 0 on neighbor list is this particle -- nnLlist nneighbors can be up to nSmooth + 1 particles */
    {
      qpart=nnList[i].pPart; 
      flddataarray[index] = (AFLOAT)qpart->r[0];

      index += 1;
      flddataarray[index] = (AFLOAT)qpart->r[1];
      index += 1;
      flddataarray[index] = (AFLOAT)qpart->r[2];
      index += 1;
    }


  /* convert pos units into cgs (cm) */	
  indextmp = index - 3*(nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * units->lengthunit_cgs ;
      flddataarray[i] = valuefloat;
    }


  /* -------- gradenergy */
  flddataarray[index] = (AFLOAT)ppart->gradU[0]; /* since nnList[0]==ppart, we could just ref nnList */
  index += 1;
  flddataarray[index] = (AFLOAT)ppart->gradU[1]; 
  index += 1;
  flddataarray[index] = (AFLOAT)ppart->gradU[2]; 
  index += 1;
  
  for (i = 0; i < nneighbors; ++i)
    {
      qpart=nnList[i].pPart;
      flddataarray[index] = (AFLOAT)qpart->gradU[0];
      index += 1;
      flddataarray[index] = (AFLOAT)qpart->gradU[1];
      index += 1;
      flddataarray[index] = (AFLOAT)qpart->gradU[2];
      index += 1;
    }

  /* convert gradU units into cgs (erg/cm) */
  indextmp = index - 3*(nneighbors+1);
   for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * units->gradenergyunit_cgs ;
      flddataarray[i] = valuefloat;
    }



  /* -------- internal energy -- use Temperature instead */
   /* 
  flddataarray[index] = (AFLOAT)ppart->u; 
  index +=1;
  for (i = 0; i < nneighbors; ++i)
    {
      qpart=nnList[i].pPart;
      flddataarray[index] = (AFLOAT)qpart->u;
      index +=1;
    }
   */
  /* gasoline energy units to c.g.s. units (velocity**2/mass) */
   /* 
 indextmp = index - nneighbors;
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * fldcontext->energyunitcgs;
      flddataarray[i] = valuefloat;
    }
*/


  /* -------- density */
  flddataarray[index] = (AFLOAT)ppart->fDensity; 
  index += 1;
  for (i = 0; i < nneighbors; ++i)
    {
      qpart=nnList[i].pPart;
      flddataarray[index] = (AFLOAT)qpart->fDensity;
      index += 1;
    }
  /* gasoline density units to c.g.s. units (g/cm**3) */
  indextmp = index - (nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * units->densityunit_cgs;
      flddataarray[i] = valuefloat;
    }


    /* -------- mass */
  flddataarray[index] = (AFLOAT)ppart->fMass; 
  index += 1;
  for (i = 0; i < nneighbors; ++i)
    {
      qpart=nnList[i].pPart;
      flddataarray[index] = (AFLOAT)qpart->fMass;
      index += 1;
    }
    /* gasoline mass units to c.g.s. units (g) */
  indextmp = index - (nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {
      valuefloat = flddataarray[i] * units->massunit_cgs;
      flddataarray[i] = valuefloat;
    }


  /* Temperature (from internal energy) -- this part, then neighrbors */
  valuefloat = (AFLOAT)ppart->u;
  flddataarray[index] = valuefloat;
  index += 1;
  for (i = 0; i < nneighbors; ++i)
    {
      qpart=nnList[i].pPart;
      valuefloat = (AFLOAT)qpart->u; 
      //      valuefloat = (AFLOAT)qpart->u/ fldcontext->temperature_to_internalenergy_simunit;
      flddataarray[index] = valuefloat;
      index += 1;
    }
  /* of course we could do the unit conversion in the loop just above */
  /* gasoline internal energy units to c.g.s. units (Kelvin) */
  indextmp = index - (nneighbors+1);
  for (i = indextmp; i < index; ++i)
    {      
      valuefloat = flddataarray[i] / units->temperature_to_internalenergy_simunit;
      flddataarray[i] = valuefloat;
    }

  
}


/* gasoline call on every particle, after the FLD Sph sum is finished,
       from smmoth->fcnPost   -- returns dtFLDmax  */
/* using dtfldmax = etaFLD *  u / du/dt_fld  */
/*  input and output is in sim units  */
/* Note: in gather-only gasoline, we could just do this at end of neighbor loop inside the main SPH sum, but doing it after Sph sum is also compatible with symmtric method */
void getFLDmaxtimestep_particle(PARTICLE *ppart, SMF *smf)
{
  FLDContextStruct* fldcontext;
  fldcontext = smf->pkd->dpn_context->fld;
  
  if(is_gasoline_particle_active(ppart)) // else, ignore, non-active particle
    {
      ppart->dtFLDmax = (SIMFLOAT) get_fld_timestep_from_dudt(ppart->flduDot, ppart->u, fldcontext->EtaFLD); 
    }
}

  

  


//#endif //GASOLINE
