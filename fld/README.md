# README #

DIAPHANE: a Library for radiation and neutrino transport for astrophysics hydro codes.

Quick instructions are:

cd fld

make

This compiles the core physics layer of the Flux Limited Diffusion library module.  All radiation transfer physics algorithms are containted here.  This module does not require MPI and can be called from c/c++ or modern fortran.



The directories fldgasolinewrapper  contains the files to interface with the library from gasoline. Update the diaphane section of the gasoline Makefile with the relevant paths for your system and compile.

The directories fldgadgetwrapper  contains the files to interface with the library from gasoline.  Copy them into the gasoline source directory before compiling gadget.

The directories fldsphynxwrapper  contains the files to interface with the library from gasoline.  Copy them into the gasoline source directory before compiling gadget.  Sphynx is a fortran90 code.  The interface between sphynx in fortran and the FLD module in c/c++ is handled by the iso_c_binding module.


We provide instructions to interface the library with your own hydro code.

Let us know of any problems.

-Darren Reed, for the DIAPHANE team



