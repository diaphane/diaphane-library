

import pynbody
import matplotlib.pyplot as plt
import numpy as np

params={'backend': 'pdf',
       'text.usetex': True}

plotname="tempevo.pdf"


lbox = 2000.
kboltzmann = 1.38e-16
mproton = 1.67e-24
f_mhydrogen = 0.76
mean_molecular_weight = 4. / (1. + 3. * f_mhydrogen)
UnitVelocity_in_cm_per_s = 3163513.0729769906
###   u*m*2/3/k = u* mu * mp *2/ 3 /k
gadgetu_to_temperature = mean_molecular_weight * mproton / kboltzmann * 2./3. * UnitVelocity_in_cm_per_s**2

s = pynbody.load("snapshot_050")
label='t=5000'
plt.plot(s.gas['x'],s.gas['u']*gadgetu_to_temperature,'.',label=label)

s = pynbody.load("snapshot_010")
label='t=1000'
plt.plot(s.gas['x'],s.gas['u']*gadgetu_to_temperature,'.',label=label)

s = pynbody.load("snapshot_000")
label='t=0'
plt.plot(s.gas['x'],s.gas['u']*gadgetu_to_temperature,'.',label=label)


plt.xlabel('x [10$^{-6}$pc]')
plt.ylabel('T [K]')
plt.figtext(.15,.8,'FLD')
plt.legend()


plt.savefig(plotname)

