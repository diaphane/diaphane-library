#!/bin/bash

## sample run script template

## modify to match your diaphane library path
export LD_LIBRARY_PATH=/home/ubuntu/diaphane-library:$LD_LIBRARY_PATH


## Example to set MPI paths if a module name OpenMPI is available on the system
module load OpenMPI

##   4 process mpi job
mpirun -np 4 ./Gadget2 ./run.param > run.log

