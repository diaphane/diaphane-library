

import pynbody
import matplotlib.pyplot as plt

params={'backend': 'pdf',
       'text.usetex': True}

plotname="temperature_evolution.pdf"


#s = pynbody.load("cube.94500")
#label='t=9000'
#plt.plot(s.gas['x'],s.gas['temp'],'.',label=label)

s = pynbody.load("cube.005000")
label='t=5000'
plt.plot(s.gas['x'],s.gas['temp'],'.',label=label)

s = pynbody.load("cube.001000")
label='t=1000'
plt.plot(s.gas['x'],s.gas['temp'],'.',label=label)

s = pynbody.load("ics.std")
label='t=0'
plt.plot(s.gas['x'],s.gas['temp'],'.',label=label)


plt.xlabel('x [10$^{-6}$pc]')
plt.ylabel('T [K]')
plt.figtext(.45,.8,'FLD')
plt.legend()


plt.savefig(plotname)

