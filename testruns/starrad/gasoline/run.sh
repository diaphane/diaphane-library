#!/bin/bash

## sample run script template


##  modify to match your checkpoint.fdl path
export PKDGRAV_CHECKPOINT_FDL="../../../../gasoline-diaphane/checkpoint.fdl"

## modify to match your diaphane library path 
###export LD_LIBRARY_PATH=/home/ubuntu/diaphane-library:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=../../../../diaphane-library:$LD_LIBRARY_PATH

## Example to set MPI paths if a module name OpenMPI is available on the system
## module load OpenMPI

##   8 process mpi job
mpirun -np 8 ./gasoline ./run.param > run.log


