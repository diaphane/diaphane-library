

import pynbody
import matplotlib.pyplot as plt

params={'backend': 'pdf',
       'text.usetex': True}

plotname="temperature_evolution.pdf"


s = pynbody.load("cube.001000")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=1000'
plt.plot(r,s.gas['temp'],'.',label=label)

s = pynbody.load("cube.000300")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=300'
plt.plot(r,s.gas['temp'],'.',label=label)

s = pynbody.load("cube.000100")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=100'
plt.plot(r,s.gas['temp'],'.',label=label)

plt.xlabel('r [10$^{-6}$pc]')
plt.ylabel('T [K]')
plt.figtext(.45,.8,'Starrad source + FLD')
plt.legend()


plt.savefig(plotname)

