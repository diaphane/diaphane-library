

import pynbody
import matplotlib.pyplot as plt

params={'backend': 'pdf',
       'text.usetex': True}

plotname="temperature_evolution.pdf"


s = pynbody.load("cube.010500")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=1000'
plt.plot(r,s.gas['temp'],'.',label=label)

s = pynbody.load("cube.003100")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=295'
plt.plot(r,s.gas['temp'],'.',label=label)

s = pynbody.load("cube.001000")
##  some info ## s.gas.loadable_keys()
r = (s['x']**2 + s['y']**2 + s['z']**2)**0.5
label='t=95'
plt.plot(r,s.gas['temp'],'.',label=label)

plt.xlabel('r [10$^{-6}$pc]')
plt.ylabel('T [K]')
plt.figtext(.45,.8,'Starrad source + FLD')
plt.legend()


plt.savefig(plotname)

